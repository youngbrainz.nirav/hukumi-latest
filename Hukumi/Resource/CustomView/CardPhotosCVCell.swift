//
//  CardPhotosCVCell.swift
//  Hukumi
//
//  Created by Govind Prajapati on 10/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class CardPhotosCVCell: UICollectionViewCell {
    
    @IBOutlet weak var imgViewPhoto: UIImageView!
}
