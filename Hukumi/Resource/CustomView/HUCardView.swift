//
//  HUCardView.swift
//  Hukumi
//
//  Created by Govind Prajapati on 10/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Kingfisher

class HUCardView: UIView {

    @IBOutlet weak var cvUserPhotos: UICollectionView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblUserLocation: UILabel!
    @IBOutlet weak var viewFade: UIView!
    
    private var currentIndex = 0
    
    var model: ModelMatches? {
        didSet{
            loadData()
        }
    }
    
    @IBAction func btnNextAction() {
        
    }
    
    
    @IBAction func btnPreviousAction() {
        
    }
    
    func initSetup () {
        
        let aNib = UINib.init(nibName: "CardPhotosCVCell", bundle: nil)
        cvUserPhotos.register(aNib, forCellWithReuseIdentifier: "CardPhotosCVCell")
    }
    
    func loadData() {
        
        initSetup()
        
        lblUserName.text = model?.user_name ?? ""
        lblUserLocation.text = model?.city ?? ""
        
        cvUserPhotos.dataSource = self
        cvUserPhotos.delegate = self
        
        cvUserPhotos.reloadData()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        cvUserPhotos.layoutIfNeeded()
        
        cvUserPhotos.reloadData()
        
        roundCorners(corners: [.allCorners], radius: 10)
    }
    
    func getFadeLayer() -> CAGradientLayer {
        
        let aLayer = CAGradientLayer()
        aLayer.colors = [UIColor(hexString: "0000", alpha: 0.5), UIColor(hexString: "0000", alpha: 0)].map({ $0.cgColor })
        aLayer.startPoint = CGPoint(x: 0.5, y: 1)
        aLayer.endPoint = CGPoint(x: 0.5, y: 0.5)
        aLayer.frame = viewFade.layer.bounds
     
        return aLayer
    }
}

extension HUCardView: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        var aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CardPhotosCVCell", for: indexPath) as! CardPhotosCVCell
        
        let aImg = model?.profile_image ?? ""
        
        KingfisherManager.shared.retrieveImage(with: URL(string:aImg)!) { result in
            let image = try? result.get().image
            if let image = image {
                aCell.imgViewPhoto.image = image
            }
        }

        return aCell
    }
}

extension HUCardView: UICollectionViewDelegateFlowLayout {

    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        collectionView.superview?.frame.size ?? .zero
    }
}

class userCardModel {
    
    var photos: [UIImage] = []
    var name = ""
    var location = ""
    
    init(dic: [String: Any]) {
        
        photos = (dic["photos"] as? [String])?.compactMap({ UIImage(named: $0) }) ?? []
        name = dic["name"] as? String ?? ""
        location = dic["location"] as? String ?? ""
    }
}


extension UIView {
    class func fromNib<T: UIView>() -> T {
        return Bundle(for: T.self).loadNibNamed(String(describing: T.self), owner: nil, options: nil)![0] as! T
    }
}

extension UIView {
   func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
}
