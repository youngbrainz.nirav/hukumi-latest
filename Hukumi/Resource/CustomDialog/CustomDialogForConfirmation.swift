//
//  CustomDialogForConfirmation.swift
//  Hukumi
//
//  Created by ThisIsAnkur on 10/02/22.
//  Copyright © 2022 Hiren Mistry. All rights reserved.
//

import UIKit

class CustomDialogForConfirmation: UIView {

    @IBOutlet weak var alertView: UIView!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var btnLeft: UIButton!
    @IBOutlet weak var btnRight: UIButton!
    @IBOutlet weak var txtEditText: UITextView!
    
    var onClickLeftButton:(()->Void)?
    var onClickRightButton:(()->Void)?
    var onConfirmReport:((_ text: String)->Void)?
    static func ShowCustomDialogForConfirmation(title:String,msg:String,titleBtnLeft:String,titleBtnRight:String,isTextfiledHidden:Bool=true,tag:Int=400)->CustomDialogForConfirmation{
        let view = UINib(nibName: "CustomDialogForConfirmation", bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! CustomDialogForConfirmation
        if title.isEmpty {
            view.lblTitle.isHidden = true
        }
        let frame = (Common.keyWindow?.frame)!
        view.frame = frame
        view.tag = tag
        view.txtEditText.isHidden = isTextfiledHidden
        view.lblTitle.text = title
        view.lblMessage.text = msg
        view.initialSetup()
        view.btnLeft.isHidden = titleBtnLeft.isEmpty
        view.btnLeft.setTitle(titleBtnLeft, for: .normal)
        view.btnRight.setTitle(titleBtnRight, for: .normal)
        view.backgroundColor = UIColor.black.withAlphaComponent(0.5)
        view.alertView.layer.cornerRadius = 10
        view.txtEditText.setRound(withBorderColor: .black, andCornerRadious: 10, borderWidth: 1)
        Common.keyWindow?.addSubview(view)
        Common.keyWindow?.bringSubviewToFront(view)
        return view
    }
    func initialSetup(){
        btnLeft.titleLabel?.font = Constant.Font.gilroy(size: 20, type: .semiBold)
        btnLeft.setTitleColor(UIColor.themeColor, for: .normal)
        btnRight.titleLabel?.font = Constant.Font.gilroy(size: 20, type: .semiBold)
        btnRight.setTitleColor(UIColor.themeColor, for: .normal)
        lblTitle.font = Constant.Font.gilroy(size: 20, type: .semiBold)
        lblMessage.font = Constant.Font.gilroy(size: 18, type: .regular)
    }
    @IBAction func onClickBtnLeft(_ sender: Any) {
        if onClickLeftButton != nil {
            onClickLeftButton!()
        }
    }
    
    @IBAction func onClickBtnRight(_ sender: Any) {
        if txtEditText.isHidden {
        if onClickRightButton != nil {
            onClickRightButton!()
        }
        }
        else {
            if onConfirmReport != nil {
                if txtEditText.text!.isEmpty {
                    Common().showAlert(strMsg: "Please enter your comment.", view: Common.keyWindow!.rootViewController!)
                } else {
                onConfirmReport!(txtEditText.text!)
                }
            }
        }
    }
    
}
