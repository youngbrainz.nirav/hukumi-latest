
//Sagar

import UIKit
import StoreKit
import SwiftyStoreKit

//MARK: In-App IDs

let isProductionMode = false //1-true or 0-false

var strInAppPurchasePrice = ""

struct InAppIDs {
    
    static let monthly =  "Hukume_subscription"
    static let quarterly =  "Hukume_3_months_subscription"
    static let halfly =  "Hukume_6_months_subscription"

    //static let yearly =  "com.carolacastillo.syfu.yearly"

    
    static let arrSubscription = Set(arrayLiteral: InAppIDs.monthly,InAppIDs.quarterly,InAppIDs.halfly)
}

struct InAppValidationKeys {
    static let production = AppleReceiptValidator(service : .sandbox, sharedSecret: "c055b05d45c14ff9ab4317e52907e2d3")
   // static let production = AppleReceiptValidator(service : .production, sharedSecret: "c055b05d45c14ff9ab4317e52907e2d3")
}

class IAPManager: NSObject {
    
    var myNewView = UIView()
    static let shared = IAPManager()

    var isPurchased : Bool {
        get {
          //   return true
            return UserDefaults.standard.bool(forKey: "IAPUpgradeToPro")
        }
    }
    
//    var yearlyPrice = 19.99
//
//    var yearlyLocalizedPrice = "$ 19.99"
 
    var MonthlyPrice = 9.99
    
    var MonthlyLocalizedPrice = "$ 9.99"
    
    
    var QuarterlyPrice = 21.99
    
    var QuarterlyLocalizedPrice = "$ 21.99"
    
    var HalflyPrice = 42.99
    
    var HalflyLocalizedPrice = "$ 42.99"
    
    let rootVC = appDel.window?.rootViewController  ///appDelegate.window?.rootViewController
    
    override init() {   
        super.init()
        
        SwiftyStoreKit.completeTransactions(atomically: true) { purchases in
            for purchase in purchases {
                print("Purchasing..........")
                if purchase.transaction.transactionState == .purchased || purchase.transaction.transactionState == .restored {
                    if purchase.needsFinishTransaction {
                        // Deliver content from server, then:
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    
                    if InAppIDs.arrSubscription.contains(purchase.productId) {
                        UserDefaults.standard.set(true, forKey:  "IAPUpgradeToPro")
                        UserDefaults.standard.synchronize()
                        
                        if purchase.productId == InAppIDs.monthly
                        {
                            strInAppPurchasePrice =  UserDefaults.standard.value(forKey: "Monthly_PRICE") as! String
                        }
                        else if purchase.productId == InAppIDs.quarterly
                        {
                            strInAppPurchasePrice =  UserDefaults.standard.value(forKey: "Quarterly_PRICE") as! String
                        }
                        else if purchase.productId == InAppIDs.halfly
                        {
                            strInAppPurchasePrice =  UserDefaults.standard.value(forKey: "Halfly_PRICE") as! String
                        }
                        
                    }else {
                        UserDefaults.standard.set(false, forKey:  "IAPUpgradeToPro")
                        UserDefaults.standard.synchronize()
                    }
                }
            }
//            if self.isPurchasedApp(), let rootVC = appDel.window?.rootViewController as? UINavigationController, let salesNavigation = rootVC.presentedViewController as? UINavigationController, salesNavigation.viewControllers[0] is SalesPageVC {
//                rootVC.presentedViewController?.dismiss(animated: true, completion: nil)
//            }
        }
        
        verifyReceipt(productIds: InAppIDs.arrSubscription) { (_, _) in }
        
    }
    
    
    func isPurchasedApp() -> Bool {
        if IAPManager.shared.isPurchased  {
            return true
        }
        else
        {
            return false
        }
    }
    
    func retrivePricing() {
        
        SwiftyStoreKit.retrieveProductsInfo(InAppIDs.arrSubscription) { (results) in
            for product in results.retrievedProducts {
                let proudctId = product.productIdentifier
                let price = product.localizedPrice
                
                if proudctId == InAppIDs.monthly {
                    self.MonthlyLocalizedPrice = price!
                    UserDefaults.standard.set(self.MonthlyLocalizedPrice, forKey: "Monthly_PRICE")
                    self.MonthlyPrice = Double(truncating: product.price)
                }
                
                if proudctId == InAppIDs.quarterly {
                    self.QuarterlyLocalizedPrice = price!
                    UserDefaults.standard.set(self.QuarterlyLocalizedPrice, forKey: "Quarterly_PRICE")
                    self.QuarterlyPrice = Double(truncating: product.price)
                }
                
                if proudctId == InAppIDs.halfly {
                    self.HalflyLocalizedPrice = price!
                    UserDefaults.standard.set(self.HalflyLocalizedPrice, forKey: "Halfly_PRICE")
                    self.HalflyPrice = Double(truncating: product.price)
                }
                
                debugPrint("Id -\(proudctId) Price - \(price!)")
                
                if #available(iOS 11.2, *) {
                    func unitName(unitRawValue:UInt) -> String {
                        switch unitRawValue {
                        case 0: return "days"
                        case 1: return "week"
                        case 2: return "months"
                        case 3: return "years"
                        default: return ""
                        }
                    }
                    
                    if let period = product.introductoryPrice?.subscriptionPeriod {
                        let strPeriod = "\(period.numberOfUnits)"
                        let strPeriodUnit = "\(unitName(unitRawValue: period.unit.rawValue))"
                        let trialPeriod = strPeriod + " " + strPeriodUnit
                        
                        if (product.productIdentifier == InAppIDs.monthly) {
//                            IAPManager.shared.monthTrialDays = trialPeriod
//                            UserDefaults.standard.set(IAPManager.shared.monthTrialDays, forKey: "MONTH_TRIAL_DAYS")
                        }
                    
                        UserDefaults.standard.synchronize()
                    }
                }
            }
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "UpdateSubPrice"), object: nil)
        }
    }
        
    
    func doPurchase(_ forController:UIViewController, _ productId:String, _ completion:@escaping(_ success:Bool,_ error:String)->Void)
    {
       // if !(currentReachabilityStatus == .notReachable) {
        
            DispatchQueue.main.async {
        
        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first
        keyWindow?.rootViewController?.showSpinner(onView: (keyWindow?.rootViewController?.view)!)
            }

        
            SwiftyStoreKit.purchaseProduct(productId, atomically: true) { result in
                
                if case .success(let purchase) = result {
                    // Deliver content from server, then:
                    if purchase.needsFinishTransaction {
                        SwiftyStoreKit.finishTransaction(purchase.transaction)
                    }
                    self.verifyReceipt(productIds: [productId], { (success, reason) in
                        completion(success,reason)
                    })
                    DispatchQueue.main.async {
                      //  GlobalClass.sharedInstance.removeActivity()
                        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

                        keyWindow?.rootViewController?.removeSpinner()
                    }
                } else {
                    // purchase error
                    DispatchQueue.main.async {
                      //  GlobalClass.sharedInstance.removeActivity()
                        
                        let keyWindow = UIApplication.shared.windows.filter {$0.isKeyWindow}.first

                        keyWindow?.rootViewController?.removeSpinner()
                    }
                    
                    if case .error(let er) = result {
                        let code = (er as NSError).code
                        if code == SKError.Code.paymentCancelled.rawValue {
                            completion(false, "Cancelled")
                        } else {
                            completion(false, er.localizedDescription)
                        }
                    }
                }
            }
       // }
//        else {
//            completion(false,"")
//            self.createCustomView(lblTitle: "Warning!", viewBackColor: UIColor.init(red: 237.0/255.0, green: 188.0/255.0, blue: 56.0/255.0, alpha: 1.0), subLabelTitle: "No! internet is Unavailable.", isShowBtn: false)
////            alertController(message: ALERT_NO_NETWORK, controller: rootVC!)
//        }
    }
    
    func doRestore(_ forController:UIViewController, _ completion:@escaping(_ success:Bool, _ error:String) -> ()) {
     //   if !(currentReachabilityStatus == .notReachable){
//            DispatchQueue.main.async {
//                SwiftLoader.show(animated: true)
//            }
            SwiftyStoreKit.restorePurchases(atomically: true) { results in
                
                if results.restoredPurchases.count > 0 {
                    self.verifyReceipt(productIds: InAppIDs.arrSubscription, { (success, reason) in
                        completion(success,reason)
                    })
                } else if let failedResult = results.restoreFailedPurchases.first {
                    completion(false, failedResult.0.localizedDescription)
                }
            }
//         }else{
//            completion(false,"")
//            self.createCustomView(lblTitle: "Warning!", viewBackColor: UIColor.init(red: 237.0/255.0, green: 188.0/255.0, blue: 56.0/255.0, alpha: 1.0), subLabelTitle: "No! internet is Unavailable.", isShowBtn: false)
//        }
    }
    
    func verifyReceipt(productIds : Set<String>,_ completion:@escaping(_ success:Bool,_ error:String)->Void){
     //  if !(currentReachabilityStatus == .notReachable) {
            SwiftyStoreKit.verifyReceipt(using: InAppValidationKeys.production) { result in
                switch result {
                    case .success(let receipt):
                        let purchaseResult = SwiftyStoreKit.verifySubscriptions(productIds: productIds, inReceipt: receipt)
                        switch purchaseResult {
                            case .purchased(let expiryDate, let items):
                                //appDel.APIcallForAppleRecipt()
                                for item in items {
                                    if InAppIDs.arrSubscription.contains(item.productId) {
                                        UserDefaults.standard.set(true, forKey: "IAPUpgradeToPro")
                                        UserDefaults.standard.synchronize()
                                        
                                        if item.productId == InAppIDs.monthly
                                        {
                                            strInAppPurchasePrice =  UserDefaults.standard.value(forKey: "Monthly_PRICE") as! String
                                        }
                                        else if item.productId == InAppIDs.quarterly
                                        {
                                            strInAppPurchasePrice =  UserDefaults.standard.value(forKey: "Quarterly_PRICE") as! String
                                        }
                                        else if item.productId == InAppIDs.halfly
                                        {
                                            strInAppPurchasePrice =  UserDefaults.standard.value(forKey: "Halfly_PRICE") as! String
                                        }
                                        
                                        
                                    }
                                }
                                completion(true,"purchased")
                                print("\(productIds) are valid until \(expiryDate)\n\(items)\n")
                            case .expired(let expiryDate, let items):
                                for item in items{
                                    if InAppIDs.arrSubscription.contains(item.productId) {
                                        UserDefaults.standard.set(false, forKey: "IAPUpgradeToPro")
                                    
                                        UserDefaults.standard.synchronize()
                                        
                                        isUserSubscribe = false
                                     
                                        guard let aWebViewVC = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "ExploreVC") as? ExploreVC else { return }
                                        
                                        aWebViewVC.CallUpdateSubscription()

                                    }
                                }
                                completion(false,"expired")
                                debugPrint("\(productIds) are expired since \(expiryDate)\n\(items)\n")
                            case .notPurchased:
                                completion(false,"notPurchased")
                                print("The user has never purchased \(productIds)")
                    }
                case .error(let error):
                    completion(false,error.localizedDescription)
                    debugPrint("Receipt verification failed: \(error)")
                }
                DispatchQueue.main.async {
                    
                }
            }
       // }
    }
}

extension IAPManager {
    func createCustomView(lblTitle: String, viewBackColor: UIColor, subLabelTitle: String, isShowBtn: Bool)
    {
        let window = appDel.window
        let rootViewController = window?.rootViewController
        var currentController = rootViewController
        while let presentedController = currentController?.presentedViewController {
            currentController = presentedController
        }
        let view = currentController?.view!
        myNewView = UIView(frame: CGRect(x: 0, y: view!.frame.height - 100, width: view!.frame.width * 0.9, height: 80))
        myNewView.backgroundColor=UIColor.lightGray
        myNewView.layer.cornerRadius=10
        myNewView.backgroundColor = viewBackColor
        myNewView.center = CGPoint(x: view!.frame.width / 2, y: view!.frame.height - 80)
        view!.addSubview(self.myNewView)
        
        myNewView.center.y += 100
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.3,
                           delay: 0.2,
                           options: UIView.AnimationOptions.transitionCurlUp,
                           animations: { () -> Void in
                            self.myNewView.center.y -= 100
            }, completion: { (finished) -> Void in
                
            })
        }
        let label = UILabel(frame: CGRect(x: 20, y: myNewView.frame.height / 4, width: 100, height: 25))
        label.textAlignment = NSTextAlignment.center
        label.text = lblTitle
        label.textColor = UIColor.white
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .left
        label.font = UIFont.boldSystemFont(ofSize: 20)
        myNewView.addSubview(label)
        
        
        let subLabel = UILabel(frame: CGRect(x: 20, y: (label.frame.origin.y + label.frame.height), width: myNewView.frame.width * 0.9, height: 25))
        subLabel.textAlignment = NSTextAlignment.center
        subLabel.text = subLabelTitle
        subLabel.textColor = UIColor.white
        subLabel.numberOfLines = 0
        subLabel.lineBreakMode = .byWordWrapping
        subLabel.textAlignment = .left
        myNewView.addSubview(subLabel)
        
        var btnClose :UIButton
        btnClose = UIButton(frame:CGRect(x: 0, y: myNewView.frame.height / 2, width: 25, height: 25));
        btnClose.setImage(UIImage.init(named: "delete.png"), for: .normal)
        btnClose.center = CGPoint(x: myNewView.frame.width - 30, y: myNewView.frame.height / 2)
        btnClose.addTarget(self, action: #selector(closeAction), for: .touchUpInside)
        
        if isShowBtn {
            myNewView.addSubview(btnClose)
        }else {
            UIView.animate(withDuration: 0.7,
                           delay: 1.5,
                           options: UIView.AnimationOptions.curveLinear,
                           animations: { () -> Void in
                            self.myNewView.center.y += 200
            }, completion: { (finished) -> Void in
                
            })
        }
    }
    
    @objc func closeAction()
    {
        DispatchQueue.main.async {
            UIView.animate(withDuration: 0.2,
                           delay: 1.5,
                           options: UIView.AnimationOptions.curveLinear,
                           animations: { () -> Void in
                            self.myNewView.center.y += 200
            }, completion: { (finished) -> Void in
                
            })
        }
    }
    
    func priceStringForProduct(item: SKProduct) -> String? {
        let price = item.price
        if price == NSDecimalNumber(decimal: 0.00) {
            return "GET" //or whatever you like really... maybe 'Free'
        } else {
            let numberFormatter = NumberFormatter()
            let locale = item.priceLocale
            numberFormatter.numberStyle = .currency
            numberFormatter.locale = locale
            return numberFormatter.string(from: price)
        }
    }
}

extension SKProduct {

    private static let formatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.numberStyle = .currency
        return formatter
    }()

    var isFree: Bool {
        price == 0.00
    }

    var localizedPrice: String? {
        guard !isFree else {
            return nil
        }
        
        let formatter = SKProduct.formatter
        formatter.locale = priceLocale

        return formatter.string(from: price)
    }

}
