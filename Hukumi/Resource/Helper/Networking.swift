
import UIKit
import Alamofire
import SwiftyJSON

struct APIs{
    
    //static let BASE_URL = "http://kudumi.creptive.com/api/v1/"
//   static let BASE_URL = "http://18.220.99.17/api/"
//    static let BASE_URL = "http://demoubill.creptive.com/api/v1/"
    static let BASE_URL = "https://www.hukumeonline.com/api/"

    static let LOGIN = "loginUser"
    static let REGISTER = "registerUser"
    static let SEND_OTP = "sendOTP"
    static let GET_USER_PROFILE = "getUserProfile"
    static let GET_PAGES = "getPages"
    static let GET_INTRO_IMAGE = "getIntroImagesList"
    static let HOME_SCREEN_USER_LIST = "homeScreenUserList"
    static let GET_PASSION_LIST = "getPassionList"
    static let GET_MATCHED_RECORD = "getMatchesRecord"
    static let GET_EDUCATION_LIST = "getEducationList"
    static let CHANGE_STATUS = "changeStatus"
    static let ADD_TO_FAV = "addtoFavourite"
    static let CHANGE_REQUESTEDMATCH_RECORD = "changeRequstedMatchesRecord"
    static let CHECK_USER_EXIST = "checkUserExistorNot"
    static let UPDATE_PROFILE = "updateProfile"
    static let GET_FEED_DETAIL = "getFeedDetail"
    static let START_CONVERSATION = "startConversation"
    static let GET_FILTERS = "getFilter"
    static let SET_FILTERS = "setFilter"
    static let CHECK_SUBSCRIPTION = "isUserSubscribed"
    static let MAKE_SUBSCRIPTION = "makeSubscription"
    static let UPDATE_ONLINE_STATUS = "updateOnlineStatus"
    static let GET_CONVERSATIONLIST = "getConversationList"
    static let REMOVE_IMAGE = "removeImage"
    static let LAST_CONVERSATION = "lastConversation"
    static let READ_MESSAGES  = "readMessages"
    static let GET_SUBSCRIPTIONPLAN = "getSubscriptionPlans"
    static let REMOVE_FRIEND = "unMatchUser"
    static let FORGET_PWD = "resetPassword"
    static let CREATE_FACEIMG = "uploadDetectFaceImage"
    static let HUKUME_PLUS_PLANS = "hukumePlusPlans"
    static let MAKE_PAYMENT_PLUS = "makePaymentHukumePlus"

    static let LOGOUT = "logout"
    static let DEACTIVATE_ACCOUNT = "deactivateAccount"
    static let BLOCK_USER = "blockUnblockUser"
    static let REPORT_USER = "reportUser"
    static let DELETE_CHAT = "deleteChatHistory"
    static let REMOVE_SUBSCRIPTION = "unsubscribePlan"
}
struct Networking {
    
    enum Router: URLRequestConvertible {
        
        // MARK: - API name
        case loginUser([String:Any])
        case registerUser([String:Any])
        case sendOTP([String:Any])
        case getUserProfile([String:Any])
        case getPages([String:Any])
        case getIntroImagesList([String:Any])
        case homeScreenUserList([String:Any])
        case getPassionList([String:Any])
        case getMatchesRecord([String:Any])
        case getEducationList([String:Any])
        case changeStatus([String:Any])
        case addtoFavourite([String:Any])
        case changeRequstedMatchesRecord([String:Any])
        case checkUserExistorNot([String:Any])
        case logout([String:Any])
        case updateProfile([String:Any])
        case getConversationList([String:Any])
        case getFeedDetail([String:Any])
        case startConversation([String:Any])
        case getFilters([String:Any])
        case setFilters([String:Any])
        case checkSubscription([String:Any])
        case makeSubscription([String:Any])
        case updateOnlineStatus([String:Any])
        case lastConversation([String:Any])
        case readMessages([String:Any])
        case getSubscriptionPlan([String:Any])
        case forgetPwd([String:Any])
        case removeFriend([String:Any])
        case removeImage([String:Any])
        case createFace([String:Any])
        case makePayment([String:Any])
        case hukumPlusPlans([String:Any])
        case deactivateAccount([String:Any])
        case blockUser([String:Any])
        case reportUser([String:Any])
        case deleteChat([String:Any])
        case removeSubscription([String:Any])
        // MARK: - Methods
        var method: HTTPMethod {
            switch self {
            //** Post Api
            case
                .loginUser,.registerUser,.sendOTP,.getUserProfile,.homeScreenUserList,.getMatchesRecord,.changeStatus,.addtoFavourite,.changeRequstedMatchesRecord,.checkUserExistorNot,.updateProfile,.getConversationList,.getFeedDetail,.startConversation,.getFilters,.setFilters,.checkSubscription,.makeSubscription,.updateOnlineStatus,.removeImage,.lastConversation,.readMessages,.removeFriend,.getSubscriptionPlan,.forgetPwd,.createFace,.makePayment,.hukumPlusPlans,.logout,.deactivateAccount,.blockUser,.reportUser, .deleteChat, .removeSubscription:
                return .post
            case .getPages,.getPassionList,.getEducationList,.getIntroImagesList:
                return .get
                break
                return .put
            }
        }
        var path: String {
            
            switch self {
            case .loginUser(_):
                return APIs.LOGIN
            case .registerUser(_):
                return APIs.REGISTER
            case .hukumPlusPlans(_):
                return APIs.HUKUME_PLUS_PLANS
            case .createFace(_):
                return APIs.CREATE_FACEIMG
            case .makePayment(_):
                return APIs.MAKE_PAYMENT_PLUS
            case .sendOTP(_):
                return APIs.SEND_OTP
            case .getUserProfile(_):
                return APIs.GET_USER_PROFILE
            case .getPages(_):
                return APIs.GET_PAGES
            case .getIntroImagesList(_):
                return APIs.GET_INTRO_IMAGE
            case .homeScreenUserList(_):
                return APIs.HOME_SCREEN_USER_LIST
            case .getPassionList(_):
                return APIs.GET_PASSION_LIST
            case .getMatchesRecord(_):
                return APIs.GET_MATCHED_RECORD
            case .getEducationList(_):
                return APIs.GET_EDUCATION_LIST
            case .changeStatus(_):
                return APIs.CHANGE_STATUS
            case .addtoFavourite(_):
                return APIs.ADD_TO_FAV
            case .changeRequstedMatchesRecord(_):
                return APIs.CHANGE_REQUESTEDMATCH_RECORD
            case .checkUserExistorNot(_):
                return APIs.CHECK_USER_EXIST
            case .updateProfile(_):
                return APIs.UPDATE_PROFILE
            case .getConversationList(_):
                return APIs.GET_CONVERSATIONLIST
            case .getFeedDetail(_):
                return APIs.GET_FEED_DETAIL
            case .startConversation(_):
                return APIs.START_CONVERSATION
            case .getFilters(_):
                return APIs.GET_FILTERS
            case .setFilters(_):
                return APIs.SET_FILTERS
            case .checkSubscription(_):
                return APIs.CHECK_SUBSCRIPTION
            case .makeSubscription(_):
                return APIs.MAKE_SUBSCRIPTION
            case .updateOnlineStatus(_):
                return APIs.UPDATE_ONLINE_STATUS
            case .lastConversation(_):
                return APIs.LAST_CONVERSATION
            case .readMessages(_):
                return APIs.READ_MESSAGES
            case .removeImage(_):
                return APIs.REMOVE_IMAGE
            case .removeFriend(_):
                return APIs.REMOVE_FRIEND
            case .forgetPwd(_):
                return APIs.FORGET_PWD
            case .getSubscriptionPlan(_):
                return APIs.GET_SUBSCRIPTIONPLAN
            case .getPages(_):
                return APIs.GET_PAGES
                
           
            case .logout(_):
                return APIs.LOGOUT
                
            case    .deactivateAccount(_):
                return APIs.DEACTIVATE_ACCOUNT
            case    .blockUser(_):
                return APIs.BLOCK_USER
            case    .reportUser(_):
                return APIs.REPORT_USER
            case    .deleteChat(_):
                return APIs.DELETE_CHAT
            case    .removeSubscription(_):
                return APIs.REMOVE_SUBSCRIPTION
            default :
                return ""
            }
        }
        //** Intialize api path in |path|
        func asURLRequest() throws -> URLRequest {
            
            var strUrl = APIs.BASE_URL + path


            if path.contains("https://api.twilio"){
                strUrl = path
            }
            if path.contains("create-payment"){
//                strUrl = APIs.PAYMETNBASE_URL + path
            }
            
            let URL = Foundation.URL(string:strUrl)!
            var urlRequest = URLRequest(url: URL as URL)
            let accessToken = ""
            urlRequest.httpMethod = method.rawValue
            urlRequest.setValue("application/json", forHTTPHeaderField: "Content-Type")
          //  urlRequest.setValue(Global().FetchAuthToken(), forHTTPHeaderField: "Tokenkey")
            urlRequest.timeoutInterval =  120
            
            switch self {
            case .loginUser(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .registerUser(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .sendOTP(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getUserProfile(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getPages(_):
                urlRequest = try JSONEncoding.default.encode(urlRequest)
            case .getIntroImagesList(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .homeScreenUserList(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getPassionList(_):
                urlRequest = try JSONEncoding.default.encode(urlRequest)
            case .getMatchesRecord(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getEducationList(_):
                urlRequest = try JSONEncoding.default.encode(urlRequest)
            case .changeStatus(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .addtoFavourite(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .changeRequstedMatchesRecord(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .checkUserExistorNot(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getFeedDetail(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .updateProfile(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getConversationList(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .startConversation(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getFilters(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .setFilters(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .checkSubscription(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .makeSubscription(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .updateOnlineStatus(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .lastConversation(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .readMessages(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .removeImage(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .removeFriend(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .forgetPwd(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getSubscriptionPlan(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .getPages(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .logout(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .deactivateAccount(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .blockUser(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .reportUser(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .deleteChat(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
            case .removeSubscription(let parameters):
                urlRequest = try JSONEncoding.default.encode(urlRequest, with:parameters)
           default:
                break
            }
            return urlRequest
        }
    }
    /**
     * Genric method use for performing web api request and after getting response callback to caller class.
     *
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    
    static func performApiCall(_ requestName: Networking.Router,_ showSpinner:Bool, _ onView:UIViewController, completionHandler:@escaping ( (DataResponse<Any>) -> Void) ) {
        if showSpinner{
                onView.showSpinner(onView: onView.view)
        }
       // print(requestName.urlRequest?.url?.absoluteString)

        let request = Alamofire.request(requestName).validate().responseJSON { response in
            
         //   print("Parsed JSON: \(String(describing: response.result.value))")
            switch response.result {
            case .success:
                if let objdata = response.result.value as? [String:Any]{
                    if objdata["errorCode"] as? String ?? "" == "401"{
                    }else if objdata["errorCode"] as? String ?? "" == "403"{
                    }
                }
                DispatchQueue.main.async {
                    if showSpinner{
                        onView.removeSpinner()
                    }
                }

                print("Get Success response from server with status code:\(String(describing: response.response?.statusCode)), for api request:\(String(describing: response.request?.url)) \nParameters: \(requestName)")
                
                            //** Handle failure response
            case .failure:
                
                    onView.removeSpinner()
                    
              //  print("Get response from server for api request:\(String(describing: response.request?.url)) in failure section")
                if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                }else{
                    DispatchQueue.main.async {
                        print("loader is called")
                        onView.removeSpinner()

                    }
                }
//                    403 - maintenance
//                    401 - Token Expired

                        if requestName.urlRequest?.url?.absoluteString.contains("secure/customer/order/get") ?? false{
                            
                        }else{
                           // Common().showAlert(strMsg: "Something went wrong!", view: onView)
                        }
                print("Get Success response from server with status code:\(String(describing: response.response?.statusCode)), for api request:\(String(describing: response.request?.url)) \nParameters: \(requestName)")
                Networking.handleApiResponse(response)
            }
            completionHandler(response)
        }
        print("Request Added to Queue for exection. Request URL:\(request)")   // original URL request
    }

    static func handleApiResponse(_ response: DataResponse<Any>) {
        
        let errorCode = response.response?.statusCode
        if errorCode == nil {
            //errorCode = response.result.error?.code
        }
        print("Get response from server with status code:\(String(describing: errorCode)), for api request:\(String(describing: response.request?.url))")
        
        let dataString = String(data: response.data!, encoding: String.Encoding.utf8)
        
        let result:[String:Any] = [String:Any]() //Util.convertStringToDictionary(dataString!)
        if let data = dataString?.data(using: String.Encoding.utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:Any]
                print(json)
                if let status = json?["status_code"] as? Int{
                    if status == 200 || status == 201{
                        if let message = json?["message"] as? String {
                        }
                    }else{
                        if let message = json?["message"] as? [String:Any]{
                            let allkeys = [String] (message.keys)
                            print(allkeys)
                            let msg = message[allkeys[0] ?? ""] as? [String]
                            
                        }else{
                            if status != 422{
                            }else{
                                
                            }
                        }
                    }
                }
                
            } catch {
                print("Something went wrong")
            }
        }

        var errorDescription = ""
        
        if let errorDes = result["message"] as? String {
            errorDescription = errorDes
        }
        
        if errorDescription == "" && dataString != nil {
            errorDescription = dataString!
        }
        
        var strError = errorDescription as String
        
        if let contentType = response.response?.allHeaderFields["Content-Type"] as? String {
            if contentType == "text/html" {
                strError = "Server error"
            }
        }
        
        print(strError)
        
        if let httpStatusCode = errorCode {
            switch httpStatusCode {
                
            case 401:
                print("Session Expired")
                
                let uiAlert = UIAlertController(title: "Session expire", message: "Your last session has expired, please log in again" , preferredStyle:UIAlertController.Style.alert)
                
                uiAlert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                    

                }))
                
            //** Almofire libarary error code
            case -999:
                print("\(String(describing: response.request?.url)) request was cancelled")
           // case -1001:
               // Util.showAlertWithMessage(msgTimeOut, title:"Error")
             case -1003, -1004, -1009:
                break
            //case -1005:
               // Util.showAlertWithMessage(msgConnectionLost, title:"Error")
            case -1200, -1201, -1202, -1203, -1204, -1205, -1206:
                break
            default:
                break
            }
        }
        else {
        }
    }
    
    /**
     *   This method upload image(s) as a multipart data format
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter imageArray: Array of images it must not be nil
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    static func uploadImages(_ requestName: Networking.Router, profile_image:UIImage,imageArray: [UIImage], strImageKey : String, showHud: Bool, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        
        if imageArray.count < 1 {
            return
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 0
            let imageData: NSData? = profile_image.jpegData(compressionQuality: 0.5) as NSData?
            if imageData != nil {
                multipartFormData.append(imageData! as Data, withName: "profile_image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            for image in imageArray {
                let imageData: NSData? = image.jpegData(compressionQuality: 0.5) as NSData?
                if imageData != nil {
                    multipartFormData.append(imageData! as Data, withName: "images[\(index)]", fileName: "image.jpeg", mimeType: "image/jpeg")
                }
                index += 1
            }
            
        }, with: requestName,encodingCompletion: { encodingResult in
            switch encodingResult {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    
                    print("Image(s) Uploaded successfully:\(response)")
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                
            }
        
            completionHandler!(encodingResult)
        })
    }

    /**
     *   This method upload image(s) as a multipart data format
     * - parameter requestName: A perticular request that define in Router Enum. It may contain request parameter or may not be.
     * - parameter imageArray: Array of images it must not be nil
     * - parameter callerObj: Object of class which make api call
     * - parameter showHud: A boolean value that represent is need to display hud for the api or not
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    
    
    static func uploadImagesWithParams(_ requestName: Networking.Router, profile_image:UIImage, imageDic: [UIImage], dictParams: [String: Any], showHud: Bool,onView:UIViewController, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        print("Parameters::",dictParams)
        if showHud{
                onView.showSpinner(onView: onView.view)
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 0
            let imageData: NSData? = profile_image.jpegData(compressionQuality: 0.5) as NSData?
            if imageData != nil {
                multipartFormData.append(imageData! as Data, withName: "profile_image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            


            if imageDic.count > 0
            {
            for (value) in imageDic as [UIImage] {
                if let val = value as? UIImage{
                    let imageData: NSData? = val.jpegData(compressionQuality: 0.1) as NSData?
                    print(imageData?.bytes)
                    
                    if imageData != nil {
                        multipartFormData.append(imageData! as Data, withName: "images[\(index)]", fileName: "image.jpeg", mimeType: "image/jpeg")
                    }
                }

                for (key, value) in dictParams {
                    let data = "\(value)".data(using: .utf8)
                    multipartFormData.append(data! as Data, withName: key)
                }
                
                index += 1
            }
            }
            else
            {
                for (key, value) in dictParams {
                    let data = "\(value)".data(using: .utf8)
                    multipartFormData.append(data! as Data, withName: key)
                }
            }
            
            
            
        }, with: requestName,encodingCompletion: { encodingResult in
            switch encodingResult {

            case .success(let upload, _, _):
                

                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                        if showHud{
                            onView.removeSpinner()
                        }
                    }

                    switch response.result {
                    case .success:
                        print("Get Success response from server with status code:\(String(describing: response.response?.statusCode)), for api request:\(String(describing: response.request?.url))")
                        
                        if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                        //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                        }else{
                            DispatchQueue.main.async {
                                print("loader is called")
                            }
                        }
                        
                    completionHandler!(encodingResult)

                    Networking.handleApiResponse(response)

                    case .failure:
                        
                        print("Get response from server for api request:\(String(describing: response.request?.url)) in failure section")
                        if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                        //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                        }else{
                            DispatchQueue.main.async {
                                print("loader is called")
                            }
                        }
                        completionHandler!(encodingResult)

                        Networking.handleApiResponse(response)

                    }

                    
                }
            case .failure(let encodingError):
                 print("encodingError:\(encodingError)")
                DispatchQueue.main.async {
                    if showHud{
                        onView.removeSpinner()
                    }
                }

            }
            
        })
    }
    static func uploadImagesWithParamsForReportUser(_ requestName: Networking.Router,  imageDic: [UIImage], dictParams: [String: Any], showHud: Bool,onView:UIViewController, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        print("Parameters::",dictParams)
        if showHud{
                onView.showSpinner(onView: onView.view)
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 0
            
            if imageDic.count > 0
            {
            for (value) in imageDic as [UIImage] {
                if let val = value as? UIImage{
                    let imageData: NSData? = val.jpegData(compressionQuality: 0.2) as NSData?
                    print(imageData?.bytes)
                    
                    if imageData != nil {
                        multipartFormData.append(imageData! as Data, withName: "report_image", fileName: "image.jpeg", mimeType: "image/jpeg")
                    }
                }

                for (key, value) in dictParams {
                    let data = "\(value)".data(using: .utf8)
                    multipartFormData.append(data! as Data, withName: key)
                }
                
                index += 1
            }
            }
            else
            {
                for (key, value) in dictParams {
                    let data = "\(value)".data(using: .utf8)
                    multipartFormData.append(data! as Data, withName: key)
                }
            }
            
            
            
        }, with: requestName,encodingCompletion: { encodingResult in
            switch encodingResult {

            case .success(let upload, _, _):
                

                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                        if showHud{
                            onView.removeSpinner()
                        }
                    }

                    switch response.result {
                    case .success:
                        print("Get Success response from server with status code:\(String(describing: response.response?.statusCode)), for api request:\(String(describing: response.request?.url))")
                        
                        if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                        //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                        }else{
                            DispatchQueue.main.async {
                                print("loader is called")
                            }
                        }
                        
                    completionHandler!(encodingResult)

                    Networking.handleApiResponse(response)

                    case .failure:
                        
                        print("Get response from server for api request:\(String(describing: response.request?.url)) in failure section")
                        if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                        //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                        }else{
                            DispatchQueue.main.async {
                                print("loader is called")
                            }
                        }
                        completionHandler!(encodingResult)

                        Networking.handleApiResponse(response)

                    }

                    
                }
            case .failure(let encodingError):
                 print("encodingError:\(encodingError)")
                DispatchQueue.main.async {
                    if showHud{
                        onView.removeSpinner()
                    }
                }

            }
            
        })
    }
    static func uploadFaceImagesWithParams(_ requestName: Networking.Router, profile_image:UIImage, imageDic: [UIImage], dictParams: [String: Any], showHud: Bool,onView:UIViewController, completionHandler: ((SessionManager.MultipartFormDataEncodingResult) -> Void)?) {
        print("Parameters::",dictParams)
        if showHud{
                onView.showSpinner(onView: onView.view)
        }
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            var index = 0
            let imageData: NSData? = profile_image.jpegData(compressionQuality: 0.5) as NSData?
            if imageData != nil {
                multipartFormData.append(imageData! as Data, withName: "face_image", fileName: "image.jpeg", mimeType: "image/jpeg")
            }
            


            if imageDic.count > 0
            {
            for (value) in imageDic as [UIImage] {
                if let val = value as? UIImage{
                    let imageData: NSData? = val.jpegData(compressionQuality: 0.1) as NSData?
                    print(imageData?.bytes)
                    
                    if imageData != nil {
                        multipartFormData.append(imageData! as Data, withName: "images[\(index)]", fileName: "image.jpeg", mimeType: "image/jpeg")
                    }
                }

                for (key, value) in dictParams {
                    let data = "\(value)".data(using: .utf8)
                    multipartFormData.append(data! as Data, withName: key)
                }
                
                index += 1
            }
            }
            else
            {
                for (key, value) in dictParams {
                    let data = "\(value)".data(using: .utf8)
                    multipartFormData.append(data! as Data, withName: key)
                }
            }
            
            
            
        }, with: requestName,encodingCompletion: { encodingResult in
            switch encodingResult {

            case .success(let upload, _, _):
                

                upload.responseJSON { response in
                    
                    DispatchQueue.main.async {
                        if showHud{
                            onView.removeSpinner()
                        }
                    }

                    switch response.result {
                    case .success:
                        print("Get Success response from server with status code:\(String(describing: response.response?.statusCode)), for api request:\(String(describing: response.request?.url))")
                        
                        if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                        //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                        }else{
                            DispatchQueue.main.async {
                                print("loader is called")
                            }
                        }
                        
                    completionHandler!(encodingResult)

                    Networking.handleApiResponse(response)

                    case .failure:
                        
                        print("Get response from server for api request:\(String(describing: response.request?.url)) in failure section")
                        if (requestName.urlRequest?.description.contains("timeline"))! ||  (requestName.urlRequest?.description.contains("likes-unlike"))! {
                        //    DELEGATE.API_ForgotPasswordToken = response.response?.allHeaderFields["reset_password_token"] as? String ?? ""
                        }else{
                            DispatchQueue.main.async {
                                print("loader is called")
                            }
                        }
                        completionHandler!(encodingResult)

                        Networking.handleApiResponse(response)

                    }

                    
                }
            case .failure(let encodingError):
                 print("encodingError:\(encodingError)")
                DispatchQueue.main.async {
                    if showHud{
                        onView.removeSpinner()
                    }
                }

            }
            
        })
    }
    /**
     * Method use for Image downloading from URL using KingFisher library.
     * - parameter fromUrl: Downloading image URL string
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    static func downloadImage(fromUrl url: String, completionHandler:@escaping (_ image: UIImage?) -> Void) {
        
        if url.isEmpty {
            completionHandler(nil)
            return
        }
        
        
       // imageViewTest.kf.setImage(with: URL(string: url), placeholder: nil, options: nil, progressBlock: nil) { (image, error, cacheType, imageURL) -> () in
            
          //  completionHandler(image)
       // }
    }
    
    /**
     * Genric method use for Image downloading from URL.
     * - parameter fromUrl: Downloading image URL string
     * - parameter paceholder: paceholder image name. If we not get image from server then return placehodel image.
     * - parameter completionHandler: A closure that provide callback to caller after getting response
     */
    
    static func downloadImage(fromUrl url: String, withPlaceHolder paceholder: String, completionHandler:@escaping (_ image: UIImage?) -> Void) {
        
        if url.isEmpty && paceholder.isEmpty {
            completionHandler(nil)
            return
        }
        
        if url.isEmpty && !paceholder.isEmpty {
            completionHandler(UIImage(named:paceholder))
            return
        }
        
        var defaultImage    = UIImage()
        
        if !paceholder.isEmpty {
            defaultImage = UIImage(named:paceholder)!
        }
        
       // imageViewTest.kf.setImage(with: URL(string: url), placeholder: defaultImage, options: nil, progressBlock: nil) { (image, error, cacheType, imageURL) -> () in
            
         //   if image == nil {
              //  completionHandler(defaultImage)
         //   }
          //  else {
               // completionHandler(image)
           // }
       // }
    }
    
}


/**
 * Response Object Serialization Extension
 */
public protocol ResponseObjectSerializable {
    init?(response: HTTPURLResponse, representation: AnyObject)
}


