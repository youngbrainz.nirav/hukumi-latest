//
//  HUBorderButton.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class HUBorderButton: UIButton {

    @IBInspectable var cornerRadius: Float = 10
    @IBInspectable var borderWidth: Float = 1
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        initSetup()
    }
    
    func initSetup() {
        
        layer.cornerRadius = CGFloat(cornerRadius)
        layer.borderWidth = CGFloat(borderWidth)
        layer.borderColor = UIColor.white.cgColor
    }

}
