//
//  HURadioButton.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class HURadioButton: UIButton {
    
    @IBInspectable var selectionFontColor: UIColor = Constant.Color.themeRed
    @IBInspectable var unselectFontColor: UIColor = .white

    @IBInspectable var isRadioSelected: Bool = false {
        didSet {
            selectionChange()
        }
    }
    
    @IBOutlet weak var lblText: UILabel?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        initSetup()
    }
    
    func initSetup() {
        
        addTarget(self, action: #selector(didSelect), for: .touchUpInside)
        selectionChange()
    }
    
    private func selectionChange () {
        
        let aNewImage = isRadioSelected ? #imageLiteral(resourceName: "radio select") : #imageLiteral(resourceName: "radio blank")
        let aTextColor = isRadioSelected ? selectionFontColor : unselectFontColor
        
        setImage(aNewImage, for: .normal)
        lblText?.textColor = aTextColor
    }
    
    @objc private func didSelect() {
        isRadioSelected = !isRadioSelected
    }
}
