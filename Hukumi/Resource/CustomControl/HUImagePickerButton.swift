//
//  HUImagePickerButton.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class HUImagePickerButton: HUBorderButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        initSetup()
    }
    
    override func initSetup() {
        super.initSetup()
        
    }

}
