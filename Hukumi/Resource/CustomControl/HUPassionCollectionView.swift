//
//  HUPassionCollectionView.swift
//  Hukumi
//
//  Created by Govind Prajapati on 10/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class HUPassionCollectionView: UICollectionView {

    var arrPassions: [Model_Passion] = [Model_Passion]()
    var arrSelectedPassions: [Model_Passion] = [Model_Passion]()
    
    @IBInspectable var unselectFontColor: UIColor = .red
    @IBInspectable var unselectBorderColor: UIColor = .white
    @IBInspectable var unselectBackgroundColor: UIColor = .clear
    
    @IBInspectable var selectFontColor: UIColor = .white
    @IBInspectable var selectBorderColor: UIColor = .clear
    @IBInspectable var selectBackgroundColor: UIColor = .red
    
    var selectedPassions: [Model_Passion] {
        return arrSelectedPassions
    }
    
    func loadPassion(_ aPassions: [Model_Passion]) {
        
        dataSource = self
        delegate = self
        
        arrPassions = aPassions
        
        reloadData()
    }
}

extension HUPassionCollectionView: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        arrPassions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "PassionCVCell", for: indexPath) as! PassionCVCell
        
        aCell.lblTitle.text = arrPassions[indexPath.row].name
        
        
        
        let aIsSelected = self.arrSelectedPassions.contains{$0._id == arrPassions[indexPath.row]._id}
        
        
      
        aCell.viewBackground.backgroundColor = aIsSelected ? selectBackgroundColor: unselectBackgroundColor
        
        aCell.viewBackground.layer.borderWidth = 1
        aCell.viewBackground.layer.borderColor = (aIsSelected ? selectBorderColor: unselectBorderColor).cgColor
        aCell.viewBackground.layer.cornerRadius = 18
        aCell.viewBackground.layer.masksToBounds = true
        
        
        if aIsSelected == true
        {
            aCell.lblTitle.textColor = .white
        }
        else
        {
            aCell.lblTitle.textColor = .black

        }
        
       // aCell.lblTitle.textColor = aIsSelected ? selectFontColor : unselectFontColor
        
        return aCell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        let aText = arrPassions[indexPath.row].name
        let aSize = NSString(string: aText).size(withAttributes: [NSAttributedString.Key.font: Constant.Font.gilroy(size: 12, type: .regular) as Any])

        let aWidth = aSize.width + 50

        return CGSize(width: aWidth, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
//        defer {
//            reloadData()
//        }
//
        
       
       
             var isFound = false
        
            for i in 0..<arrSelectedPassions.count
            {
                if arrSelectedPassions[i]._id == arrPassions[indexPath.row]._id
                {
                    isFound = true
                    arrSelectedPassions.remove(at: i)

                    self.reloadData()
                    break
                }
                
            }

        
        if isFound == false
        {
            arrSelectedPassions.append(arrPassions[indexPath.row])
            self.reloadData()

        }
        
        
        
        
        
//        guard let aOldIndex = arrSelectedPassions.firstIndex(of: arrPassions[indexPath.row]) else {
//
//            arrSelectedPassions.append(arrPassions[indexPath.row])
//
//            return
//
//        }
//
//        arrSelectedPassions.remove(at: aOldIndex)
    }
}
