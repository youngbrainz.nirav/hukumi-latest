//
//  HUTextFieldUnderLine.swift
//  Hukumi
//
//  Created by Govind Prajapati on 09/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class HUTextFieldUnderLine: UITextField {
    
    @IBInspectable var lineColor: UIColor = .lightGray
    @IBInspectable var lineWidth: Float = 1
    
    var isLineAdded = false
    
    func initSetup() {
        
        let aLblSaprater = UILabel(frame: CGRect(x: 0, y: frame.height, width: frame.width, height: CGFloat(lineWidth)))
        aLblSaprater.backgroundColor = lineColor
        
        addSubview(aLblSaprater)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        guard !isLineAdded else { return }
        
        isLineAdded = true
        
        initSetup()
    }
}
