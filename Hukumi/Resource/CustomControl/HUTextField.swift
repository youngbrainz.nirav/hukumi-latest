//
//  HUTextField.swift
//  Hukumi
//
//  Created by Govind Prajapati on 06/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class HUTextField: UITextField {
    
    @IBInspectable var cornerRadius: Float = 10
    @IBInspectable var borderWidth: Float = 1
    
    required init?(coder: NSCoder) {
        super.init(coder: coder)
        
        initSetup()
    }
    
    func initSetup() {
        
        layer.cornerRadius = CGFloat(cornerRadius)
        layer.borderWidth = CGFloat(borderWidth)
        layer.borderColor = UIColor.white.cgColor
        
        var FontSize =  Common.is_iPhone._x == true ? 18 : 15
        
       
        
        attributedPlaceholder = NSAttributedString(string: placeholder ?? "", attributes: [NSAttributedString.Key.font: Constant.Font.gilroy(size: Float(FontSize), type: .thin) as Any, NSAttributedString.Key.foregroundColor: UIColor.white as Any])
        
        font = Constant.Font.gilroy(size: Float(FontSize), type: .light) ?? .systemFont(ofSize: CGFloat(FontSize))
        textColor = .white
        
        addPadding(.both(20))
    }
}


extension UITextField {

    enum PaddingSide {
        case left(CGFloat)
        case right(CGFloat)
        case both(CGFloat)
    }

    func addPadding(_ padding: PaddingSide) {

        self.leftViewMode = .always
        self.layer.masksToBounds = true


        switch padding {

        case .left(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.leftView = paddingView
            self.rightViewMode = .always

        case .right(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            self.rightView = paddingView
            self.rightViewMode = .always

        case .both(let spacing):
            let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: spacing, height: self.frame.height))
            // left
            self.leftView = paddingView
            self.leftViewMode = .always
            // right
            self.rightView = paddingView
            self.rightViewMode = .always
        }
    }
    
    func addRightImage(_ aImg: UIImage, width: CGFloat) {
        
        let aRightView = UIView(frame: CGRect(x: 0, y: 0, width: width * 2, height: self.frame.height))
        let aRightImage = UIImageView(frame: CGRect(x: width - width/2, y: self.frame.height/2 - width/2, width: width, height: width))
        aRightView.addSubview(aRightImage)
        
        aRightImage.image = aImg
        aRightImage.contentMode = .scaleAspectFit
        
        rightView = aRightView
        rightViewMode = .always
    }
}
