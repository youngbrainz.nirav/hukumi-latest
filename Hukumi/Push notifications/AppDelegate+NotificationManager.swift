//
//  AppDelegate+NotificationManager.swift
//  RocketBazaar
//
//  Created by Keval on 25/03/19.
//  Copyright © 2019 KTPL. All rights reserved.
//

import Foundation

enum NotificationType : String {
    
    // Should redirect to the order details page
    case CHAT_NOTIFICATION = "Chat Notification" //1
  //  case CUSTOMER_INVOICE_GENERATE = "customer_invoice_generate" // 2
   
    
}

enum NavigateTo : String {
    case OrderDetails = "OrderDetails"
    case QuoteDetails = "QuoteDetails"
    case ChatVC = "ChatVC"
}

extension AppDelegate {
    
    func handleNotification(_ userInfo: [String: Any]) {
        
        debugPrint(userInfo)
        
        guard let dictAPS = userInfo["aps"] as? [String : Any] else {
            
           let dict = userInfo["notification"]  as! [String : Any]
            
            if let count = dict["badge"] as? Int
            {
                
                //  UIApplication.shared.applicationIconBadgeNumber = count
                let dictTmp = ["counter":"\(count)"]
                
                NotificationCenter.default.post(name: Notification.Name("notification_counter"), object: nil, userInfo: dictTmp)
                
                
            }
            
            
            
            return
            
        }
        
        
        
      //  let dict = dictAPS["badge"]  as? Int
                   
                   
        if let count = dictAPS["badge"]  as? Int
        {
            
          //  UIApplication.shared.applicationIconBadgeNumber = count
            let dictTmp = ["counter":"\(count)"]
                NotificationCenter.default.post(name: Notification.Name("notification_counter"), object: nil, userInfo: dictTmp)
            
        }
        
        
       
        
        
    }
    
}
