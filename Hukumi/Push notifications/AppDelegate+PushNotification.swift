//
//  AppDelegate+PushNotification.swift
//  RocketBazaar
//
//  Created by Keval on 25/03/19.
//  Copyright © 2019 KTPL. All rights reserved.
//

import Foundation
import Firebase
import FirebaseMessaging
import UserNotifications
import Alamofire

extension AppDelegate {
    
    
    
    func registerForNotifications() {
        let application = UIApplication.shared
        
        Messaging.messaging().delegate = self
       // Messaging.messaging().shouldEstablishDirectChannel = true
        
        // [START register_for_notifications]
        if #available(iOS 10.0, *) {
            // For iOS 10 display notification (sent via APNS)
            UNUserNotificationCenter.current().delegate = self
            
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_,_ in })
        } else {
            let settings: UIUserNotificationSettings =
                UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            application.registerUserNotificationSettings(settings)
        }
        
        application.registerForRemoteNotifications()
    }
    func configureFirebase() {
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(self.tokenRefreshNotification(_:)), name: NSNotification.Name.InstanceIDTokenRefresh, object: nil)
    }
    
    func disconnectFromFcm(){

        //Messaging.messaging().shouldEstablishDirectChannel = false
        
    }
    
    
    
}

extension AppDelegate: UNUserNotificationCenterDelegate {
    
    // [START ios_10_message_handling]
    // Called when a notification is delivered and the app is in foreground
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       willPresent notification: UNNotification,
                                       withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
        
        let userInfo = notification.request.content.userInfo as! [String:Any]
        
        NSLog("Notification received on iOS 10 or greater: %@", userInfo)
        //   callWebServiceGetUnreadCounter()
        
        var notificationType = ""
        
        
        if let type = userInfo["tag"]
        {
            notificationType = "\(type)"
            
            
        }
        
        if notificationType == NotificationType.CHAT_NOTIFICATION.rawValue
        {
           
            
            handleNotification(userInfo)
            
            if isInChatVc == true{
                completionHandler([.badge])
            }
            else
            {
                completionHandler([.alert,.sound,.badge])

            }
            
            startApiCounter()


        }
        else
        {
            
            
            completionHandler([.alert,.sound,.badge])
            
        }
        
        
    }
    
    // Called when an action was selected by the user for a given notification (app was in background)
    @available(iOS 10.0, *)
    public func userNotificationCenter(_ center: UNUserNotificationCenter,
                                       didReceive response: UNNotificationResponse,
                                       withCompletionHandler completionHandler: @escaping () -> Void) {
        
        let userInfo = response.notification.request.content.userInfo as! [String:AnyHashable]
        
        
        var notificationType = ""
        
        
        if let type = userInfo["tag"]
        {
            notificationType = "\(type)"
            
            
        }
        
        if notificationType == NotificationType.CHAT_NOTIFICATION.rawValue
        {
            // module_id
            
            let aAuthenticationStoryboard = UIStoryboard(name: "Home", bundle: nil)
           
            guard let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC else { return }
            
            var userId = ""
            var convo_id = ""
            var senderId = ""
            if let strUId = userInfo["user_name"]
            {
                userId = "\(strUId)"
            }
            
            if let strCId = userInfo["conversation_id"]
            {
                convo_id = "\(strCId)"
            }
            
            if let strCId = userInfo["user_id"]
            {
                senderId = "\(strCId)"
            }
            
            aWelcomeNavigation.isFromRoot = true
            aWelcomeNavigation.senderName = userId
            aWelcomeNavigation.conversationId = convo_id
            aWelcomeNavigation.senderID = senderId
            aWelcomeNavigation.isUserSubscribed = isUserSubscribe
            
            
            window?.rootViewController = aWelcomeNavigation
            navController = aWelcomeNavigation.navigationController
            window?.makeKeyAndVisible()
            
            
        }
        else if notificationType == "2"
        {
            
          //  var strAppointmentID = ""
            
          //  if let appointmentId = userInfo["module_id"]
          //  {
                
//                strAppointmentID = "\(appointmentId)"
//
//                let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                let aVC = storyBoard.instantiateViewController(withIdentifier: "trackAppointmentVC") as! trackAppointmentVC
//
//                aVC.strAppointmentId = strAppointmentID
//
//                aVC.isFromNotification = true
//
//                let navigationController = UINavigationController(rootViewController: aVC)
//                navigationController.navigationBar.isHidden = true
//
//
//                self.window?.rootViewController = navigationController
//                self.window?.makeKeyAndVisible()
                
            }
        
        
       
         
        
        NSLog("Opening app from local notification on iOS 10 or greater")
        print("\n Notification Data :- \n ============ \n \(userInfo) \n ========= \n")
        
        
        
//        UserProfile.sharedInstance.getSavedData()
//                     if( UserProfile.sharedInstance.strID != "" &&  UserProfile.sharedInstance.strMobile != "")
//                     {
//                       let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                       let objPhotosViewController = storyBoard.instantiateViewController(withIdentifier: "ID_NotificationViewController") as! NotificationViewController
//                        objPhotosViewController.isFromLaunch = true
//                        
//                        
//                        let navigationController = UINavigationController(rootViewController: objPhotosViewController)
//                        navigationController.navigationBar.isHidden = true
//                        
//                       // let appdelegate = UIApplication.shared.delegate as! AppDelegate
//                       // appdelegate.window!.rootViewController = navigationController
//                   
//                        self.window?.rootViewController = navigationController
//                        self.window?.makeKeyAndVisible()
//                        
//                        
//                        
//                      //  self.window?.rootViewController(objPhotosViewController, animated: true)
//                       
//                       
//               }
        
        
       // handleNotification(userInfo as! [String : Any])
        
        // Received notification
        
        
          completionHandler()
        
    }
    
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any]) {
      // If you are receiving a notification message while your app is in the background,
      // this callback will not be fired till the user taps on the notification launching the application.
      // TODO: Handle data of notification

      // With swizzling disabled you must let Messaging know about the message, for Analytics
      // Messaging.messaging().appDidReceiveMessage(userInfo)

      // Print message ID.
     

      // Print full message.
      print(userInfo)
        
        if application.applicationState == .active ||  application.applicationState == .background
        {
            
            
            print("notification recieved in didReceiveRemoteNotification while app is in bg or foreground")
            
//            if  application.applicationState == .active
//            {
//                return
//            }
            
            
            var notificationType = ""
            
            
            if let type = userInfo["notification_type"]
            {
                notificationType = "\(type)"
                
                
            }
            
            
            
//            if notificationType == "1" && isInAppointmentDetailVC == true
//            {
//
//
//                var strAppointmentID = ""
//
//                // module_id
//                if let appointmentId = userInfo["module_id"]
//                {
//
//                    strAppointmentID = "\(appointmentId)"
//                    strAppointmentID = "\(appointmentId)"
//
//                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                    let aVC = storyBoard.instantiateViewController(withIdentifier: "AptDetailFinsishNAcceptVC") as! AptDetailFinsishNAcceptVC
//
//                    aVC.AppointmentID = strAppointmentID
//                    aVC.isFromNotification = true
//
//
//                    let navigationController = UINavigationController(rootViewController: aVC)
//                    navigationController.navigationBar.isHidden = true
//
//
//                    self.window?.rootViewController = navigationController
//                    self.window?.makeKeyAndVisible()
//
//                }
//
//                return
//
//
//
//
//            }
            
           
        }
        else
        {
            
            var notificationType = ""
            
            
            if let type = userInfo["notification_type"]
            {
                notificationType = "\(type)"
                
                
            }
            
            
            
            if notificationType == "1"
            {
                // module_id
                
                
//                var strAppointmentID = ""
//
//                if let appointmentId = userInfo["module_id"]
//                {
//
//                    strAppointmentID = "\(appointmentId)"
//
//                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//
//                    let aVC = storyBoard.instantiateViewController(withIdentifier: "AptDetailFinsishNAcceptVC") as! AptDetailFinsishNAcceptVC
//
//                    aVC.AppointmentID = strAppointmentID
//                    aVC.isFromNotification = true
//
//
//                    let navigationController = UINavigationController(rootViewController: aVC)
//                    navigationController.navigationBar.isHidden = true
//
//
//                    self.window?.rootViewController = navigationController
//                    self.window?.makeKeyAndVisible()
//
//                }
                
                
                
                
            }
            else if notificationType == "2"
            {
                
//                var strAppointmentID = ""
//
//                if let appointmentId = userInfo["module_id"]
//                {
//
//                    strAppointmentID = "\(appointmentId)"
//
//                    let storyBoard = UIStoryboard(name: "Main", bundle: nil)
//                    let aVC = storyBoard.instantiateViewController(withIdentifier: "trackAppointmentVC") as! trackAppointmentVC
//
//                    aVC.strAppointmentId = strAppointmentID
//
//                    aVC.isFromNotification = true
//
//                    let navigationController = UINavigationController(rootViewController: aVC)
//                    navigationController.navigationBar.isHidden = true
//
//
//                    self.window?.rootViewController = navigationController
//                    self.window?.makeKeyAndVisible()
//
//                }
            }
            
            
        }
        
        UIApplication.shared.applicationIconBadgeNumber = 0

    }
    
    
    
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    // [START ios_10_data_message_handling]
    // Receive data message on iOS 10 devices while app is in the foreground.
    
//    public func application(received remoteMessage: MessagingRemoteMessage) {
//
//        NSLog("Data message received on iOS 10 or greater from remoteMessage: %@", remoteMessage.appData)
//
//        handleNotification(remoteMessage.appData as! [String : Any])
//
//    }
    
}

extension AppDelegate: MessagingDelegate {
    
    // [START refresh_token]
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        
        // self.sendDeviceTokenToServer(data: deviceToken)
        let tokenParts = deviceToken.map { data -> String in
            return String(format: "%02.2hhx", data)
        }
        
        let token = tokenParts.joined()
        
        NSLog("My Apns registration token: \(token)")
        
        
        UserDefaults.standard.set(deviceToken, forKey: "ApnsDeviceToken")
        UserDefaults.standard.synchronize()
        
    }
    
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String?) {
        
        NSLog("Firebase registration token: \(fcmToken)")
        // Set firebase token to the user default
        self.setDeviceTokenToUserDefaults(strToken: fcmToken ?? "abcd1234")
        
    }
    
    // [START ios_10_data_message]
    // Receive data messages on iOS 10+ directly from FCM (bypassing APNs) when the app is in the foreground.
    
//    func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage) {
//
//        NSLog("Received data message from FCM (bypassing APNs): %@", remoteMessage.appData)
//        // Received a notification
//
//       // handleNotification(remoteMessage.appData as! [String : Any])
//
//    }
    
    
    
    @objc func tokenRefreshNotification(_ notification: Notification) {
        
        connectToFCM()
        
        InstanceID.instanceID().instanceID { (result, error) in
            if let error = error {
                print("Error fetching remote instange ID: \(error)")
            } else if let result = result {
                print("Remote instance ID token: \(result.token)")
                // Set a firebase token
                self.setDeviceTokenToUserDefaults(strToken: result.token)
                
            }
            
        }
    }
    
    func connectToFCM() {
      //  Messaging.messaging().shouldEstablishDirectChannel = true
    }
 
    
}

extension AppDelegate {
    
    func setDeviceTokenToUserDefaults(strToken : String) {
        let defaults = UserDefaults.standard
        defaults.setValue(strToken, forKey: "DEVICE_TOKEN")
        defaults.synchronize()
    }
    
    static func getDeviceTokenFromUserDefaults() -> String? {
        let defaults = UserDefaults.standard
        if let strToken = defaults.string(forKey: "DEVICE_TOKEN") {
            return strToken
        } else {
            return ""
        }
        
    }
    
    
    
}




