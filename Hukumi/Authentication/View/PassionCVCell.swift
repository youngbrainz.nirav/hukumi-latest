//
//  PassionCVCell.swift
//  Hukumi
//
//  Created by Govind Prajapati on 07/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class PassionCVCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBackground: UIView!
}
