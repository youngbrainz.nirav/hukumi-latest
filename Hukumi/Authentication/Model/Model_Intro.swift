//
//  Model_Intro.swift
//  Hukumi
//
//  Created by hiren  mistry on 27/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON
class Model_Intro: NSObject {
    var _id:String = ""
    var banner_image:String = ""
    var banner_text:String = ""
    
    
    init(obj:JSON) {
        if let x = obj["_id"].string{
           _id = x
        }
        if let x = obj["banner_image"].string{
           banner_image = x
        }
        if let x = obj["banner_text"].string{
           banner_text = x
        }
    }
    
}
