//
//  ModelRegister.swift
//  Hukumi
//
//  Created by hiren  mistry on 28/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit

class ModelRegister: NSObject {
    var DOB:String = ""
    var access_type:String = ""
    var career_experiance:String = ""
    var city:String = ""
    var country_code:String = ""
    var arr_images:[UIImage] = [UIImage]()
    var device_token:String = ""
    var device_type:String = ""
    var education_level:String = ""
    var email_id:String = ""
    var gender:String = ""
    var lattitude:String = ""
    var location:String = ""
    var longitude:String = ""
    var mobile_no:String = ""
    var passions:String = ""
    var password:String = ""
    var profile_image:UIImage = UIImage()
    var user_address:String = ""
    var user_name:String = ""
    var state:String = ""
    var country:String = ""

}
