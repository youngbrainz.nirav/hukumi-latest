//
//  Model_Passion.swift
//  Hukumi
//
//  Created by hiren  mistry on 27/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON

class Model_Passion: NSObject {
    var _id:String = ""
    var name:String = ""
    var status:String = ""
    var created_date:String = ""
    var month:String = ""
    var month_name:String = ""
    var year:String = ""
    var modified_date:String = ""

    init(obj:JSON) {
        if let x = obj["_id"].string {
            _id = x
        }
        if let x = obj["name"].string {
            name = x
        }
        if let x = obj["status"].string {
            status = x
        }
        if let x = obj["created_date"].string {
            created_date = x
        }
        if let x = obj["month"].string {
            month = x
        }
        if let x = obj["month_name"].string {
            month_name = x
        }
        if let x = obj["year"].string {
            year = x
        }
        if let x = obj["modified_date"].string {
            modified_date = x
        }
    }
}
