
import UIKit
import SwiftyJSON

private var _sharedUser = UserInfo()
let kCurrentUser = UserInfo.sharedInstance

class UserInfo: UserModel {
    
    /* Use string to save to user default */
    let userDefaultKey = "user_infomation"
    
    var session_token:String?
    var email_id:String?
    var device_type:String?
    var device_token:String?
    var user_name:String?
    var password:String?
    var gender:String?
    var DOB:String?
    var phone_number:String?
    var country_code:String?
    var mobile_no:String?
    var created_date:String?
    var month:String?
    var month_name:String?
    var year:String?
    var modified_date:String?
    var user_status:String?
    var online_status:String?
    var profile_image:String?
    var notification_status:String?
    var email_status:String?
    var message_status:String?
    var lattitude:String?
    var longitude:String?
    var user_address:String?
    var passions:String?
    var education_level:String?
    var city:String?
    var intro_message:String?
    var career_experiance:String?
    var user_id:String?
    
    override init() {
        super.init()
    }
    override init(info: JSON) {
        super.init(info: info)
    }

    class var sharedInstance: UserInfo {
        if _sharedUser.user_id == nil {
            _sharedUser.loadFromDefault()
        }
        return _sharedUser
    }
    
    /* update from json, use when login or sign up */
    override func update(info: JSON)
    {
        print("this method calls",info)
        if let x = info["session_token"].string{
            session_token = x
        }
        if let x = info["email_id"].string{
            email_id = x
        }
        if let x = info["device_type"].string{
            device_type = x
        }
        if let x = info["device_token"].string{
            device_token = x
        }
        if let x = info["user_name"].string{
            user_name = x
        }
        if let x = info["password"].string{
            password = x
        }
        if let x = info["gender"].string{
            gender = x
        }
        if let x = info["DOB"].string{
            DOB = x
        }
        if let x = info["phone_number"].string{
            phone_number = x
        }
        if let x = info["country_code"].string{
            country_code = x
        }
        if let x = info["mobile_no"].string{
            mobile_no = x
        }
        if let x = info["created_date"].string{
            created_date = x
        }
        if let x = info["month"].string{
            month = x
        }
        if let x = info["month_name"].string{
            month_name = x
        }
        if let x = info["year"].string{
            year = x
        }
        if let x = info["modified_date"].string{
            modified_date = x
        }
        if let x = info["user_status"].string{
            user_status = x
        }
        if let x = info["online_status"].string{
            online_status = x
        }
        if let x = info["profile_image"].string{
            profile_image = x
        }
        if let x = info["notification_status"].string{
            notification_status = x
        }
        if let x = info["email_status"].string{
            email_status = x
        }
        if let x = info["message_status"].string{
            message_status = x
        }
        if let x = info["lattitude"].string{
            lattitude = x
        }
        if let x = info["longitude"].string{
            longitude = x
        }
        if let x = info["user_address"].string{
            user_address = x
        }
        if let x = info["passions"].string{
            passions = x
        }
        if let x = info["education_level"].string{
            education_level = x
        }
        if let x = info["city"].string{
            city = x
        }
        if let x = info["intro_message"].string{
            intro_message = x
        }
        if let x = info["career_experiance"].string{
            career_experiance = x
        }
        if let x = info["user_id"].string{
            user_id = x
        }
        saveToDefault()
    }
    
    /* save to default */
    func saveToDefault() {
        
        let userDefault = UserDefaults.standard
        //   print(dictionaryFromInfos())
        userDefault.set(dictionaryFromInfos(), forKey: userDefaultKey)
        userDefault.synchronize()
    }
    /* save all infos to a dictionary */
    func dictionaryFromInfos() -> [String: AnyObject] {
        var dicinfo = [String: AnyObject]()
        dicinfo["session_token"] = session_token as AnyObject?
        dicinfo["email_id"] = email_id as AnyObject?
        dicinfo["device_type"] = device_type as AnyObject?
        dicinfo["device_token"] = device_token as AnyObject?
        dicinfo["user_name"] = user_name as AnyObject?
        dicinfo["password"] = password as AnyObject?
        dicinfo["gender"] = gender as AnyObject?
        dicinfo["DOB"] = DOB as AnyObject?
        dicinfo["phone_number"] = phone_number as AnyObject?
        dicinfo["country_code"] = country_code as AnyObject?
        dicinfo["mobile_no"] = mobile_no as AnyObject?
        dicinfo["created_date"] = created_date as AnyObject?
        dicinfo["month"] = month as AnyObject?
        dicinfo["month_name"] = month_name as AnyObject?
        dicinfo["year"] = year as AnyObject?
        dicinfo["modified_date"] = modified_date as AnyObject?
        dicinfo["user_status"] = user_status as AnyObject?
        dicinfo["online_status"] = online_status as AnyObject?
        dicinfo["profile_image"] = profile_image as AnyObject?
        dicinfo["notification_status"] = notification_status as AnyObject?
        dicinfo["email_status"] = email_status as AnyObject?
        dicinfo["message_status"] = message_status as AnyObject?
        dicinfo["lattitude"] = lattitude as AnyObject?
        dicinfo["longitude"] = longitude as AnyObject?
        dicinfo["user_address"] = user_address as AnyObject?
        dicinfo["passions"] = passions as AnyObject?
        dicinfo["education_level"] = education_level as AnyObject?
        dicinfo["city"] = city as AnyObject?
        dicinfo["intro_message"] = intro_message as AnyObject?
        dicinfo["career_experiance"] = career_experiance as AnyObject?
        dicinfo["user_id"] = user_id as AnyObject?
        
        return dicinfo
    }
    /* load from default */
    func loadFromDefault() {
        let userDefault = UserDefaults.standard
        if let dicInfo = userDefault.object(forKey: userDefaultKey) as? [String: AnyObject] {
            update(dictionaryInfo: dicInfo)
        }
    }
    
    /* update from dictionary, use when load from user default */
    func update(dictionaryInfo: [String: AnyObject]) {
        
        func boolFromDictionary(dicInfo: [String: AnyObject], key: String) -> Bool {
            if let optionalValue = dicInfo[key] as? Bool {
                return optionalValue
            } else {
                return false
            }
        }
        
        session_token = dictionaryInfo["session_token"] as? String
        email_id = dictionaryInfo["email_id"] as? String
        device_type = dictionaryInfo["device_type"] as? String
        device_token = dictionaryInfo["device_token"] as? String
        user_name = dictionaryInfo["user_name"] as? String
        password = dictionaryInfo["password"] as? String
        gender = dictionaryInfo["gender"] as? String
        DOB = dictionaryInfo["DOB"] as? String
        phone_number = dictionaryInfo["phone_number"] as? String
        country_code = dictionaryInfo["country_code"] as? String
        mobile_no = dictionaryInfo["mobile_no"] as? String
        created_date = dictionaryInfo["created_date"] as? String
        month = dictionaryInfo["month"] as? String
        month_name = dictionaryInfo["month_name"] as? String
        year = dictionaryInfo["year"] as? String
        modified_date = dictionaryInfo["modified_date"] as? String
        user_status = dictionaryInfo["user_status"] as? String
        online_status = dictionaryInfo["online_status"] as? String
        profile_image = dictionaryInfo["profile_image"] as? String
        notification_status = dictionaryInfo["notification_status"] as? String
        email_status = dictionaryInfo["email_status"] as? String
        message_status = dictionaryInfo["message_status"] as? String
        lattitude = dictionaryInfo["lattitude"] as? String
        longitude = dictionaryInfo["longitude"] as? String
        user_address = dictionaryInfo["user_address"] as? String
        passions = dictionaryInfo["passions"] as? String
        education_level = dictionaryInfo["education_level"] as? String
        city = dictionaryInfo["city"] as? String
        intro_message = dictionaryInfo["intro_message"] as? String
        career_experiance = dictionaryInfo["career_experiance"] as? String
        user_id = dictionaryInfo["user_id"] as? String
    }
    /* save setting */
    
    func logOut()
    {
        let userDefault = UserDefaults.standard
        userDefault.removeObject(forKey: userDefaultKey)
        userDefault.removeObject(forKey: "TOKEN")
        session_token = ""
        email_id = ""
        device_type = ""
        device_token = ""
        user_name = ""
        password = ""
        gender = ""
        DOB = ""
        phone_number = ""
        country_code = ""
        mobile_no = ""
        created_date = ""
        month = ""
        month_name = ""
        year = ""
        modified_date = ""
        user_status = ""
        online_status = ""
        profile_image = ""
        notification_status = ""
        email_status = ""
        message_status = ""
        lattitude = ""
        longitude = ""
        user_address = ""
        passions = ""
        education_level = ""
        city = ""
        intro_message = ""
        career_experiance = ""
        user_id = ""
    }
    class func clearInstance()
    {
        _sharedUser = UserInfo()
    }
}

