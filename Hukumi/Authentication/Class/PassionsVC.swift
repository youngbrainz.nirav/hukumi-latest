//
//  PassionsVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 07/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON
class PassionsVC: MasterVC {
    
    @IBOutlet weak var cvPassion: HUPassionCollectionView!
    var objRegister:ModelRegister = ModelRegister()

    override func viewDidLoad() {
        super.viewDidLoad()
        cvPassion.loadPassion([Model_Passion]())
        self.GetPassionList()
    }
    
    
    @IBAction func btnLetsGoAction() {
        if cvPassion.selectedPassions.count > 3{
            
            let passion = cvPassion.arrSelectedPassions.map{$0._id}
            let characterArray = passion.flatMap { $0 }
            let stringArray2 = characterArray.map { String($0) }
            let string = stringArray2.joined(separator: ", ")
            self.objRegister.passions = string
            self.CallSignUp()
            
        }else{
            Common().showAlert(strMsg: "Please select atleast 3 passion.", view: self)
        }
    }
}
//MARK:- API Calling

extension PassionsVC{
    func GetPassionList(){
        
        Networking.performApiCall(.getPassionList(["":""]), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.cvPassion.arrPassions.removeAll()
                        if let ArrData = dictData["data"] as? [[String:Any]]{
                            for obj in ArrData{
                                self.cvPassion.arrPassions.append(Model_Passion(obj: JSON(obj)))
                            }
                            self.cvPassion.loadPassion(self.cvPassion.arrPassions)
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
    func CallSignUp(){
        var param:[String:Any] = [String:Any]()
        param["DOB"] = self.objRegister.DOB
        param["access_type"] = "1"
        param["career_experiance"] = self.objRegister.career_experiance
        param["city"] = self.objRegister.city
        param["country_code"] = self.objRegister.country_code
      //  param["device_token"] = DELEGATE.strDeviceToken
        param["device_token"] = AppDelegate.getDeviceTokenFromUserDefaults() ?? "1234"

        param["device_type"] = "1"
        param["education_level"] = self.objRegister.education_level
        param["email_id"] = self.objRegister.email_id
        param["gender"] = self.objRegister.gender
        param["lattitude"] = self.objRegister.lattitude
        param["location"] = self.objRegister.location
        param["longitude"] = self.objRegister.longitude
        param["mobile_no"] = self.objRegister.mobile_no
        param["passions"] = self.objRegister.passions
        param["password"] = self.objRegister.password
        param["user_address"] = self.objRegister.location
        param["user_name"] = self.objRegister.user_name
        
        
        Networking.uploadImagesWithParams(.registerUser(param), profile_image: self.objRegister.profile_image, imageDic: self.objRegister.arr_images, dictParams: param, showHud: true,onView:self) { (res) in
            
            switch res {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    if let dictData = response.result.value as? [String:Any]{
                        if let status = dictData["status"] as? Int {
                            if status == 1{
                                if let data = dictData["data"] as? [String:Any]{
                                    
                                    isFromSignUp = true
                                    kCurrentUser.update(info: JSON(data))
                                    let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }else{
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                            }
                        }else{
                            Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                        }
                    }
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                Common().showAlert(strMsg: "\(encodingError)", view: self)
            }
            
            
        }
    }
}
