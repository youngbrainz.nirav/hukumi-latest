//
//  ForgotPasswordVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 02/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class ForgotPasswordVC: MasterVC {

    //MARK:- outlets
    
    
    @IBOutlet weak var txtEmail: HUTextField!
    
    
    
    //MARK:- view did load
    override func viewDidLoad() {
        super.viewDidLoad()
        
        

    }
    
    @IBAction func actionSend(_ sender: Any) {
        
        
        if txtEmail.text == ""{
             Common().showAlert(strMsg: "Please enter email.", view: self)

        }
        else if !Common().isValidEmail(email: txtEmail.text!){
             Common().showAlert(strMsg: "Please enter valid email.", view: self)

        }
        else
        {
            self.callForgetPwdApi()
        }
    }
    
    
    
    //MARK:- api call
    func callForgetPwdApi(){
        
        var param:[String:Any] = [String:Any]()

        param["email_id"] = txtEmail.text
        
        print("\n ========= \n forget pwd api params \n ======= \n \(param)")

        
        Networking.performApiCall(.forgetPwd(param), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                
                print("\n ========= \n forget pwd api responce \n ======= \n \(dictData)")

                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        
                        
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                        self.navigationController?.popViewController(animated: true)
                        
                        
                    }
                    else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
   

}
