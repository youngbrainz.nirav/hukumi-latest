//
//  EmailDialogueVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 07/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
protocol emailDialogueDelegate {
    func SentOTP(str:String)
}
class EmailDialogueVC: UIViewController {
    
    @IBOutlet weak var viewDialogue: UIView!
    var delegate:emailDialogueDelegate?
    var objRegister:ModelRegister = ModelRegister()
    var strOTP = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        initSetup()
    }
    
    func initSetup() {
        
        viewDialogue.layer.cornerRadius = 10
        viewDialogue.layer.borderWidth = 1
        viewDialogue.layer.borderColor = UIColor.white.cgColor
    }
    
    @IBAction func btnSendAction() {
     
        remove()
    }
    
    @IBAction func btnCloseAction() {
     
        remove()
    }
    
    func remove() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0

        }) { (tr) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }

        
    }
    func CallGetOtp(){
        var param:[String:Any] = [String:Any]()
        param["email_id"] = objRegister.email_id
        param["verification_type"] = "2"
        Networking.performApiCall(.sendOTP(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.strOTP = dictData["data"] as? String ?? ""
                        self.delegate?.SentOTP(str: self.strOTP)
                        self.remove()
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "Something went wrong!", view: self)
                    }
                }
            }
        }
        
    }
}

