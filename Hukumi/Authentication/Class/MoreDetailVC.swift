//
//  MoreDetailVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 03/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import DropDown
import SwiftyJSON
import GooglePlaces
import GoogleMaps
import GooglePlacePicker

class MoreDetailVC: MasterVC,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate {
    
    var objRegister:ModelRegister = ModelRegister()

    @IBOutlet weak var txtIntroMessage: HUTextField!
    @IBOutlet weak var txtEducation: HUTextField!
    @IBOutlet weak var txtLocation: HUTextField!
    var ArrEducation:[Model_EducationList] = [Model_EducationList(obj: JSON(["":""]))]
    let startDayDropDown = DropDown()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.txtEducation.delegate = self
        self.txtLocation.delegate = self

        initSetup()
        GetEducationList()
    }
    
    func initSetup() {
        txtLocation.addRightImage(#imageLiteral(resourceName: "gps"), width: 20)
        txtEducation.addRightImage(#imageLiteral(resourceName: "Dropdown white"), width: 20)
    }
    func SetupDropdown(){
        startDayDropDown.dismissMode = .onTap
        //self.btnBG.isHidden = false
        startDayDropDown.dataSource = ArrEducation.map{$0.name}
        
        startDayDropDown.width = self.txtEducation.frame.size.width
        startDayDropDown.anchorView = self.txtEducation
        startDayDropDown.selectionAction = {
            (index: Int, item: String) in
          //  self.DisplayData()
            self.objRegister.education_level = self.ArrEducation[index]._id
            self.txtEducation.text = item
        }
        
        startDayDropDown.show()
       // startDayDropDown.bringSubviewToFront(self.btnBG)

    }
    @IBAction func btnNextAction() {
        if validateTexts(){
            let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "PassionsVC") as! PassionsVC
            self.objRegister.career_experiance = self.txtEducation.text!
            self.objRegister.location = self.txtLocation.text!
            vc.objRegister = self.objRegister
            self.navigationController?.pushViewController(vc, animated: true)
        }
    }

    func validateTexts() -> Bool{
        if txtEducation.text == ""{
            Common().showAlert(strMsg: "Please enter education.", view: self)
            return false
        }
        
        if txtEducation.text == ""{
            Common().showAlert(strMsg: "Please enter education.", view: self)
            return false
        }
        if txtEducation.text == ""{
            Common().showAlert(strMsg: "Please enter education.", view: self)
            return false
        }
        return true
    }
    //MARK: - GMSAutocompleteViewController Delegate -
    func tapLocation(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.objRegister.lattitude = "\(place.coordinate.latitude)"
        self.objRegister.longitude = "\(place.coordinate.longitude)"
        self.objRegister.location = place.formattedAddress!
        self.objRegister.user_address = place.formattedAddress!
        
                if place.addressComponents != nil {
                    for component in place.addressComponents! {
                        if component.type == "city" {
                        print(component.name)
                            self.objRegister.city = component.name
                        }
                        if component.type == "locality" {
                        print(component.name)
                            self.objRegister.city = component.name
                        }
                    }
                }
        
        
        
        print(place)
        txtLocation.text = place.formattedAddress!
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    //MARK:-
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtEducation{
            self.SetupDropdown()
            return false
        }
        if textField == txtLocation {
            self.tapLocation()
            return false
        }
        return true
    }
    
    func GetEducationList(){
        
        Networking.performApiCall(.getEducationList(["":""]), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.ArrEducation.removeAll()
                        if let ArrData = dictData["data"] as? [[String:Any]]{
                            for obj in ArrData{
                                self.ArrEducation.append(Model_EducationList(obj: JSON(obj)))
                            }
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
}
