//
//  OtpVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 03/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import OTPFieldView

class OtpVC: MasterVC,OTPFieldViewDelegate,emailDialogueDelegate {
    
    func shouldBecomeFirstResponderForOTP(otpTextFieldIndex index: Int) -> Bool {
        return false
    }
    
    
    
    func hasEnteredAllOTP(hasEnteredAll: Bool) -> Bool {
        return true
    }
    

    @IBOutlet weak var txtOtpField: OTPFieldView!
    var strOTP:String = ""
    var strEnteredOTP = ""
    var objRegister:ModelRegister = ModelRegister()

    @IBOutlet weak var txtAuth: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        initSetup()
        
        self.CallGetOtp()
    }
    
    func initSetup() {
        
        txtOtpField.cursorColor = .white
        txtOtpField.defaultBorderColor = .white
        txtOtpField.displayType = .roundedCorner
        txtOtpField.fieldFont = Constant.Font.gilroy(size: 20, type: .bold) ?? .systemFont(ofSize: 20)
        txtOtpField.filledBorderColor = .white
        txtOtpField.tintColor = .white
        txtOtpField.delegate = self
        txtOtpField.isUserInteractionEnabled = true
        txtOtpField.becomeFirstResponder()
        txtOtpField.initializeUI()
        
        txtOtpField.subviews.compactMap({ $0 as? UITextField }).forEach({ $0.textColor = .white; $0.layer.cornerRadius = 10 })
    }
    func enteredOTP(otp: String) {
        self.strEnteredOTP = otp
    }
    func SentOTP(str: String) {
        self.strOTP = str
    }
    @IBAction func btnVerifyAction() {
        if self.txtAuth.text != ""{
            if self.txtAuth.text == self.strOTP{
                let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "CompleteProfileVC") as! CompleteProfileVC
                vc.objRegister = self.objRegister
                self.navigationController?.pushViewController(vc, animated: true)
            }else{
                Common().showAlert(strMsg: "Please enter valid OTP.", view: self)
            }
        }else{
            Common().showAlert(strMsg: "Please enter OTP.", view: self)
        }
      //  performSegue(withIdentifier: "CompleteProfileVCSegue", sender: nil)
    }
    
    @IBAction func btnCodeAction() {
        
        guard let aEmailVC = storyboard?.instantiateViewController(identifier: "EmailDialogueVC") as? EmailDialogueVC else { return }
        
        aEmailVC.view.alpha = 0
        aEmailVC.view.frame = view.bounds
        aEmailVC.delegate = self
        view.addSubview(aEmailVC.view)
        
        UIView.animate(withDuration: 0.3, animations: {
            aEmailVC.view.alpha = 1

        }) { (tr) in
            self.addChild(aEmailVC)
        }
    }

}

//MARK:- API Call
extension OtpVC{
    func CallGetOtp(){
        var param:[String:Any] = [String:Any]()
        param["mobile_no"] = objRegister.country_code + objRegister.mobile_no
        param["verification_type"] = "1"
        Networking.performApiCall(.sendOTP(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.strOTP = dictData["data"] as? String ?? ""
                        print("\n ========= \n otp screen \n \(dictData["data"] as? String ?? "") \n ======== ")
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "Something went wrong!", view: self)
                    }
                }else{
                    Common().showAlert(strMsg: dictData["msg"] as? String ?? "Something went wrong!", view: self)
                }
            }
        }
        
    }
}
