//
//  CompleteProfileVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 07/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import DatePickerDialog
import GooglePlaces
import GoogleMaps
import GooglePlacePicker
import SwiftyJSON

class CompleteProfileVC: MasterVC,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate {
    @IBOutlet weak var btnProfile3: HUImagePickerButton!
    @IBOutlet weak var btnFemale: HURadioButton!
    
    @IBOutlet weak var btnYear: HUBorderButton!
    @IBOutlet weak var btnMonth: HUBorderButton!
    @IBOutlet weak var btnDate: HUBorderButton!
    @IBOutlet weak var btnOther: HURadioButton!
    @IBOutlet weak var btnMale: HURadioButton!
    @IBOutlet weak var btnProfile5: HUImagePickerButton!
    @IBOutlet weak var btnProfile4: HUImagePickerButton!
    @IBOutlet weak var btnProfile2: HUImagePickerButton!
    @IBOutlet weak var btnProfile1: HUImagePickerButton!
    @IBOutlet weak var btnUploadProfile: HUImagePickerButton!
    var objRegister:ModelRegister = ModelRegister()
    var selectedImage:Int = 0
    var imagePicker = UIImagePickerController()
    var DOB = ""
    var profileImage : UIImage!
    var ArrProfileImage:[UIImage] = [UIImage]()
    
    
    @IBOutlet weak var txtLocation: HUTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        ArrProfileImage.append(UIImage(named: "Upload Image")!)
        ArrProfileImage.append(UIImage(named: "Upload Image")!)
        ArrProfileImage.append(UIImage(named: "Upload Image")!)
        ArrProfileImage.append(UIImage(named: "Upload Image")!)
        ArrProfileImage.append(UIImage(named: "Upload Image")!)

        self.txtLocation.delegate = self
        txtLocation.addRightImage(#imageLiteral(resourceName: "gps"), width: 20)

        
        // Do any additional setup after loading the view.
    }
    //MARK:- Button Click
    
    @IBAction func btnGenderClick(_ sender: HURadioButton) {
        btnMale.isSelected = false
        btnFemale.isSelected = false
        btnOther.isSelected = false
        sender.isSelected = true
    }
    
    @IBAction func btnAddProfileImageClick(_ sender: HUBorderButton) {
        self.selectedImage = sender.tag
        self.OpenAlertForImagePicker()
    }
    @IBAction func btnUploadProfile_Click(_ sender: HUBorderButton) {
        self.selectedImage = 0
        self.OpenAlertForImagePicker()
    }
    @IBAction func btnDateClick(_ sender: HUBorderButton) {
        self.OpenDatePicker()
    }
    @IBAction func btnNextAction() {
        if self.ValidateFields(){
         
            self.objRegister.passions = ""
            self.objRegister = self.FillModelClass()
            self.CallSignUp()

            
            
//            let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "MoreDetailVC") as! MoreDetailVC
//            vc.objRegister = self.FillModelClass()
//            self.navigationController?.pushViewController(vc, animated: true)
//
            
            
        }
    }
    
    
    //MARK:- Other Functions
    func ValidateFields() -> Bool{
        
        if self.profileImage == nil{
            Common().showAlert(strMsg: "Please select profile image.", view: self)
            return false

        }
//        if self.ArrProfileImage.filter{$0 != UIImage(named:"Upload Image")}.count < 1{
//            Common().showAlert(strMsg: "Please select atleast 1 image.", view: self)
//            return false
//
//        }
        if !self.btnMale.isSelected && !self.btnFemale.isSelected && !self.btnOther.isSelected{
            Common().showAlert(strMsg: "Please select gender.", view: self)
            return false

        }
        if self.DOB == ""{
            Common().showAlert(strMsg: "Please enter DOB.", view: self)
            return false
        }
        if self.txtLocation.text == ""{
            Common().showAlert(strMsg: "Please select location.", view: self)
            return false
        }
        return true
        
    }
    func FillModelClass() -> ModelRegister{
        self.objRegister.profile_image = self.profileImage
        self.objRegister.arr_images = self.ArrProfileImage.filter{$0 != UIImage(named:"Upload Image")}
        if self.btnMale.isSelected{
            self.objRegister.gender = "1"
        }else if self.btnFemale.isSelected{
            self.objRegister.gender = "2"
        }else if self.btnOther.isSelected{
            self.objRegister.gender = "3"
        }
        self.objRegister.DOB = self.DOB
        return self.objRegister
    }
    func OpenAlertForImagePicker(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

        self.present(alert, animated: true, completion: nil)

    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    
    func OpenDatePicker(){
        
        let calendar = Calendar(identifier: .gregorian)

            let currentDate = Date()
            var components = DateComponents()
            components.calendar = calendar

            components.year = -18
            components.month = 12
            let minDate = calendar.date(byAdding: components, to: currentDate)!
        
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", maximumDate: minDate, datePickerMode: .date) { date in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let Arrdate = formatter.string(from: dt)
                self.objRegister.DOB = Arrdate
                self.DOB = Arrdate
                var dates = Arrdate.split{$0 == "-"}.map(String.init)
                self.btnDate.setTitle(dates[0], for: .normal)
                self.btnMonth.setTitle(dates[1], for: .normal)
                self.btnYear.setTitle(dates[2], for: .normal)
            }
            
        }
    }
    
    //MARK:- Imagepicker delegate
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {

        if let pickedImage = info[.editedImage] as? UIImage {

            if selectedImage == 100{
                self.btnProfile1.setImage(pickedImage, for: .normal)
                self.ArrProfileImage[0] = pickedImage
            }else if selectedImage == 101{
                self.ArrProfileImage[1] = pickedImage
                self.btnProfile2.setImage(pickedImage, for: .normal)
            }else if selectedImage == 102{
                self.ArrProfileImage[2] = pickedImage
                self.btnProfile3.setImage(pickedImage, for: .normal)
            }else if selectedImage == 103{
                self.ArrProfileImage[3] = pickedImage
                self.btnProfile4.setImage(pickedImage, for: .normal)
            }else if selectedImage == 104{
                self.ArrProfileImage[4] = pickedImage
                self.btnProfile5.setImage(pickedImage, for: .normal)
            }else if selectedImage == 0{
                self.btnUploadProfile.setImage(pickedImage, for: .normal)
                self.profileImage = pickedImage
            }
        
        }
        else if let pickedImage = info[.originalImage] as? UIImage {


            if selectedImage == 100{
                self.btnProfile1.setImage(pickedImage, for: .normal)
                self.ArrProfileImage[0] = pickedImage
            }else if selectedImage == 101{
                self.ArrProfileImage[1] = pickedImage
                self.btnProfile2.setImage(pickedImage, for: .normal)
            }else if selectedImage == 102{
                self.ArrProfileImage[2] = pickedImage
                self.btnProfile3.setImage(pickedImage, for: .normal)
            }else if selectedImage == 103{
                self.ArrProfileImage[3] = pickedImage
                self.btnProfile4.setImage(pickedImage, for: .normal)
            }else if selectedImage == 104{
                self.ArrProfileImage[4] = pickedImage
                self.btnProfile5.setImage(pickedImage, for: .normal)
            }else if selectedImage == 0{
                self.btnUploadProfile.setImage(pickedImage, for: .normal)
                self.profileImage = pickedImage
            }
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    //MARK: - GMSAutocompleteViewController Delegate -
    func tapLocation(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        self.objRegister.lattitude = "\(place.coordinate.latitude)"
        self.objRegister.longitude = "\(place.coordinate.longitude)"
        self.objRegister.location = place.formattedAddress!
        self.objRegister.user_address = place.formattedAddress!
        
        print(place)
        
        if place.addressComponents != nil {
            for component in place.addressComponents! {
                if component.type == "city" {
                print(component.name)
                    self.objRegister.city = component.name
                }
                
                
                if component.type == "locality" {
                print(component.name)
                    self.objRegister.city = component.name
                }
                
                
                if component.type == "administrative_area_level_1" {
                print(component.name)
                    self.objRegister.state = component.name
                }
                
                if component.type == "country" {
                print(component.name)
                    self.objRegister.country = component.name
                }
                
                
            }
        }

        
        
        txtLocation.text = place.formattedAddress!
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    //MARK:-
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
       
        if textField == txtLocation {
            self.tapLocation()
            return false
        }
        return true
    }

}

extension CompleteProfileVC
{
    func CallSignUp(){
        var param:[String:Any] = [String:Any]()
        param["DOB"] = self.objRegister.DOB
        param["access_type"] = "1"
        param["career_experiance"] = self.objRegister.career_experiance
        param["city"] = self.objRegister.city
        param["country_code"] = self.objRegister.country_code
      //  param["device_token"] = DELEGATE.strDeviceToken
        param["device_token"] = AppDelegate.getDeviceTokenFromUserDefaults() ?? "1234"

        param["device_type"] = "1"
        param["education_level"] = self.objRegister.education_level
        param["email_id"] = self.objRegister.email_id
        param["gender"] = self.objRegister.gender
        param["lattitude"] = self.objRegister.lattitude
        param["location"] = self.objRegister.location
        param["longitude"] = self.objRegister.longitude
        param["mobile_no"] = self.objRegister.mobile_no
        param["passions"] = self.objRegister.passions
        param["password"] = self.objRegister.password
        param["user_address"] = self.objRegister.location
        param["user_name"] = self.objRegister.user_name
        param["state"] = self.objRegister.state
        param["country"] = self.objRegister.country

        print("\n ========= \n sign up params \n ========== \(param) \n ======= ")
        
        Networking.uploadImagesWithParams(.registerUser(param), profile_image: self.objRegister.profile_image, imageDic: self.objRegister.arr_images, dictParams: param, showHud: true,onView:self) { (res) in
            
            switch res {
            case .success(let upload, _, _):
                
                upload.responseJSON { response in
                    if let dictData = response.result.value as? [String:Any]{
                        if let status = dictData["status"] as? Int {
                            if status == 1{
                                if let data = dictData["data"] as? [String:Any]{
                                    
                                    isFromSignUp = true
                                    kCurrentUser.update(info: JSON(data))
                                    let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
                                    UserDefaults.standard.set("1", forKey: "isNewLogin")
                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }else{
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                            }
                        }else{
                            Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                        }
                    }
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                Common().showAlert(strMsg: "\(encodingError)", view: self)
            }
            
            
        }
    }
    
}
