//
//  WelcomeVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 01/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class WelcomeVC: UIViewController {

    @IBOutlet weak var cvWelcome: UICollectionView!
    @IBOutlet weak var pgControl: UIPageControl!
    @IBOutlet weak var btnNext: UIButton!
    @IBOutlet weak var btnSkip: UIButton!
    
    @IBOutlet weak var viewPlayer: UIView!
    var arrWelcomeData: [WelcomeModel] = []
    var player : AVPlayer!
        var avPlayerLayer : AVPlayerLayer!
    override func viewDidLoad() {
        super.viewDidLoad()

        btnNext.isHidden = true
        
        arrWelcomeData =  [
            ["title": "Sample title 1", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ", "imgBanner": "welcome banner img-1"],
            ["title": "Sample title 2", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ", "imgBanner": "welcome banner img-2"],
            ["title": "Sample title 3", "description": "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. ", "imgBanner": "welcome banner img-3"],
        ].map( WelcomeModel.init )
        
        cvWelcome.reloadData()

    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
       
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
//        let videoURL = URL(string: "https://www.youtube.com/watch?v=yHdNL9lkSnc&ab_channel=AmazonPrimeVideoIndia")
//
//        let filepath: String? = Bundle.main.path(forResource: "example", ofType: "mp4")
//               let fileURL = URL.init(fileURLWithPath: filepath!)
//
//      //  let player = AVPlayer(url: videoURL!)
//
//        let player = AVPlayer(url: fileURL)
//
//        let avPlayerController = AVPlayerViewController()
//               avPlayerController.player = player
//        avPlayerController.view.frame = viewPlayer.bounds
//        avPlayerController.showsPlaybackControls = false
//       // avPlayerController.delegate = self
//        viewPlayer.addSubview(avPlayerController.view)
//        avPlayerController.player?.play()

        
      //  let playerLayer = AVPlayerLayer(player: player)
       // playerLayer.frame = viewPlayer.bounds
       
       // viewPlayer.layer.addSublayer(playerLayer)

        
       // player.play()
        
        self.playVideo()
    }
    
    
    func playVideo() {
        guard let path = Bundle.main.path(forResource: "example2", ofType:"mp4") else {
            debugPrint("video.m4v not found")
            return
        }
        
        player = AVPlayer(url: URL(fileURLWithPath: path))
        avPlayerLayer = AVPlayerLayer(player: player)
        avPlayerLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        avPlayerLayer.frame = viewPlayer.bounds
        
        let session = AVAudioSession.sharedInstance()
        try! session.setCategory(AVAudioSession.Category.playback)
            try! session.setActive(true)
        
        
        viewPlayer.layer.addSublayer(avPlayerLayer)
        self.player.play()
        
     
        NotificationCenter.default.addObserver(self, selector: #selector(playerDidFinishPlaying), name: .AVPlayerItemDidPlayToEndTime, object: nil)

//        let videoURL =  URL(fileURLWithPath: path)
//
//        if videoURL != nil {
//
//            let player = AVPlayer(url: videoURL)
//            let playerViewController = AVPlayerViewController()
//            playerViewController.player = player
//            playerViewController.view.frame = viewPlayer.bounds
//
//            viewPlayer.addSubview(playerViewController.view)
//            playerViewController.videoGravity = AVLayerVideoGravity.resizeAspectFill
//            //  self.present(playerViewController, animated: true) {
//            playerViewController.player!.play()
//            // }
//
//        }
        
        
    }
    
    @objc func playerDidFinishPlaying(note: NSNotification) {
        print("Video Finished")
        
        let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    
    @IBAction func btnSkipAction(_ sender: Any) {
        
        self.player.pause()

        
        let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @IBAction func btnNextAction(_ sender: Any) {
        let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "LoginVC") as! LoginVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    class WelcomeModel {
        
        var title = ""
        var description = ""
        var imgBanner: UIImage?
        
        init(dic: [String: String]) {
            title = dic["title"] ?? ""
            description = dic["description"] ?? ""
            imgBanner = UIImage(named: dic["imgBanner"] ?? "")
        }
    }
}


extension WelcomeVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return arrWelcomeData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WelcomeTblCell", for: indexPath) as! WelcomeTblCell
        
        let aModel = arrWelcomeData[indexPath.row]
        
        cell.lblTitle.text = aModel.title
        cell.lblDesc.text = aModel.description
        cell.imgWC.image = aModel.imgBanner
        
        cell.backgroundColor = [UIColor.red, UIColor.blue, UIColor.green].randomElement()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return collectionView.frame.size
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        
        checkForScrollEnd(scrollView)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        
        checkForScrollEnd(scrollView)
    }
    
    func checkForScrollEnd(_ scrollView: UIScrollView) {
        
        let aFlagIsEnd = scrollView.contentOffset.x >= scrollView.contentSize.width - UIScreen.main.bounds.width
        btnSkip.isHidden = aFlagIsEnd
        btnNext.isHidden = !aFlagIsEnd
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            
            guard let aCollection = (scrollView as? UICollectionView),
                  let aCell = aCollection.visibleCells.first,
                  let aIndex = aCollection.indexPath(for: aCell)?.row
            else { return }
            
            self.pgControl.currentPage = aIndex
        }
        
    }
}

class WelcomeTblCell: UICollectionViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var lblDesc: UILabel!
    @IBOutlet weak var imgWC: UIImageView!
}
