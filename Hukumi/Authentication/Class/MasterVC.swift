//
//  MasterVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 07/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class MasterVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction() {
        navigationController?.popViewController(animated: true)
    }
}
