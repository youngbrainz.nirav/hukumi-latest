//
//  SignupVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 02/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import CountryPickerView

class SignupVC: MasterVC,CountryPickerViewDelegate,UITextFieldDelegate {
    
 
    var objRegister:ModelRegister = ModelRegister()
    
    @IBOutlet weak var txtConfirmPass: HUTextField!
    @IBOutlet weak var txtName: HUTextField!
    @IBOutlet weak var txtPassword: HUTextField!
    @IBOutlet weak var txtPhoneNumber: HUTextField!
    @IBOutlet weak var txtCountryCode: HUTextField!
    @IBOutlet weak var txtEmail: HUTextField!
    let countryPickerView_1 = CountryPickerView()

    
    @IBOutlet weak var btnPwd: UIButton!
    
    @IBOutlet weak var btnConfirmPwd: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        countryPickerView_1.delegate = self
        txtCountryCode.delegate = self
        
        
        btnPwd.setImage(#imageLiteral(resourceName: "eye_close"), for: .normal)
        btnPwd.tag = 0
        txtPassword.isSecureTextEntry = false
        btnConfirmPwd.setImage(#imageLiteral(resourceName: "eye_close"), for: .normal)
        btnConfirmPwd.tag = 0
        txtConfirmPass.isSecureTextEntry = false

        
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        DispatchQueue.main.async {
                self.removeSpinner()
        }


    }
    
    //MARK:- other func
    func ValidateValues() -> Bool{
        if txtName.text == ""{
             Common().showAlert(strMsg: "Please enter full name.", view: self)
            return false

        }
        if txtEmail.text == ""{
             Common().showAlert(strMsg: "Please enter email.", view: self)
            return false
        }
        if !Common().isValidEmail(email: txtEmail.text!){
             Common().showAlert(strMsg: "Please enter valid email.", view: self)
            return false
        }
        
        if txtCountryCode.text == ""{
            Common().showAlert(strMsg: "Please enter country code.", view: self)
           return false
        }
        if txtPhoneNumber.text == ""{
            Common().showAlert(strMsg: "Please enter phone number.", view: self)
            return false

        }
        if txtPassword.text == ""{
            Common().showAlert(strMsg: "Please enter password.", view: self)
            return false
        }
        if txtConfirmPass.text == ""{
            Common().showAlert(strMsg: "Please confirm your password.", view: self)
            return false
        }
        if txtPassword.text != txtConfirmPass.text{
            Common().showAlert(strMsg: "Password and confirm password must be same!", view: self)
            return false
        }
        return true
    }
    func PassValuetoNext(){
        objRegister.user_name = txtName.text!
        objRegister.password = txtPassword.text!
        objRegister.mobile_no = txtPhoneNumber.text!
        objRegister.country_code = txtCountryCode.text!
        
//        let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "OtpVC") as! OtpVC
        let vc = Common.AuthStoryBoard.instantiateViewController(withIdentifier: "CompleteProfileVC") as! CompleteProfileVC
        vc.objRegister = self.objRegister
        self.navigationController?.pushViewController(vc, animated: true)
        
    }
    //MARK:- btn click
    
    @IBAction func btnNextAction() {
        if self.ValidateValues(){
            self.CallCheckUserExist()
        }
        
    }
    
   
    
    @IBAction func btnPwdSecureText(_ sender: UIButton) {
        
        if sender.tag == 0
        {
           
            sender.setImage(#imageLiteral(resourceName: "eye_open"), for: .normal)
            sender.tag = 1
            txtPassword.isSecureTextEntry = true

        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "eye_close"), for: .normal)
            sender.tag = 0
            txtPassword.isSecureTextEntry = false

        }
        
    }
    
    @IBAction func btnConfirmPwdSecureText(_ sender: UIButton) {
        
        if sender.tag == 0
        {
           
            sender.setImage(#imageLiteral(resourceName: "eye_open"), for: .normal)
            sender.tag = 1
            txtConfirmPass.isSecureTextEntry = true

        }
        else
        {
            sender.setImage(#imageLiteral(resourceName: "eye_close"), for: .normal)
            sender.tag = 0
            txtConfirmPass.isSecureTextEntry = false

        }
        
    }
    
    //MARK:- Textfield delegate
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtCountryCode{
            countryPickerView_1.showCountriesList(from: self)
            return false
        }
        return true
    }
    func countryPickerView(_ countryPickerView: CountryPickerView, didSelectCountry country: Country) {
        txtCountryCode.text = "\(country.phoneCode)"
    }

}

//MARK:- API call

extension SignupVC{
    func CallCheckUserExist(){
        objRegister.user_name = txtName.text!
        objRegister.password = txtPassword.text!
        objRegister.mobile_no = txtPhoneNumber.text!
        objRegister.country_code = txtCountryCode.text!
        objRegister.email_id = txtEmail.text!

        var param : [String:Any] = [String:Any]()
        param["mobile_no"] = self.objRegister.country_code + self.objRegister.mobile_no
        param["email_id"] = self.objRegister.email_id
        Networking.performApiCall(.checkUserExistorNot(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        
                        self.PassValuetoNext()
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
            
        }
    }
    
    
}
