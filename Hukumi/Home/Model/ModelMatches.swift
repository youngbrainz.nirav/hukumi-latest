//
//  ModelMatches.swift
//  Hukumi
//
//  Created by hiren  mistry on 27/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON

class ModelMatches: NSObject {
    var _id:String = ""
    var session_token:String = ""
    var email_id:String = ""
    var device_type:String = ""
    var device_token:String = ""
    var user_name:String = ""
    var password:String = ""
    var gender:String = ""
    var DOB:String = ""
    var phone_number:String = ""
    var country_code:String = ""
    var mobile_no:String = ""
    var created_date:String = ""
    var month:String = ""
    var month_name:String = ""
    var year:String = ""
    var modified_date:String = ""
    var user_status:String = ""
    var online_status:String = ""
    var profile_image:String = ""
    var notification_status:String = ""
    var email_status:String = ""
    var message_status:String = ""
    var lattitude:String = ""
    var longitude:String = ""
    var user_address:String = ""
    var passions:String = ""
    var education_level:String = ""
    var city:String = ""
    var intro_message:String = ""
    var career_experiance:String = ""
    var is_approved:String = ""
    var isHukumePlusUser = ""
    var is_chat_deleted = 0
    var is_report_user = 0
    var isBlock = 0
    var images:[[String:String]] = [[String:String]]()

    
    init(obj:JSON) {
        if let x = obj["_id"].int{
            _id = "\(x)"
        }
        if _id == "" {
            if let x = obj["_id"].string{
                _id = "\(x)"
            }
        }
        if let x = obj["isHukumePlusUser"].int{
            isHukumePlusUser = "\(x)"
        }
        if let x = obj["session_token"].string{
            session_token = x
        }
        if let x = obj["is_chat_deleted"].int{
            is_chat_deleted = x
        }
        if let x = obj["is_report_user"].int{
            is_report_user = x
        }
        if let x = obj["isBlock"].int{
            isBlock = x
        }
        if let x = obj["email_id"].string{
            email_id = x
        }
        if let x = obj["device_type"].string{
            device_type = x
        }
        if let x = obj["device_token"].string{
            device_token = x
        }
        if let x = obj["user_name"].string{
            user_name = x
        }
        if let x = obj["password"].string{
            password = x
        }
        if let x = obj["gender"].string{
            gender = x
        }
        if let x = obj["DOB"].string{
            DOB = x
        }
        if let x = obj["phone_number"].string{
            phone_number = x
        }
        if let x = obj["country_code"].string{
            country_code = x
        }
        if let x = obj["mobile_no"].string{
            mobile_no = x
        }
        if let x = obj["created_date"].string{
            created_date = x
        }
        if let x = obj["month"].string{
            month = x
        }
        if let x = obj["month_name"].string{
            month_name = x
        }
        if let x = obj["year"].string{
            year = x
        }
        if let x = obj["modified_date"].string{
            modified_date = x
        }
        if let x = obj["user_status"].string{
            user_status = x
        }
        if let x = obj["online_status"].string{
            online_status = x
        }
        if let x = obj["profile_image"].string{
            profile_image = x
        }
        if let x = obj["notification_status"].string{
            notification_status = x
        }
        if let x = obj["email_status"].string{
            email_status = x
        }
        if let x = obj["message_status"].string{
            message_status = x
        }
        if let x = obj["lattitude"].string{
            lattitude = x
        }
        if let x = obj["longitude"].string{
            longitude = x
        }
        if let x = obj["user_address"].string{
            user_address = x
        }
        if let x = obj["passions"].string{
            passions = x
        }
        if let x = obj["education_level"].string{
            education_level = x
        }
        if let x = obj["city"].string{
            city = x
        }
        if let x = obj["intro_message"].string{
            intro_message = x
        }
        if let x = obj["career_experiance"].string{
            career_experiance = x
        }
        if let x = obj["is_approves"].string{
            is_approved = x
        }
        
        if let x = obj["images"].array{
            for xx in x{
                
                
                var dict = [String:String]()
                
                if let img = xx["image"].string
                {
                    dict["image"] = img
                    
                    if let id = xx["_id"].string
                    {
                        dict["id"] = id
                        
                        images.append(dict)

                    }
                    else
                    {
                        dict["id"] = "\(String(describing: xx["_id"].int!))"
                        
                        images.append(dict)
                    }

                }


                
//                if let img = xx.string {
//                    images.append(img)
//                }
            }
        }
        
    }
}
