//
//  modelChat.swift
//  Hukumi
//
//  Created by Stalwart It Solution on 06/02/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class ModelChat: NSObject {
    
    var senderID:String = ""
    var senderName:String = ""
    var senderProfileImage:String = ""
    var sender_unread:String = ""
    
    var recipientID:String = ""
    var recipientName:String = ""
    var recipientProfileImage:String = ""
    var receiver_unread:String = ""

  
    var conversation_id:String = ""
    var lastConversationDate:String = ""
    var lastMsg:String = ""
    var isBlock:Int = 0
    var isdeleted:String = ""
    var onlineStatus:String = "0"
    var to_user_id:String = "0"
    var isHukumePlusUser = ""
    var is_report_user = 0

    

    init(obj:JSON) {
        
        if let x = obj["user_id"].string{
            senderID = x
        }
        if let x = obj["isHukumePlusUser"].int{
            isHukumePlusUser = "\(x)"
        }
        
        if let x = obj["user_name"].string{
            senderName = x
        }
        
        if let x = obj["user_profile_image_url"].string{
            senderProfileImage = x
        }
        
        if let x = obj["unread_count"].string{
            sender_unread = x
        }
        
        if let x = obj["unread_count"].int{
            sender_unread = "\(x)"
        }
        //
        
        if let x = obj["to_user_id"].string{
            to_user_id = x
        }
        
        if let x = obj["to_user_id"].int{
            to_user_id = "\(x)"
        }
        if let x = obj["is_report_user"].int{
            is_report_user = x
        }
        
        //
        
        if let x = obj["recipient_id"].string{
            recipientID = x
        }
        
        if let x = obj["recipient_id"].int{
            recipientID = "\(x)"
            
        }
        
        if let x = obj["recipientName"].string{
            recipientName = x
        }
        
        if let x = obj["recipient_profile_image_url"].string{
            recipientProfileImage = x
        }
        
        if let x = obj["receiver_unread"].string{
            receiver_unread = x
        }
        
        //
        
        if let x = obj["id"].int{
            conversation_id = "\(x)"
            
        }
        
        if let x = obj["lastConversationDate"].string{
            lastConversationDate = x
        }
        
        if let x = obj["lastMsg"].string{
            lastMsg = x
        }
        
        if let x = obj["isBlock"].int{
            isBlock = x
        }
        
        if let x = obj["isdeleted"].string{
            isdeleted = x
        }
        
        if let x = obj["online_status"].string{
            onlineStatus = x
        }
        
        
        if let x = obj["online_status"].int{
            onlineStatus = "\(x)"
            
        }
        
    }
    
}
