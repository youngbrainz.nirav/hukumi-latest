//
//  ModelPages.swift
//  Hukumi
//
//  Created by hiren  mistry on 27/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON

class ModelGetFilters: NSObject {
   
    var filter_city:String = ""
    var filter_distance:String = "0"
    var filter_gender:String = ""
    var filter_new_lattitude:String = "0.0"
    var filter_new_longitude:String = "0.0"
    var filter_new_country:String = ""
    var filter_new_state:String = ""
    var filter_new_city:String = ""

    
    init(obj:JSON) {
        if let x = obj["filter_city"].string{
            filter_city = x
        }
        if let x = obj["filter_distance"].string{
            
            if x != ""
            {
            filter_distance = x
            }
        }
        if let x = obj["filter_gender"].string{
            filter_gender = x
        }
        
        if let x = obj["filter_new_lattitude"].string{
            
            if x != ""
            {
                filter_new_lattitude = x
            }
        }
        
        if let x = obj["filter_new_lattitude"].string{
            
            if x != ""
            {
            filter_new_lattitude = x
            }
        }
        
        if let x = obj["filter_new_city"].string{
            
            if x != ""
            {
            filter_new_city = x
            }
        }
        
        if let x = obj["filter_new_state"].string{
            
            if x != ""
            {
            filter_new_state = x
            }
        }
        
        if let x = obj["filter_new_country"].string{
            
            if x != ""
            {
            filter_new_country = x
            }
        }
       
    }
}
