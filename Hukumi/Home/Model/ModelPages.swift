//
//  ModelPages.swift
//  Hukumi
//
//  Created by hiren  mistry on 27/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON

class ModelPages: NSObject {
    var sc_id:String = ""
    var sc_title:String = ""
    var sc_content:String = ""
    var status:String = ""
    var orderBy:String = ""
    var created_date:String = ""
    var modified_date:String = ""
    
    init(obj:JSON) {
        if let x = obj["sc_id"].string{
            sc_id = x
        }
        if let x = obj["sc_title"].string{
            sc_title = x
        }
        if let x = obj["sc_content"].string{
            sc_content = x
        }
        if let x = obj["status"].string{
            status = x
        }
        if let x = obj["orderBy"].string{
            orderBy = x
        }
        if let x = obj["created_date"].string{
            created_date = x
        }
        if let x = obj["modified_date"].string{
            modified_date = x
        }
    }
}
