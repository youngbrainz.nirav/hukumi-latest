//
//  modelChatObject.swift
//  Hukumi
//
//  Created by Stalwart It Solution on 07/02/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import Foundation
import SwiftyJSON
import UIKit

class modelChatObject: NSObject {
    
    
    var senderId:String = ""
    var senderName:String = ""
    var timeStamp:String = ""
    var message:String = ""
    
    init(obj:JSON) {
        
        if let x = obj["senderId"].string{
            senderId = x
        }
        
        if senderId == ""
        {
            if let x = obj["senderId"].int{
                senderId = "\(x)"
            }
        }
        
        if let x = obj["senderName"].string{
            senderName = x
        }
        if let x = obj["timeStamp"].string{
            timeStamp = x
        }
        
        if timeStamp == ""
        {
            if let x = obj["timeStamp"].int{
                timeStamp = "\(x)"
            }
        }
        
        
        if let x = obj["message"].string{
            message = x
        }
    
    }
    
}
