//
//  AddCardDetailsVC.swift
//  Hukumi
//
//  Created by VATSAL on 11/19/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class AddCardDetailsVC: UIViewController {

    @IBOutlet weak var lblAmount : UILabel!
    @IBOutlet weak var lblMonths : UILabel!
    @IBOutlet weak var txtCardnumber : UITextField!
    @IBOutlet weak var txtExpriy : UITextField!
    @IBOutlet weak var txtCVV : UITextField!
    @IBOutlet weak var btnAutoRenewal: UIButton!
    
    
    var planId = ""
    var strYear = ""
    var strMonth = ""
    var monthsTitle = ""
    var priceTitle = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        txtCardnumber.delegate =  self
        txtExpriy.delegate =  self
        txtCVV.delegate =  self
        btnAutoRenewal.isSelected = true
        self.lblMonths.text =  monthsTitle
        self.lblAmount.text =  "subscription = \(priceTitle)"
        
        txtCardnumber.attributedPlaceholder = NSAttributedString(
            string: "Credit / Debit Card Number",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )
        txtExpriy.attributedPlaceholder = NSAttributedString(
            string: "Expiry date",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )
        txtCVV.attributedPlaceholder = NSAttributedString(
            string: "CVV",
            attributes: [NSAttributedString.Key.foregroundColor: UIColor.white]
        )
        
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        
        if isValidCardNumber(){
            if txtExpriy.text?.count != 0{
                let mainStr = txtExpriy.text?.split(separator: "/")
                let strMonth1 = mainStr![0]
                let strYear1 = mainStr![1]
               
                self.strMonth =  "\(strMonth1)"
                self.strYear =  "\(strYear1)"
                print("Month : \(strMonth), Year : \(strYear)")
            }
            callPaymentAPI()
        }
        
    }
    @IBAction func onClickBtnAutoRenewal(_ sender: Any) {
        btnAutoRenewal.isSelected = !btnAutoRenewal.isSelected
    }
    
    func isValidCardNumber() -> Bool{
        
        if (txtCardnumber.text?.isEmpty)!
        {
            Common().showAlert(strMsg: "Please enter Card Number", view: self)
            txtCardnumber.becomeFirstResponder()
            return false
        }
        
        if (txtExpriy.text?.isEmpty)!
        {
            Common().showAlert(strMsg: "Please enter expriy date", view: self)
            txtExpriy.becomeFirstResponder()
            return false
        }
        
        if (txtCVV.text?.isEmpty)!
        {
            Common().showAlert(strMsg: "Please enter cvv", view: self)
            txtCVV.becomeFirstResponder()
            return false
        }
      
        return true
    }
    
    func callPaymentAPI(){
        var param:[String:Any] = [String:Any]()
        
        showSpinner(onView: self.view)
        
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["plan_id"] = planId
        param["card_number"] = txtCardnumber.text ?? ""
        param["cvv"] = txtCVV.text ?? ""
        param["expiry_year"] = strYear
        param["expiry_month"] = strMonth
        param["is_auto_subscribed"] = btnAutoRenewal.isSelected ? "1":"0"
        print("\n ========= \n home screen userList api params \n ======= \n \(param)")

        print("URL:- \(APIs.BASE_URL + APIs.MAKE_PAYMENT_PLUS)")
        print("\n ======= \n make subscription plan \n ======= \n \(param)")
        Alamofire.request(APIs.BASE_URL + APIs.MAKE_PAYMENT_PLUS, method: .post, parameters: param, encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                switch response.result {
                case let .success(value):
                    print(value)
                    let tempvalue = value as! [String:Any]
                    let status = tempvalue["status"]as! Int
                    let message = tempvalue["msg"]as? String ?? "something went wrong!"
                    if status == 1 {
                        self.removeSpinner()
                        let jsonObject =  JSON(value)
                            Common().showAlert(strMsg: message, view: self)
                        let aAuthenticationStoryboard = UIStoryboard(name: "Home", bundle: nil)
                        let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(identifier: "CustomTabbarVC")
                        let appDelegate = UIApplication.shared.delegate as! AppDelegate
                        appDelegate.window?.rootViewController = aWelcomeNavigation
                        appDelegate.navController = aWelcomeNavigation.navigationController
                        appDelegate.window?.makeKeyAndVisible()
                        }else{
                            self.removeSpinner()
                            Common().showAlert(strMsg: message, view: self)
                        }
                case let .failure(error):
                    print(error)
                    self.removeSpinner()

                    
                }
            }
        }
}
extension AddCardDetailsVC :  UITextFieldDelegate{
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let currentText = (textField.text as NSString?)?.replacingCharacters(in: range, with: string) else { return true }
        
        if textField == txtCardnumber {
            if (currentText.utf16.count) <= 19{
                textField.setText(to: currentText.grouping(every: 4, with: " "), preservingCursor: true)
                return false
            }
        }else if textField == txtExpriy {
            if (currentText.utf16.count) <= 5{
                textField.setText(to: currentText.grouping(every: 2, with: "/"), preservingCursor: true)
                return false
            }
        }else if textField == txtCVV {
            if (currentText.utf16.count) == 4{
        // textField.setText(to: currentText.grouping(every: 4, with: "-"), preservingCursor: true)
                return false
            }
            else{
                return true
            }
        }else {
            
        }
        return false
    }
}

extension UITextField{
   
    public func setText(to newText: String, preservingCursor: Bool) {
        if preservingCursor {
            let cursorPosition = offset(from: beginningOfDocument, to: selectedTextRange!.start) + newText.count - (text?.count ?? 0)
            text = newText
            if let newPosition = self.position(from: beginningOfDocument, offset: cursorPosition) {
                selectedTextRange = textRange(from: newPosition, to: newPosition)
            }
        }
        else {
            text = newText
        }
    }
}
extension String{
    func grouping(every groupSize: String.IndexDistance, with separator: Character) -> String {
       let cleanedUpCopy = replacingOccurrences(of: String(separator), with: "")
       return String(cleanedUpCopy.enumerated().map() {
            $0.offset % groupSize == 0 ? [separator, $0.element] : [$0.element]
       }.joined().dropFirst())
    }
}
