//
//  WebViewVC.swift
//  Hukumi
//
//  Created by VATSAL on 11/18/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import WebKit


protocol DISMISSWEBVC {
    func getDetails(isCancelFace : Bool)
}


class WebViewVC: UIViewController,WKNavigationDelegate {

    @IBOutlet weak var webView : WKWebView!
     var delegateVC : DISMISSWEBVC?
    
    
    var strUrl = ""
    var strUrl2 = ""
    var isFrom = ""
    var strConfirmRURL = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print("URL :- https://www.hukumeonline.com/face?url1=\(strUrl)&url2=\(self.strUrl2)")
        webView.navigationDelegate =  self
        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='\(100)%'"
        if self.strUrl != ""{
            
            self.webView.addObserver(self, forKeyPath: "URL", options: .new, context: nil)
                self.webView.addObserver(self, forKeyPath: "estimatedProgress", options: .new, context: nil)
            self.webView.load(URLRequest(url: URL(string: "https://www.hukumeonline.com/face?url1=\(strUrl)&url2=\(self.strUrl2)")!))
                
        }
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='100%'"//dual size
        webView.evaluateJavaScript(js, completionHandler: nil)

    }
    
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        let js = "document.getElementsByTagName('body')[0].style.webkitTextSizeAdjust='100%'"//dual size
        webView.evaluateJavaScript(js, completionHandler: nil)
    }
    
    override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
        if keyPath == #keyPath(WKWebView.url) {
          print("### URL:", self.webView.url!)
            if let str = self.webView.url{
                strConfirmRURL = "\(str)"
            }
            
            if  strConfirmRURL == "https://www.hukumeonline.com/faceFailure"{
//                Common().showAlert(strMsg: "Your Face did not matched vour profile picture, make sure your profile and face picture are the same.", view: self)
                self.delegateVC?.getDetails(isCancelFace: true)
                self.navigationController?.popViewController(animated: true)
                
            }else if strConfirmRURL ==  "https://www.hukumeonline.com/faceSuccess"{
                let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
                vc.isFrom = self.isFrom
                vc.hidesBottomBarWhenPushed =  true
                self.navigationController?.pushViewController(vc, animated: true)

            }
        }

        if keyPath == #keyPath(WKWebView.estimatedProgress) {
          // When page load finishes. Should work on each page reload.
          if (self.webView.estimatedProgress == 1) {
            print("### EP:", self.webView.estimatedProgress)
              
          
          }
        }
      }
    

    
}
