//
//  MatchesVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 03/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
import CoreLocation

class MatchesVC: MasterVC {
    
    @IBOutlet weak var cvMatches: UICollectionView!
    
    @IBOutlet weak var lblLikes: UILabel!
    var arrMatches: [ModelMatches] = [ModelMatches]()
    var ArrLikes: [ModelMatches] = [ModelMatches]()

    var selectedTab = "1"
    var selectedTag = 0
    
    @IBOutlet weak var lblMatches: UILabel!
   
    
    
    @IBOutlet weak var viewPopUpMatched: UIView!
    
    @IBOutlet weak var viewInnerPopUpMatched: INXView!
    
    @IBOutlet weak var lblTitlePopUpMatched: UILabel!
    var strWhoLikeMe = ""
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        self.lblMatches.isHidden = false
        self.lblLikes.isHidden = true
        
        self.viewPopUpMatched.isHidden = true

        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewPopUpMatched.isHidden = true

        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.startApiCounter()
        
        CallGetMatches(str:selectedTab)

    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.stopApiCounter()
    }
    
    @IBAction func btnLikesClick(_ sender: Any) {
        self.strWhoLikeMe = "yes"
        self.lblMatches.isHidden = true
        self.lblLikes.isHidden = false
        CallGetMatches(str:"2")
        selectedTab = "2"

    }
    @IBAction func btnMatchesClick(_ sender: Any) {
        self.strWhoLikeMe = "no"
        self.lblMatches.isHidden = false
        self.lblLikes.isHidden = true
        CallGetMatches(str:"1")
        selectedTab = "1"

    }
    
    
    @IBAction func actionOkayMatcehdPopUp(_ sender: Any) {
        
        self.hideMatchedPopup()
    }
    
    
    func showMatchedPopUp()
    {
        self.viewPopUpMatched.isHidden = false
        self.viewInnerPopUpMatched.alpha = 0

        UIView.animate(withDuration: 0.5, animations: {
            self.viewInnerPopUpMatched.alpha = 1
        }) { (finished) in


        }
    }

    func hideMatchedPopup()
    {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.viewInnerPopUpMatched.alpha = 0
        }) { (finished) in

            self.viewPopUpMatched.isHidden = true

        }

    }
    
}

extension MatchesVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        arrMatches.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "MatchesCell", for: indexPath) as! MatchesCell
        
        let aModel = arrMatches[indexPath.row]
        let aImg = aModel.profile_image
        
        
        if selectedTab == "1"
        {
            aCell.stackLikeDislike.isHidden = true
        }
        else
        {
            aCell.stackLikeDislike.isHidden = false
        }
        
        if aModel.isHukumePlusUser == "0" || aModel.isHukumePlusUser == ""
        {
            aCell.imgVerified.isHidden = true
            aCell.widthConstImgVerified.constant = 0
        }
        else{
            aCell.imgVerified.isHidden = false
            aCell.widthConstImgVerified.constant = 15

        }

        
        aCell.imgDislike.image = aCell.imgDislike.image?.imageWithColor(color1: UIColor.black)
        
        KingfisherManager.shared.retrieveImage(with: URL(string:aImg)!) { result in
            let image = try? result.get().image
            if let image = image {
                aCell.imgUser.image = image
            }
        }

//        aCell.imgUser.image = aModel.imgUser
        aCell.lblUserName.text = "\(aModel.user_name)"
        
        
        //My location
        let myLocation = CLLocation(latitude: Double(kCurrentUser.lattitude!) ?? 0.0, longitude: Double(kCurrentUser.longitude!) ?? 0.0)

        //My buddy's location
        let myBuddysLocation = CLLocation(latitude: Double(aModel.lattitude) ?? 0.0, longitude: Double(aModel.longitude) ?? 0.0)

        //Measuring my distance to my buddy's (in mile)
        let distance = myLocation.distance(from: myBuddysLocation) / 1609
        
        
//        aCell.lblMiles.text = "\(distance) miles"
//        aCell.lblMiles.text = String(format:"%.2f miles", distance)

        aCell.lblMiles.text = aModel.city

        
        aCell.viewOnline.backgroundColor = aModel.online_status == "1" ? UIColor.green : UIColor.lightGray
        
        aCell.btnLike.tag = indexPath.row
        aCell.btnLike.addTarget(self, action: #selector(btnLikeCliked(_:)), for: .touchUpInside)
        
        aCell.btnDislike.tag = indexPath.row
        aCell.btnDislike.addTarget(self, action: #selector(btnDisLikeCliked(_:)), for: .touchUpInside)
        
        
        return aCell
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
        if strWhoLikeMe == "yes"{
            vc.isFrom = "whoLikeMe"
        }else{
            vc.isFrom = ""
        }
        vc.objUser = self.arrMatches[indexPath.row]
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    @objc func btnLikeCliked(_ sender: UIButton)
    {
        print(sender.tag)
        
        selectedTag = sender.tag
        
        let dict = arrMatches[sender.tag]
        
        self.CallAcceptReject(strID: dict._id , status: "1", isReject: false)
    }
    
    @objc func btnDisLikeCliked(_ sender: UIButton)
    {
        print(sender.tag)
        
        selectedTag = sender.tag

        
        let dict = arrMatches[sender.tag]

        
        self.CallAcceptReject(strID: dict._id, status: "0", isReject: true)
    }
    
    func CallAcceptReject(strID:String,status:String,isReject:Bool){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["screen_type"] = "1"
        param["requested_id"] = strID
        param["requsted_status"] = status

        
        print("\n ========= \n accept reject api params \n ======= \n \(param)")

        
        Networking.performApiCall(.changeRequstedMatchesRecord(param), true, self) { (res) in
            print(res)
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                      
                        print("success")
                        
                        
                        let username = "\(self.arrMatches[self.selectedTag].user_name)"
                       
                        if isReject == false
                        {
                        
                        let strMsg = "You and \(username) Match - you're now hooked.You can chat with each other on tha Match Section/Screen."
                            
                        self.lblTitlePopUpMatched.text = strMsg
                        
                        self.showMatchedPopUp()
                        }
                        
                        self.arrMatches.remove(at: self.selectedTag)

                        self.cvMatches.reloadData()

                        
//                        self.CallGetMatches(str:"2")
//                        self.selectedTab = "2"
                        
                        
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
}

extension MatchesVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if selectedTab == "1"
        {
        let aWidth = (UIScreen.main.bounds.width - 55) / 2
        
        return CGSize(width: aWidth, height: aWidth * 1.3)
        }
        else
        {
            let aWidth = (UIScreen.main.bounds.width - 55) / 2
            
            return CGSize(width: aWidth, height: aWidth * 1.70)
        }
    }
}



class MatchesCell: UICollectionViewCell {
    
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var lblMiles: UILabel!
    @IBOutlet weak var viewOnline: UIView!
    @IBOutlet weak var viewBackground: UIView!
    
    @IBOutlet weak var stackLikeDislike: UIStackView!
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var btnDislike: UIButton!
    
    
    @IBOutlet weak var imgLike: UIImageView!
    
    @IBOutlet weak var imgDislike: UIImageView!
    @IBOutlet weak var imgVerified: UIImageView!
    @IBOutlet weak var widthConstImgVerified: NSLayoutConstraint!
    override func awakeFromNib() {
     
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        viewBackground.layer.cornerRadius = 5
        
        imgDislike.image = imgDislike.image?.imageWithColor(color1: UIColor.red)
        imgLike.image = imgLike.image?.imageWithColor(color1: UIColor(red: 43.0/255.0, green: 189.0/255.0, blue: 172.0/255.0, alpha: 1.0))

        
        btnDislike.layer.borderWidth = 2
        btnDislike.layer.borderColor = UIColor.red.cgColor
        btnDislike.layer.masksToBounds = true
        
        btnLike.layer.borderWidth = 2
        btnLike.layer.borderColor = UIColor(red: 43.0/255.0, green: 189.0/255.0, blue: 172.0/255.0, alpha: 1.0).cgColor
        btnLike.layer.masksToBounds = true
        
        btnLike.layer.cornerRadius = btnLike.frame.size.width/2
        btnDislike.layer.cornerRadius = btnDislike.frame.size.width/2
        
        
    }
}

//MARK:- API Call
extension MatchesVC{
    func CallGetMatches(str:String){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["screen_type"] = str

        print("\n ========= \n get matches api params \n ======= \n \(param)")

        
        Networking.performApiCall(.getMatchesRecord(param), true, self) { (res) in


            self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                
                print("\n ========= \n get matches api responce \n ======= \n \(dictData)")

                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                            for obj in data{
                                let user = ModelMatches(obj: JSON(obj))
                                if user.isBlock != 1  {
                                self.arrMatches.append(user)
                                }
                            }
                            
                            self.cvMatches.reloadData()
                            
                            if let tabItems = self.tabBarController?.tabBar.items {
                                // In this case we want to modify the badge number of the third tab:
                               
                               
                                
                                
                                let tabItemMatches = tabItems[1]
                                
                                
                                    tabItemMatches.badgeValue = nil
                               
                                
                            }
                            
                            
                            
                        }
                    }else{
                        
//                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                        self.cvMatches.reloadData()

                    }
                }
            }
        }
    }
}
