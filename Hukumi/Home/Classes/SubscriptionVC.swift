//
//  SubscriptionVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class SubscriptionVC: MasterVC {
    
    @IBOutlet weak var viewSubsccriptionBackground: UIView!

    @IBOutlet weak var viewBlock1: UIView!
    
    
    @IBOutlet weak var lblBlock1: UILabel!
    
    @IBOutlet weak var viewBlock2: UIView!
    @IBOutlet weak var lblBlock2: UILabel!
    
    @IBOutlet weak var viewBlock3: UIView!
    
    @IBOutlet weak var lblBlock3: UILabel!
    
    
    @IBOutlet weak var viewSubList1: UIView!
    @IBOutlet weak var lblSubList1: UILabel!
    @IBOutlet weak var imgSubList1: UIImageView!
    
    
    @IBOutlet weak var viewSubList2: UIView!
    @IBOutlet weak var imgSublist2: UIImageView!
    @IBOutlet weak var lblSubList2: UILabel!
    
    
    
    @IBOutlet weak var viewSublist3: UIView!
    @IBOutlet weak var lblSublist3: UILabel!
    @IBOutlet weak var imgSublist3: UIImageView!
    
    
    @IBOutlet weak var viewPlans: UIView!
    @IBOutlet weak var viewSubList12: UIView!
    @IBOutlet weak var lblSubList12: UILabel!
    @IBOutlet weak var imgSubList12: UIImageView!
    
    
    @IBOutlet weak var viewSubList22: UIView!
    @IBOutlet weak var imgSublist22: UIImageView!
    @IBOutlet weak var lblSubList22: UILabel!
    
    
    
    @IBOutlet weak var viewSublist32: UIView!
    @IBOutlet weak var lblSublist32: UILabel!
    @IBOutlet weak var imgSublist32: UIImageView!
    
    
    var selectedPlan = ""
    var selectedPlanPricing = 0.0

    
    @IBOutlet weak var stackPrivacy: UIStackView!
    
    @IBOutlet weak var stackPlans: UIStackView!
    @IBOutlet weak var viewStackPlans: UIView!
    
    @IBOutlet weak var btnSubScribe: UIButton!
    @IBOutlet weak var btnTerms: UIButton!
    @IBOutlet weak var btnPrivacy: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    @IBOutlet weak var btnPaynow: UIButton!
    @IBOutlet weak var btnMobilemoney: UIButton!
    
    
    @IBOutlet weak var lblSubscriptionData: UILabel!
    
    @IBOutlet weak var lblTitleHeader: UILabel!
    
    @IBOutlet weak var btnSubscribe: UIButton!
    
    
    @IBOutlet weak var viewPurchase: UIView!
    @IBOutlet weak var viewNavTitle: UIView!
    
    
    @IBOutlet weak var constViewPurchaseBottom: NSLayoutConstraint!
    
    @IBOutlet weak var heightconststackPlans: NSLayoutConstraint!
    
    // view mobile money
    
    
    @IBOutlet weak var viewMobileMoney: UIView!
    
    @IBOutlet weak var txtDescMobileMoney: UITextView!
    
    
    @IBOutlet weak var constViewMobileMoneyBottom: NSLayoutConstraint!
    @IBOutlet weak var heightConstviewSubsccriptionBackground: NSLayoutConstraint!
    
    
    var isFrom = ""
    var arrPlans = [JSON]()
    var planId = ""
    var selectedIndex =  0
    var strPrice =  ""
    var isHUKUMEPLUSSUBSRCIBE = false
    var planName = ""
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()

        
//        lblBlock1.text = "Three (3)\n months\n\(IAPManager.shared.QuarterlyLocalizedPrice)/\nmonth"
//        lblBlock2.text = "Six (6) months \n\(IAPManager.shared.HalflyLocalizedPrice)/month"
//        lblBlock3.text = "One (1) month \n\(IAPManager.shared.MonthlyLocalizedPrice)/month"

        
        
        lblBlock1.text = "Three (3)\n months\n\(IAPManager.shared.QuarterlyLocalizedPrice)"
        lblBlock2.text = "Six (6) months \n\(IAPManager.shared.HalflyLocalizedPrice)"
        lblBlock3.text = "One (1) month \n\(IAPManager.shared.MonthlyLocalizedPrice)"
        
        lblSubList2.text = "Three (3) Months: \(IAPManager.shared.QuarterlyLocalizedPrice)"
        lblSublist3.text = "Six (6) Months: \(IAPManager.shared.HalflyLocalizedPrice)"
        lblSubList1.text = "One (1) Month: \(IAPManager.shared.MonthlyLocalizedPrice)"
        
        viewSubsccriptionBackground.layer.borderWidth = 1
        viewSubsccriptionBackground.layer.borderColor = Constant.Color.themeRed.cgColor
        viewSubsccriptionBackground.layer.cornerRadius = 8
        
        viewSubList1.layer.borderWidth = 1
        viewSubList1.layer.borderColor = UIColor.white.cgColor
        viewSubList1.layer.cornerRadius = 8

        viewSubList2.layer.borderWidth = 1
        viewSubList2.layer.borderColor = UIColor.white.cgColor
        viewSubList2.layer.cornerRadius = 8

        viewSublist3.layer.borderWidth = 1
        viewSublist3.layer.borderColor = UIColor.white.cgColor
        viewSublist3.layer.cornerRadius = 8

        
        imgSubList1.tintColor = Constant.Color.themeRed
        imgSublist2.tintColor = Constant.Color.themeRed
        imgSublist3.tintColor = Constant.Color.themeRed
        
        imgSubList1.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)
        imgSublist2.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)
        imgSublist3.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)

        
        
        lblTitleHeader.font = Constant.Font.gilroy(size: 20, type: .bold)
        btnSubscribe.titleLabel?.font = Constant.Font.gilroy(size: 22, type: .bold)
        self.lblSubscriptionData.isHidden = true
        
        
        
//        txtDescMobileMoney.text = "STEPS TO LOAD EDR WALLET WITH MOBILE MONEY \n\(IAPManager.shared.MonthlyLocalizedPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money"
        
        
        txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(IAPManager.shared.MonthlyLocalizedPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and Hukume registered name and number.\n\nWhen the steps are done, our customer support team will grant you access to get hooked on Hukume.", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(IAPManager.shared.MonthlyLocalizedPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and Hukume registered name and number.\n\nWhen the steps are done, our customer support team will grant you access to get hooked on Hukume.")
        
        
        if self.isFrom == "verificationVC"{
            self.viewStackPlans.isHidden = true
            self.viewPlans.isHidden = false
            
            lblTitleHeader.text = "".uppercased()
            self.viewNavTitle.isHidden = false
            self.viewSubsccriptionBackground.isHidden =  true
            self.heightConstviewSubsccriptionBackground.constant = 0
            self.btnTerms.isHidden =  true
            self.btnPrivacy.isHidden =  true
            btnSubScribe.isHidden =  true
            btnSubscribe.isHidden =  true
            btnBack.isHidden =  false
            btnPaynow.setTitle("PAY WITH CARD", for: .normal)
            btnMobilemoney.setTitle("MOBILE MONEY PAYMENT", for: .normal)
            constViewPurchaseBottom.constant = -200
            constViewMobileMoneyBottom.constant = -650
            callPlusPlanAPIs()
        }else{
            self.viewStackPlans.isHidden = false
            self.viewPlans.isHidden = true
            lblTitleHeader.text = "SUBSCRIPTION"
            self.viewNavTitle.isHidden = true
            self.viewSubsccriptionBackground.isHidden =  false
            self.heightConstviewSubsccriptionBackground.constant = 180
            self.btnTerms.isHidden =  false
            self.btnPrivacy.isHidden =  false
            self.btnSubscribe.isHidden =  false
            btnSubScribe.isHidden =  false
            btnSubscribe.isHidden =  false
            btnBack.isHidden =  true
            btnPaynow.setTitle("PAY NOW", for: .normal)
            btnMobilemoney.setTitle("MOBILE MONEY PAYMENT", for: .normal)
            constViewPurchaseBottom.constant = -160
            constViewMobileMoneyBottom.constant = -550
            getSubscriptionPlansAPI()
            setSelectedPlan(index: 0)
            
        }
    }
    
     func setAttributesForType2(fullStr: String , boldTxt: String, normalTxt : String) -> NSAttributedString {
        
        let style = NSMutableParagraphStyle()
        style.alignment = NSTextAlignment.center
        
        let attributedString = NSMutableAttributedString(string: fullStr, attributes: [NSAttributedString.Key.font: Constant.Font.gilroy(size: 15, type: .regular)!, NSAttributedString.Key.foregroundColor : UIColor.black])
        
        let boldFontAttribute: [NSAttributedString.Key: Any] = [NSAttributedString.Key.font: Constant.Font.gilroy(size: 15, type: .bold)!, NSAttributedString.Key.foregroundColor : UIColor.black]
        
        let range = (fullStr as NSString).range(of: boldTxt)
        attributedString.addAttributes(boldFontAttribute, range: range)
        attributedString.addAttributes([NSAttributedString.Key.paragraphStyle: style ],
                               range: range)
        return attributedString
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.startApiCounter()
        
//        strServerPurchaseMsg = ""
//        isUserSubscribe = false
        
//        if strServerPurchaseMsg != ""
//        {
//        self.lblSubscriptionData.isHidden = false
//            lblSubscriptionData.text = strServerPurchaseMsg
//            hidePlans()
//        }
//        else{
//            if isUserSubscribe == true
//            {
//
//  self.lblSubscriptionData.isHidden = false
//                lblSubscriptionData.text = "Your \(strInAppPurchasePrice) is currently active. Thank you for finding love with Hukume"
//                hidePlans()
//            } else if isHUKUMEPLUSSUBSRCIBE == true{
//        self.lblSubscriptionData.isHidden = false
//                lblSubscriptionData.text = "You have successfully paid for \(planName) Hukume Plus subscription"
//
//                hidePlans()
//            }
//            else
//            {
//                showPlans()
//            }
//        }
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.stopApiCounter()
    }
    
    func setData(){
        lblSubList1.text = "\(arrPlans[0]["name"].string ?? "") : \("\(arrPlans[0]["converted_price"].string ?? "")")"
        lblSubList2.text = "\(arrPlans[1]["name"].string ?? "") : \("\(arrPlans[1]["converted_price"].string ?? "")")"
        lblSublist3.text = "\(arrPlans[2]["name"].string ?? "") : \("\(arrPlans[2]["converted_price"].string ?? "")")"
        lblSubList12.text = "\(arrPlans[0]["name"].string ?? "") : \("\(arrPlans[0]["converted_price"].string ?? "")")"
        lblSubList22.text = "\(arrPlans[1]["name"].string ?? "") : \("\(arrPlans[1]["converted_price"].string ?? "")")"
        lblSublist32.text = "\(arrPlans[2]["name"].string ?? "") : \("\(arrPlans[2]["converted_price"].string ?? "")")"
        
        
        viewSubsccriptionBackground.layer.borderWidth = 1
        viewSubsccriptionBackground.layer.borderColor = Constant.Color.themeRed.cgColor
        viewSubsccriptionBackground.layer.cornerRadius = 8
        
        viewSubList1.layer.borderWidth = 1
        viewSubList1.layer.borderColor = UIColor.white.cgColor
        viewSubList1.layer.cornerRadius = 8
        
        viewSubList12.layer.borderWidth = 1
        viewSubList12.layer.borderColor = UIColor.white.cgColor
        viewSubList12.layer.cornerRadius = 8

        viewSubList2.layer.borderWidth = 1
        viewSubList2.layer.borderColor = UIColor.white.cgColor
        viewSubList2.layer.cornerRadius = 8
        
        viewSubList22.layer.borderWidth = 1
        viewSubList22.layer.borderColor = UIColor.white.cgColor
        viewSubList22.layer.cornerRadius = 8

        viewSublist3.layer.borderWidth = 1
        viewSublist3.layer.borderColor = UIColor.white.cgColor
        viewSublist3.layer.cornerRadius = 8
        
        viewSublist32.layer.borderWidth = 1
        viewSublist32.layer.borderColor = UIColor.white.cgColor
        viewSublist32.layer.cornerRadius = 8

        
        imgSubList1.tintColor = Constant.Color.themeRed
        imgSublist2.tintColor = Constant.Color.themeRed
        imgSublist3.tintColor = Constant.Color.themeRed
        
        imgSubList12.tintColor = Constant.Color.themeRed
        imgSublist22.tintColor = Constant.Color.themeRed
        imgSublist32.tintColor = Constant.Color.themeRed
        
        imgSubList1.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)
        imgSublist2.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)
        imgSublist3.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)

        imgSubList12.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)
        imgSublist22.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)
        imgSublist32.image = imgSubList1.image?.imageWithColor(color1: Constant.Color.themeRed)

        
        setSelectedPlan2(index: 0)
    }
    func showPlans()
    {
        if self.isFrom == "verificationVC"{
            btnSubScribe.isHidden = true
        }else{
            btnSubScribe.isHidden = false
        }
        
        viewSubsccriptionBackground.isHidden = false
        stackPlans.isHidden = false
        stackPrivacy.isHidden = false
        lblSubscriptionData.isHidden = true
    }
    
    func hidePlans(isLive:Int=1)
    {
        btnSubScribe.isHidden = true
        viewSubsccriptionBackground.isHidden = true
        stackPlans.isHidden = true
        stackPrivacy.isHidden = true

        lblSubscriptionData.isHidden = false
        
        if strServerPurchaseMsg != ""
        {
            self.lblSubscriptionData.isHidden = false
            lblSubscriptionData.text = strServerPurchaseMsg
        }else if isHUKUMEPLUSSUBSRCIBE == true{
            self.lblSubscriptionData.isHidden = false
            self.lblSubscriptionData.text = "You have successfully paid for \(self.planName) Hukume Plus subscription"
            self.constViewPurchaseBottom.constant = -200
            self.view.layoutIfNeeded()
          
        }
        else
        {
            self.lblSubscriptionData.isHidden = false
            lblSubscriptionData.text = "Your \(strInAppPurchasePrice) is currently active. Thank you for finding love with Hukume"
        }
        if isLive == 0 {
            self.constViewPurchaseBottom.constant = -200
            self.view.layoutIfNeeded()
        }
        
    }
    
    
    @IBAction func actionTermsAndPrivacy(_ sender: UIButton) {
        
        if sender.tag == 0
        {
            pushPolicyVC()
        }
        else
        {
            pushPolicyVC(isPushForPolicy: false)

        }
        
    }
    
    func pushPolicyVC(isPushForPolicy: Bool = true) {
        
        guard let aWebViewVC = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "WebViewLoaderVC") as? WebViewLoaderVC else { return }
        aWebViewVC.pageTitle = isPushForPolicy ? "Privacy Policy" : "Terms of Services"
        self.navigationController?.pushViewController(aWebViewVC, animated: true)
    }
    
    @IBAction func actionSubscribe(_ sender: Any) {
        
        UIView.animate(withDuration: 0.5, animations: {
            
            self.constViewPurchaseBottom.constant = 0
            self.view.layoutIfNeeded()
        }) { (tr) in

        }
        
        
    }
    
    
    @IBAction func actionSelectPlan(_ sender: UIButton) {
        
        self.setSelectedPlan(index: sender.tag)
        
    }
    @IBAction func actionSelectPlan2(_ sender: UIButton) {
    var index = -1
        if sender.tag == 4{
            index = 0
        }else if sender.tag ==  5{
            index = 1
        }else if sender.tag ==  6{
            index = 2
        }
        
        self.setSelectedPlan2(index: index)
        
    }
   
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }

    
    //MARK:-  new view methods
    
    @IBAction func actionPurchaseNewIapView(_ sender: Any) {
        
        if self.isFrom == "verificationVC"{
            let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "AddCardDetailsVC") as! AddCardDetailsVC
            vc.planId =  self.planId
            vc.monthsTitle =  self.arrPlans[selectedIndex]["name"].string ?? ""
            vc.priceTitle =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            self.navigationController?.pushViewController(vc, animated: true)
        }else{
        UIView.animate(withDuration: 0.3, animations: {
            
            self.constViewPurchaseBottom.constant = -200
            self.view.layoutIfNeeded()

        }) { (tr) in

        }
        
        IAPManager.shared.doPurchase(self, selectedPlan) { (isSucsess, aString) in
            if isSucsess{
                self.dismiss(animated: true, completion: nil)

                self.CallMakeSubscription()
                
             //   Common().showAlert(strMsg: "Plan purhcased !!!", view: self)
                
               // self.transitionToHome()
            }
        }
        }
        
    }
    
    
    @IBAction func actionMobileMoneyIapView(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.constViewPurchaseBottom.constant = -200

            self.constViewMobileMoneyBottom.constant = 0
            self.view.layoutIfNeeded()

        }) { (tr) in

        }
        
    }
    
    @IBAction func actionCloseViewPurchase(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.constViewPurchaseBottom.constant = -200
            self.view.layoutIfNeeded()

        }) { (tr) in

        }
        
    }
    
    
    
    @IBAction func actionDoneViewMobileMoney(_ sender: Any) {
        
        UIView.animate(withDuration: 0.3, animations: {
            
            self.constViewMobileMoneyBottom.constant = -550
            self.view.layoutIfNeeded()

        }) { (tr) in

        }
        
    }
    
    
    func setSelectedPlan(index : Int)
    {
        var strPrice = ""
        
        if index == 0
        {
            viewBlock3.backgroundColor = Constant.Color.themeRed
            viewBlock1.backgroundColor = UIColor.clear
            viewBlock2.backgroundColor = UIColor.clear

            viewSubList1.backgroundColor = UIColor.white
            viewSubList2.backgroundColor = UIColor.clear
            viewSublist3.backgroundColor = UIColor.clear

            imgSubList1.isHidden = false
            imgSublist2.isHidden = true
            imgSublist3.isHidden = true
            
            selectedPlan = InAppIDs.monthly
            selectedPlanPricing = IAPManager.shared.MonthlyPrice
            
            
            selectedIndex =  index
            if self.isFrom == "verificationVC"{
                planId = arrPlans[0]["_id"].string ?? ""
                strPrice =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            }else{
                strPrice = IAPManager.shared.MonthlyLocalizedPrice
            }
            
            txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money")
        }
        else if index == 1
        {
            viewBlock1.backgroundColor = Constant.Color.themeRed
            viewBlock3.backgroundColor = UIColor.clear
            viewBlock2.backgroundColor = UIColor.clear
            
            selectedIndex =  index
            
            viewSubList1.backgroundColor = UIColor.clear
            viewSubList2.backgroundColor = UIColor.white
            viewSublist3.backgroundColor = UIColor.clear
            
            imgSubList1.isHidden = true
            imgSublist2.isHidden = false
            imgSublist3.isHidden = true
           
            selectedPlan = InAppIDs.quarterly
            selectedPlanPricing = IAPManager.shared.QuarterlyPrice
            if self.isFrom == "verificationVC"{
                planId = arrPlans[1]["_id"].string ?? ""
                strPrice =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            }else{
                strPrice = IAPManager.shared.QuarterlyLocalizedPrice
            }
            
            txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money")
            
        }
        else if index == 2
        {
            viewBlock2.backgroundColor = Constant.Color.themeRed
            viewBlock3.backgroundColor = UIColor.clear
            viewBlock1.backgroundColor = UIColor.clear
            
            selectedIndex =  index
            
            viewSubList1.backgroundColor = UIColor.clear
            viewSubList2.backgroundColor = UIColor.clear
            viewSublist3.backgroundColor = UIColor.white
            
            imgSubList1.isHidden = true
            imgSublist2.isHidden = true
            imgSublist3.isHidden = false
           
            selectedPlan = InAppIDs.halfly
            selectedPlanPricing = IAPManager.shared.HalflyPrice
            if self.isFrom == "verificationVC"{
                planId = arrPlans[2]["_id"].string ?? ""
                strPrice =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            }else{
                strPrice = IAPManager.shared.HalflyLocalizedPrice
            }
            
            txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money")

            
        }
       
            UIView.animate(withDuration: 0.5, animations: {
                
                self.constViewPurchaseBottom.constant = 0
                self.view.layoutIfNeeded()
            }) { (tr) in

        }
        
    }
    func setSelectedPlan2(index : Int)
    {
        var strPrice = ""
        
        if index == 0
        {
           
            viewSubList12.backgroundColor = UIColor.white
            viewSubList22.backgroundColor = UIColor.clear
            viewSublist32.backgroundColor = UIColor.clear

            imgSubList12.isHidden = false
            imgSublist22.isHidden = true
            imgSublist32.isHidden = true
            
            selectedPlan = InAppIDs.monthly
            selectedPlanPricing = IAPManager.shared.MonthlyPrice
            
            
            selectedIndex =  index
            if self.isFrom == "verificationVC"{
                planId = arrPlans[0]["_id"].string ?? ""
                strPrice =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            }else{
                strPrice = IAPManager.shared.MonthlyLocalizedPrice
            }
            
            txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money")
        }
        else if index == 1
        {
            
            
            selectedIndex =  index
            
            viewSubList12.backgroundColor = UIColor.clear
            viewSubList22.backgroundColor = UIColor.white
            viewSublist32.backgroundColor = UIColor.clear
            
            imgSubList12.isHidden = true
            imgSublist22.isHidden = false
            imgSublist32.isHidden = true
           
            selectedPlan = InAppIDs.quarterly
            selectedPlanPricing = IAPManager.shared.QuarterlyPrice
            if self.isFrom == "verificationVC"{
                planId = arrPlans[1]["_id"].string ?? ""
                strPrice =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            }else{
                strPrice = IAPManager.shared.QuarterlyLocalizedPrice
            }
            
            txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money")
            
        }
        else if index == 2
        {
            
            
            selectedIndex =  index
            
            viewSubList12.backgroundColor = UIColor.clear
            viewSubList22.backgroundColor = UIColor.clear
            viewSublist32.backgroundColor = UIColor.white
            
            imgSubList12.isHidden = true
            imgSublist22.isHidden = true
            imgSublist32.isHidden = false
           
            selectedPlan = InAppIDs.halfly
            selectedPlanPricing = IAPManager.shared.HalflyPrice
            if self.isFrom == "verificationVC"{
                planId = arrPlans[2]["_id"].string ?? ""
                strPrice =  self.arrPlans[selectedIndex]["converted_price"].string ?? ""
            }else{
                strPrice = IAPManager.shared.HalflyLocalizedPrice
            }
            
            txtDescMobileMoney.attributedText = self.setAttributesForType2(fullStr: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money", boldTxt: "STEPS TO SUBSCRIBE WITH MOBILE MONEY ON HUKUME\n\n\(strPrice)", normalTxt: "\nStep 1: Send the full amount listed above to our mobile money number +233 546 872 809.\n\n Step 2: After payment is done, contact our customer support team on WhatsApp at +233 546 872 809, share with us the confirmation number of your payment and TopHub registered name and number.\n\nWhen these steps are done, our customer support team will automatically grant you access to start watching movies on TopHub using your mobile money\n\nAMOUNT WILL REFLECT IN YOUR SUBSRIPTION PAGE ONCE PAYMENT IS RECEIVED. Mobile Money")

            
        }
       
            UIView.animate(withDuration: 0.5, animations: {
                
                self.constViewPurchaseBottom.constant = 0
                self.view.layoutIfNeeded()
            }) { (tr) in

        }
        
    }
   
    
    func CallMakeSubscription(){
        
        
        var strPlanId = "1"
        
        
        if imgSubList1.isHidden == false
        {
            strPlanId = "1"
        }
        else if imgSublist2.isHidden == false
        {
            strPlanId = "2"
        }
        else if imgSublist3.isHidden == false
        {
            strPlanId = "3"
        }
        
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["price"] = "\(selectedPlanPricing)"
        param["product_id"] = selectedPlan
        param["is_expired"] = "0"
        param["plan_id"] = strPlanId

        print("\n ======= \n make subscription plan \n ======= \n \(param)")
        
        Networking.performApiCall(.makeSubscription(param), true, self) { (res) in
            print(res)
            //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                        }
                        
                        isUserSubscribe = true
                        self.lblSubscriptionData.text = "You have successfully paid for package subscription."

                        self.hidePlans()
                        
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                        
                    }else{
                       
                        isUserSubscribe = false

                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    
    func callPlusPlanAPIs(){
        var strPlanId = "1"
        self.showSpinner(onView: self.view)
        
        if imgSubList1.isHidden == false
        {
            strPlanId = "1"
        }
        else if imgSublist2.isHidden == false
        {
            strPlanId = "2"
        }
        else if imgSublist3.isHidden == false
        {
            strPlanId = "3"
        }
        
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        print("URL:- \(APIs.BASE_URL + APIs.HUKUME_PLUS_PLANS)")
        print("\n ======= \n make subscription plan \n ======= \n \(param)")
        Alamofire.request(APIs.BASE_URL + APIs.HUKUME_PLUS_PLANS, method: .post, parameters: param, encoding: URLEncoding.default, headers: [:]).responseJSON { (response) in
                switch response.result {
                case let .success(value):
                    print(value)
                    let tempvalue = value as! [String:Any]
                    let status = tempvalue["status"]as! Int
                    let message = tempvalue["msg"]as! String
                    if status == 1 {
                        let jsonObject =  JSON(value)
                        self.removeSpinner()
                        
                        self.arrPlans =  jsonObject["data"].arrayValue
                        self.setData()
                        if let isLive = tempvalue["is_live"] as? String {
                            if isLive == "0" {
                                self.btnMobilemoney.isHidden = true
                            } else {
                                self.btnMobilemoney.isHidden = false
                            }
                        }
//                        self.tblPackageList.delegate = self
//                        self.tblPackageList.dataSource = self
//                        self.tblPackageList.reloadData()
                        }else{
                            
                        }
                case let .failure(error):
                    print(error)
//                    SVProgressHUD.dismiss()
                    
                }
            }
        }
    func getSubscriptionPlansAPI(){
        var param:[String:Any] = [String:Any]()
        
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token

        print("\n ========= \n home screen userList api params \n ======= \n \(param)")

        
        Networking.performApiCall(.getSubscriptionPlan(param), !isFromSignUp, self) { (res) in
            print(res)
            if let dictData = res.result.value as? [String:Any]{
                
                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let str = dictData["isHukumePlusSubscribed"] as? Bool{
                            self.isHUKUMEPLUSSUBSRCIBE =  str
                        }
                        if let str = dictData["isHukumePlusPlanName"] as? String{
                            self.planName =  str
                        }
                        
                        if strServerPurchaseMsg != ""
                        {
                            self.lblSubscriptionData.text = strServerPurchaseMsg
                            self.hidePlans()
                        }
                        else{
                            
                            if isUserSubscribe == true
                            {
                                
                                
                                self.lblSubscriptionData.text = "Your \(strInAppPurchasePrice) is currently active. Thank you for finding love with Hukume"
                                self.hidePlans()
                            } else if self.isHUKUMEPLUSSUBSRCIBE == true{
                                print("You have successfully paid for \(self.planName) Hukume Plus subscription vatsal")
                                self.lblSubscriptionData.text = "You have successfully paid for \(self.planName) Hukume Plus subscription"
                                
                                self.hidePlans()
                            }
                            else
                            {
                                self.showPlans()
                            }
                        }
                        if let isLive = dictData["is_live"] as? String {
                            if isLive == "0" {
                                self.btnMobilemoney.isHidden = true
                            } else {
                                self.btnMobilemoney.isHidden = false
                            }
                        }
                    }
                    else{
                      
                        //Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                        
                    }
                }
                else
                {
                }
            }
            else{
                
                
            }
        }
    }
}
//extension SubscriptionVC : UITableViewDataSource, UITableViewDelegate{
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return arrPlans.count
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PackageTVCell
//
//        let item = arrPlans[indexPath.row]
//        let name = item["name"].string ?? ""
//        let price = item["converted_price"].string ?? ""
//        cell.lblTitle.text = "\(name) month : \(price)"
//        if indexPath.row ==  selectedIndex {
//            cell.viewBlock2.backgroundColor = Constant.Color.themeRed
//            cell.viewSubList2.backgroundColor = UIColor.clear
//            cell.imgSubList1.isHidden = true
//
//
//            viewBlock3.backgroundColor = Constant.Color.themeRed
//            viewBlock1.backgroundColor = UIColor.clear
//            viewBlock2.backgroundColor = UIColor.clear
//
//            viewSubList1.backgroundColor = UIColor.white
//            viewSubList2.backgroundColor = UIColor.clear
//            viewSublist3.backgroundColor = UIColor.clear
//
//            imgSubList1.isHidden = false
//            imgSublist2.isHidden = true
//            imgSublist3.isHidden = true
//
//        }else{
//            cell.viewBlock2.backgroundColor = UIColor.clear
//            cell.viewSubList2.backgroundColor = UIColor.clear
//            cell.imgSubList1.isHidden = true
//        }
//
//        return cell
//    }
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        let item = arrPlans[indexPath.row]
//        planId = item["_id"].string ?? ""
//        strPrice =  item["converted_price"].string ?? ""
//        self.selectedIndex =  indexPath.row
//        self.setSelectedPlan(index: indexPath.row)
//        self.tblPackageList.reloadData()
//    }
//
//
//}

extension UIImage {
    func imageWithColor(color1: UIColor) -> UIImage {
        UIGraphicsBeginImageContextWithOptions(self.size, false, self.scale)
        color1.setFill()

        let context = UIGraphicsGetCurrentContext()
        context?.translateBy(x: 0, y: self.size.height)
        context?.scaleBy(x: 1.0, y: -1.0)
        context?.setBlendMode(CGBlendMode.normal)

        let rect = CGRect(origin: .zero, size: CGSize(width: self.size.width, height: self.size.height))
        context?.clip(to: rect, mask: self.cgImage!)
        context?.fill(rect)

        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()

        return newImage!
    }
}

//MARK:- TableView cell
class PackageTVCell : UITableViewCell{
    
    
    @IBOutlet weak var viewMain : UIView!
    @IBOutlet weak var lblTitle : UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMain.layer.cornerRadius = 5.0
        viewMain.backgroundColor =  .black
        viewMain.layer.borderWidth = 1.0
        viewMain.layer.borderColor = UIColor.white.cgColor
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
    }
}
