//
//  FlagVC.swift
//  Hukumi
//
//  Created by ThisIsAnkur on 04/02/22.
//  Copyright © 2022 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON

class FlagVC: UIViewController {

    @IBOutlet var cvUserImages: UICollectionView!
    @IBOutlet weak var txtComment: UITextView!
    var imagePicker = UIImagePickerController()
    var arrImages: [UIImage] = []
    var arrNewImages: [UIImage] = []
    var isFromAddImages = false
    var reportId = "0"
   
  //  var objProfile:ModelProfile = ModelProfile(obj: JSON(["":""]))
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self
        txtComment.setRound(withBorderColor: .white, andCornerRadious: 10, borderWidth: 1)
        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnSubmitAction(_ sender: Any) {
        if txtComment.text!.isEmpty {
            Common().showAlert(strMsg: "Please enter your comment", view: self)
        } else if arrNewImages.count == 0 {
            Common().showAlert(strMsg: "Please upload your screen shot", view: self)
        } else {
            callReportUserApi()
            
        }
    }
    func callReportUserApi(){
        
            let dictParam:[String:Any] = [
                "loginuser_id":kCurrentUser.user_id!,
                "session_token":kCurrentUser.session_token!,
                "report_detail":txtComment.text!,
                "report_type":"2",
                "user_id":reportId
            ]
        Networking.uploadImagesWithParamsForReportUser(.reportUser(dictParam), imageDic: arrNewImages, dictParams: dictParam, showHud: true, onView: self) { (res) in
            switch res {
            case .success(let upload, _, _):

                upload.responseJSON { response in
                    if let dictData = response.result.value as? [String:Any]{
                        if let status = dictData["status"] as? Int {
                            if status == 1{
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                                self.navigationController?.popToRootViewController(animated: false)
                                if let tabVC = appDel.window?.rootViewController as? CustomTabbarVC {
                                    tabVC.selectedIndex = 0
                                    NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)
                                }
                                
                            }else{
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                            }
                        }else{
                            Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                        }
                    }
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                Common().showAlert(strMsg: "\(encodingError)", view: self)
            }
        }
           
        
    }

}
extension FlagVC:UICollectionViewDataSource,UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      //  guard indexPath.row < objProfile.images.count else {

        guard indexPath.row < self.arrImages.count else {

            let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserAddImagesTblCell", for: indexPath)

            return aCell
        }
        
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserImagesTblCell", for: indexPath) as! UserImagesTblCell
        
        
//        if indexPath.row == 0
//        {
//            self.arrImages.removeAll()
//        }
        
//        let aImage = objProfile.images[indexPath.row]
//
//        KingfisherManager.shared.retrieveImage(with: URL(string:aImage)!) { result in
//            let image = try? result.get().image
//            if let image = image {
//                aCell.imgViewUser.image = image
//                self.arrImages.append(image)
//
//            }
//        }

        aCell.imgViewUser.image = arrImages[indexPath.row]
        
        aCell.didSelectOption = { [weak self] in
            
            print("image picker")
            
            if self!.arrImages.count > 0
            {
                
              //  self!.objProfile.images.remove(at: indexPath.row)
                self?.arrImages.remove(at: indexPath.row)
                self!.cvUserImages.reloadData()
            }
            else
            {


            }

        }
        
        return aCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       // objProfile.images.count + 1
        
        self.arrImages.count == 0 ? (self.arrImages.count + 1) : self.arrImages.count
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row < arrImages.count else {
            
            isFromAddImages = true
            self.OpenAlertForImagePicker()
            return
        }
    }
    
}
extension FlagVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func OpenAlertForImagePicker(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {_ in
            
            self.isFromAddImages = false
        }))

        self.present(alert, animated: true, completion: nil)

    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if isFromAddImages == true{
            if let pickedImage = info[.editedImage] as? UIImage {
                
                let compressData = pickedImage.jpegData(compressionQuality: 0.5) //max value is 1.0 and minimum is 0.0
                let compressedImage = UIImage(data: compressData!)
                
  
                self.arrImages.append(compressedImage!)
                self.arrNewImages.append(compressedImage!)

//                self.arrImages.append(pickedImage)
                
                let dict = ["id":"0","image":""]
                
               // self.objProfile.images.append(dict)
                
            }
            else if let pickedImage = info[.originalImage] as? UIImage {
                
                let compressData = pickedImage.jpegData(compressionQuality: 0.5) //max value is 1.0 and minimum is 0.0
                let compressedImage = UIImage(data: compressData!)
                
  
                self.arrImages.append(compressedImage!)
                self.arrNewImages.append(compressedImage!)

               // self.arrImages.append(pickedImage)
                
                let dict = ["id":"0","image":""]
                
              //  self.objProfile.images.append(dict)
            }
            
            self.cvUserImages.reloadData()
        }
        

        self.imagePicker.dismiss(animated: true, completion: nil)

        
    }
}
