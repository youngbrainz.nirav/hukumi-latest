//
//  VerifyImageVC.swift
//  Hukumi
//
//  Created by VATSAL on 11/18/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

class VerifyImageVC: UIViewController,DISMISSWEBVC {
   
    

    @IBOutlet weak var  imgProfile : UIImageView!
    @IBOutlet weak var  viewError : UIView!
    
    var imgDisplay : UIImage!
    var isFaceCancel = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        imgProfile.image = imgDisplay
        self.viewError.isHidden = true
    }
    
    @IBAction func btnBackAction(_ sender: UIButton) {
          self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnVerifyPictureAction(_ sender: UIButton) {
        
//        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
//        vc.isFrom = "verificationVC"
//        self.navigationController?.pushViewController(vc, animated: true)
        callCreateFaceImg()
    }
    @IBAction func btnRetakeAction(_ sender: UIButton) {
        let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = false
        vc.delegate = self
        present(vc, animated: true)
    }
    @IBAction func btnOkAction(_ sender: UIButton) {
        self.viewError.isHidden = true
    }
    func getDetails(isCancelFace: Bool) {
        self.isFaceCancel =  isCancelFace
        if isFaceCancel == true{
            self.viewError.isHidden = false
        }else{
            self.viewError.isHidden = true
        }
    }
    
    func callCreateFaceImg(){
        var param:[String:Any] = [String:Any]()
        showSpinner(onView: self.view)
        param["loginuser_id"] = kCurrentUser.user_id
        
       print("\n ========= \n update profile Verify parama \n ====== \n \(param)")
        let image = imgProfile.image!
        let imgData = image.jpegData(compressionQuality: 0.2)!

       Alamofire.upload(multipartFormData: { multipartFormData in
               multipartFormData.append(imgData, withName: "face_image",fileName: "file.jpg", mimeType: "image/jpg")
               for (key, value) in param {
                   multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                   } //Optional for extra parameters
           },
       to: APIs.BASE_URL + APIs.CREATE_FACEIMG)
       { (result) in
           switch result {
           case .success(let upload, _, _):

               upload.uploadProgress(closure: { (progress) in
                   print("Upload Progress: \(progress.fractionCompleted)")
               })

               upload.responseJSON { response in
                   let data =  JSON(response.result.value)
                   let objData = data["data"].dictionaryValue
                   print(objData,"Dictionary")
                   var strUrl = ""
                   var strUrl2 = ""
                   if let str = objData["faceDetectImage"]?.stringValue{
                       strUrl = str
                       
                   }
                   if let str = objData["databaseFaceImage"]?.stringValue{
                       strUrl2 = str
                       
                   }
                   self.removeSpinner()
                   let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "WebViewVC") as! WebViewVC
                   vc.isFrom = "verificationVC"
                   vc.strUrl =  strUrl
                   vc.strUrl2 =  strUrl2
                   vc.delegateVC =  self
                   self.navigationController?.pushViewController(vc, animated: true)
                   
               }

           case .failure(let encodingError):
               print(encodingError)
               self.removeSpinner()
           }
       }
        
    }

}
extension VerifyImageVC : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }

        self.imgProfile.image = image



        // print out the image size as a test
        print(image.size)
    }
}
