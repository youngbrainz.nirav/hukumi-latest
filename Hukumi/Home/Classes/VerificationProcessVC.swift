//
//  VerificationProcessVC.swift
//  Hukumi
//
//  Created by VATSAL on 11/18/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit

class VerificationProcessVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    @IBAction func btnBackAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func btnTakePictureAction(_ sender: Any) {
       
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            let vc = UIImagePickerController()
        vc.sourceType = .camera
        vc.allowsEditing = false
        vc.delegate = self
        present(vc, animated: true)
        }
        else {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
        
    }
}
extension VerificationProcessVC : UINavigationControllerDelegate,UIImagePickerControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        picker.dismiss(animated: true)

        guard let image = info[.originalImage] as? UIImage else {
            print("No image found")
            return
        }
        
        
        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "VerifyImageVC") as! VerifyImageVC
        vc.imgDisplay =  image
        self.navigationController?.pushViewController(vc, animated: true)
        
        

        // print out the image size as a test
        print(image.size)
    }
}
