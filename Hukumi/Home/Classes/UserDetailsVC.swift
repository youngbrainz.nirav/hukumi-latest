//
//  UserDetailsVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 31/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import CoreLocation
import Foundation
import ImageViewer

class UserDetailsVC: UIViewController {

    @IBOutlet weak var lblCity: UILabel!
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var ingUser: UIImageView!
    @IBOutlet weak var imgVerified: UIImageView!
    @IBOutlet weak var lblGender: UILabel!
    @IBOutlet weak var lblEducation: UILabel!
    @IBOutlet weak var lblintro: UILabel!
    @IBOutlet weak var lblPassion: UILabel!
    @IBOutlet weak var widthForReport: NSLayoutConstraint!
    @IBOutlet weak var lblReport: UILabel!
    
    var objUser:ModelMatches?
    var homeDelegate : ProcessStatusDelegate?
   // var arrMatches : [ModelMatches] = [ModelMatches]()

    @IBOutlet weak var viewStartChat: UIView!
    
    var isUserSubscribed = false
    var subscriptionMsg = ""
    var isFrom = ""
    
    @IBOutlet weak var viewAcceptReject: UIView!
    
    
    @IBOutlet weak var viewCancle: UIView!
    
    @IBOutlet weak var viewLike: UIView!
    @IBOutlet weak var viewFlag: UIView!
    
    @IBOutlet weak var imgFlag: UIImageView!
    @IBOutlet weak var imgDislike: UIImageView!
    
    @IBOutlet weak var imgLike: UIImageView!
    
    
    
    var userImages:[[String:String]] = [[String:String]]()
    var arrImages: [UIImage] = []

    
    @IBOutlet weak var imgUserImages: UIImageView!
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // GetPassionList()
        CallGetFeedDetail()
        CallCheckSubscription()
        
        self.lblCity.text = ""
        
        imgDislike.image = imgDislike.image?.imageWithColor(color1: UIColor.red)
        imgLike.image = imgLike.image?.imageWithColor(color1: UIColor(red: 43.0/255.0, green: 189.0/255.0, blue: 172.0/255.0, alpha: 1.0))
        
        
        viewCancle.layer.borderWidth = 2
        viewCancle.layer.borderColor = UIColor.red.cgColor
        viewCancle.layer.masksToBounds = true
        
        viewFlag.layer.borderWidth = 2
        viewFlag.layer.borderColor = UIColor.red.cgColor
        viewFlag.layer.masksToBounds = true
        
        viewLike.layer.borderWidth = 2
        viewLike.layer.borderColor = UIColor(red: 43.0/255.0, green: 189.0/255.0, blue: 172.0/255.0, alpha: 1.0).cgColor
        viewLike.layer.masksToBounds = true
        
        viewLike.layer.cornerRadius = viewLike.frame.size.width/2
        viewFlag.layer.cornerRadius = viewFlag.frame.size.width/2
        viewCancle.layer.cornerRadius = viewCancle.frame.size.width/2
        
        imgUserImages.isHidden = true
        
        let aImg = objUser?.profile_image ?? ""
        
        KingfisherManager.shared.retrieveImage(with: URL(string:aImg)!) { result in
            let image = try? result.get().image
            if let image = image {
                self.ingUser.image = image
                self.arrImages.insert(image, at: 0)
                self.imgUserImages.isHidden = false
            }
        }

        
        self.lblName.text = "\(self.objUser?.user_name ?? "")"
        

        
        if self.objUser?.isHukumePlusUser == "0" || objUser?.isHukumePlusUser == ""
        {
            imgVerified.isHidden = true
        }
        else{
            imgVerified.isHidden = false

        }
        self.lblintro.text = self.objUser?.career_experiance
        
        //self.lblCity.text = self.objUser?.city
        
//        let myLocation = CLLocation(latitude: Double(kCurrentUser.lattitude!) ?? 0.0, longitude: Double(kCurrentUser.longitude!) ?? 0.0)
//
//        //My buddy's location
//        let myBuddysLocation = CLLocation(latitude: Double(self.objUser!.lattitude) ?? 0.0, longitude: Double(self.objUser!.longitude) ?? 0.0)
//
//        //Measuring my distance to my buddy's (in km)
//        let distance = myLocation.distance(from: myBuddysLocation) / 1609
//
//        let strCity = self.objUser?.city
//        self.lblCity.text = String(format:"\(strCity!), (%.2f miles)",distance)
        
        // Do any additional setup after loading the view.
        
      
        //Here you need to set your origin and destination points and mode
        
        
//        let url = NSURL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(kCurrentUser.lattitude!),\(kCurrentUser.longitude!)&destination=\(self.objUser!.lattitude),\(self.objUser!.longitude)&sensor=false&key=AIzaSyCUIAUr8iTcnyxaFwTG7kv3CRV9XxIcNwk")
//
//        print(url)
//
//        if url != nil
//        {
//
//            let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
//
//                do {
//                    if data != nil {
//                        let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as!  [String:AnyObject]
//                        //                        print(dic)
//
//                        let status = dic["status"] as! String
//                        var routesArray:String!
//
//                        if status == "OK" {
//
//                            routesArray = (((((dic["routes"]!as! [Any])[0] as! [String:Any])["legs"] as! [Any])[0] as! [String:Any])["distance"] as! [String:Any])["text"] as! String
//
//
//                            print(routesArray)
//
//                            let strCity = self.objUser?.city
//
//
//                            DispatchQueue.main.async {
//
//                                self.lblCity.text = String(format:"\(strCity!),  (\(routesArray!))")
//
//                            }
//
//                            let geoCoder = CLGeocoder()
//
//                            let lat = Double(self.objUser!.lattitude)
//                            let long = Double(self.objUser!.longitude)
//
//                            let location = CLLocation(latitude: lat!, longitude:  long!) // <- New York
//
//                            var strState = ""
//
//                            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in
//
//                                placemarks?.forEach { (placemark) in
//
//                                    if let city = placemark.locality { print(city) } // Prints "New York"
//
//                                    if let state = placemark.administrativeArea {
//
//                                        print(state)
//
//                                        strState = state
//
//
//                                        DispatchQueue.main.async {
//
//
//                                            self.lblCity.text = String(format:"\(strCity!), \(strState), (\(routesArray!))")
//
//                                        }
//
//
//                                    } // Prints "New York"
//
//
//                                    if let dict = placemark.addressDictionary {
//
//                                        print(dict)
//
//                                    }
//
//                                }
//                            })
//
//
//                            //                            print("routesArray: \(String(describing: routesArray))")
//                        }
//                        else
//                        {
//
//
//                            let strCity = self.objUser?.city
//
//
//                            let geoCoder = CLGeocoder()
//
//                            let lat = Double(self.objUser!.lattitude)
//                            let long = Double(self.objUser!.longitude)
//
//                            //                                let lat = Double("23.050849731756728")
//                            //                                let long = Double("72.51990485784967")
//
//
//                            let location = CLLocation(latitude: lat!, longitude:  long!) // <- New York
//
//                            var strState = ""
//
//                            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in
//
//                                placemarks?.forEach { (placemark) in
//
//                                    if let city = placemark.locality { print(city) } // Prints "New York"
//
//                                    if let state = placemark.administrativeArea {
//
//                                        print(state)
//
//                                        strState = state
//
//
//                                        DispatchQueue.main.async {
//
//
//
//                                            self.lblCity.text = String(format:"\(strCity!), \(strState)")
//
//                                        }
//
//
//                                    } // Prints "New York"
//
//
//                                    if let dict = placemark.addressDictionary {
//
//                                        print(dict)
//
//                                    }
//
//                                }
//                            })
//
//
//
//                        }
//
//                        DispatchQueue.main.async {
//                            //                                let path = GMSPath.init(fromEncodedPath: routesArray!)
//                            //                                let singleLine = GMSPolyline.init(path: path)
//                            //                                singleLine.strokeWidth = 6.0
//                            //                                singleLine.strokeColor = .blue
//                            //                                singleLine.map = mapView
//                        }
//
//                    }
//                } catch {
//                    print("Error")
//                }
//            }
//
//            task.resume()
//
//        }
        
    }
    
    @IBAction func btnCancel(_ sender: Any) {
        self.CallAcceptReject(strID: objUser?._id ?? "", status: "0")
    }
    
    @IBAction func btnLike(_ sender: Any) {
        self.CallAcceptReject(strID: objUser?._id ?? "", status: "1")
    }
    
    @IBAction func btnBackClick(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func actionBtnReportUser(_ sender: Any) {
        openReportUserDialog()
    }
    
    
    @IBAction func actionStartChat(_ sender: Any) {
        
//        if isUserSubscribe == false{
//            Common().showAlert(strMsg: (self.subscriptionMsg), view: self)
//        }
//       else
//        {
        self.CallStartConversation()
       // }
        
    }
    
    
    @IBAction func actionRemoveFriend(_ sender: Any) {
        
        
        let alert = UIAlertController(title: "Are you sure want to remove this friend?", message: nil, preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Yes", style: .default, handler: { _ in
            self.CallRemoveFriend()
        }))

        alert.addAction(UIAlertAction(title: "No", style: .default, handler: { _ in
        }))

       

        self.present(alert, animated: true, completion: nil)
        
        
    }
    
    @IBAction func actionUserImages(_ sender: Any) {
        
//        if userImages.count > 0
//        {
        self.presentImageGallery(GalleryViewController(startIndex: 0, itemsDataSource: self))
      //  }
        
    }
    
    
    
    func CallCheckSubscription(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        
        Networking.performApiCall(.checkSubscription(param), false, self) { (res) in
            print(res)
            //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.isUserSubscribed = true
                        
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                        }
                    }else{
                        self.isUserSubscribed = false
                        self.subscriptionMsg = dictData["msg"] as? String ?? "something went wrong!"
                        
                       // Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    func GetPassionList(){
        
        Networking.performApiCall(.getPassionList(["":""]), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.GetEducationList()
                        if let ArrData = dictData["data"] as? [[String:Any]]{
                            var strPassion = ""
                            
                            for obj in ArrData {
                                var str = self.objUser?.passions ?? ""
                                
                                str = str.trimmingCharacters(in: .whitespaces)

                                
                                let arrStr = str.components(separatedBy: ",")
                              //  let arrStr = str.map{ String($0) }
                                
                                for strInner in arrStr{
                                    if strInner == obj["_id"] as! String{
                                        strPassion = strPassion.appending("\(obj["name"] as? String ?? ""),")
                                    }else{
                                        
                                    }
                                }
                            }
                            
                            if strPassion != ""
                            {
                            strPassion.removeLast()
                            }
                            
                            self.lblPassion.text = strPassion
                            
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
    func GetEducationList(){
        
        Networking.performApiCall(.getEducationList(["":""]), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let ArrData = dictData["data"] as? [[String:Any]]{
                            for obj in ArrData{
                                if obj["_id"] as? String == self.objUser?.education_level{
                                    self.lblEducation.text = obj["name"] as? String ?? ""
                                }
                            }
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
    func CallAcceptReject(strID:String,status:String){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        if self.isFrom == "whoLikeMe"{
            param["screen_type"] = "1"
        }else{
            param["screen_type"] = "2"
        }
        
        param["requested_id"] = strID
        param["requsted_status"] = status

        print("\n ========= \n match api params \n ======= \n \(param)")

        Networking.performApiCall(.changeRequstedMatchesRecord(param), false, self) { (res) in
            print("\n ======== \n match api responce \n ======= \n \(res)")
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.homeDelegate?.reloadData()
                        self.navigationController?.popViewController(animated: true)
                        
                        if dictData["msg"] as! String != ""{
                          //  Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                        }
                        
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
}

//MARK:- API Call
extension UserDetailsVC{
    
    func CallStartConversation(){
        var param:[String:Any] = [String:Any]()
        param["sender_id"] = kCurrentUser.user_id
      //  param["session_token"] = kCurrentUser.session_token
        param["recipient_id"] = objUser?._id

        Networking.performApiCall(.startConversation(param), true, self) { [self] (res) in
            print(res)
          //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [String:Any]{
                            
                           
                            print("Success")
                            
                            guard let aChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC else { return }
                            
                            let strName = objUser?.user_name
                            let strConvoId = "\(data["conversation_id"]!)"
                            let strSenderId = objUser?._id

                            aChatVC.senderName = strName!
                            aChatVC.conversationId = strConvoId
                            aChatVC.isUserSubscribed = isUserSubscribe
                            aChatVC.senderID = strSenderId!

                            self.navigationController?.pushViewController(aChatVC, animated: true)
                            
                            (self.parent?.parent as? UINavigationController)?.pushViewController(aChatVC, animated: true)
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    func CallRemoveFriend(){
        
        var param:[String:Any] = [String:Any]()
        
        param["loginuser_id"] = kCurrentUser.user_id
        param["user_id"] = objUser?._id

        Networking.performApiCall(.removeFriend(param), true, self) { [self] (res) in
            print(res)
          //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        self.navigationController?.popViewController(animated: true)

                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    
    func CallGetFeedDetail(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["user_id"] = objUser?._id

        self.showSpinner(onView: self.view)
        print("PARAM \(param)")
        /*
         
         1 is  chat button
         0 is intrested
         */
        var lat =  0.0
        var long =  0.0
        Networking.performApiCall(.getFeedDetail(param), false, self) { [self] (res) in
            print(res)
          //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1
                    {
                        if let data = dictData["data"] as? [String:Any]{
                            
                            if let x = data["images"] as? [[String:Any]] {
                                
                                if x.count > 0
                                {
                                    
                                }
                                else
                                {
                                    self.removeSpinner()

                                }
                                
                                
                                for xx in x{
                                    
                                    
                                    var dict = [String:String]()
                                    
                                    if let img = xx["image"] as? String
                                    {
                                        dict["image"] = img
                                        
                                        userImages.append(dict)
                                        
                                        
                                    }
                                    
                                    let group = DispatchGroup()
                                    group.enter()
                                    DispatchQueue.global(qos: .userInitiated).async { [self] in
                                        // Do work asyncly and call group.leave() after you are done
                                        for i in 0..<self.userImages.count
                                        {
                                            
                                            let aImage = userImages[i]
                                            
                                            
                                            KingfisherManager.shared.retrieveImage(with: URL(string:aImage["image"]!)!) { result in
                                                let image = try? result.get().image
                                                if let image = image {
                                                    // aCell.imgViewUser.image = image
                                                    self.arrImages.append(image)
                                                    
                                                    if i == self.userImages.count - 1
                                                    {
                                                        group.leave()
                                                        
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                    
                                    
                                    group.notify(queue: .main, execute: {
                                        // This will be called when block ends
                                        
//                                        if self.arrImages.count == 0
//                                        {
//                                            imgUserImages.isHidden = true
//                                        }
//                                        else
//                                        {
//                                            imgUserImages.isHidden = false
//
//                                        }
                                        
                                        self.removeSpinner()

                                        
                                    })
                                    
                                }
                            }
                            if let report = data["is_report_user"] as? Int {
                                if report == 0 {
                                    self.widthForReport.constant = 65
                                    self.lblReport.isHidden = false
                                    
                                } else {
                                    self.widthForReport.constant = 0
                                    self.lblReport.isHidden = true
                                }
                            }
                            
                            if let str =  data["gender"] as? String {
                                if str == "1"{
                                    print("\(str) Gender Check")
                                    self.lblGender.text = "Male"
                                }else if str == "2"{
                                    print("\(str) Gender Check")
                                    self.lblGender.text = "Female"
                                }else if str == "3"{
                                    self.lblGender.text = "Other"
                                    print("\(str) Gender Check")
                                }
                            }
                            
                            if let isApproved = data["is_approved"] as? String
                            {
                                
                                self.objUser?.is_approved = isApproved
                                
                            }
                            if let isApproved = data["lattitude"] as? String
                            {
                                
                                self.objUser?.lattitude = isApproved
                                
                            }
                            if let isApproved = data["longitude"] as? String
                            {
                                
                                self.objUser?.longitude = isApproved
                                
                            }
                            
                            
                            self.lblName.text = "\(self.objUser?.user_name ?? ""), \(data["age"]!)"

                            var strDistanceFromRes = ""
                           
                            if let distance = data["distance"] as? String
                            {
                                strDistanceFromRes = "\(distance) Miles"
                            }
                            
                            if let EducationList = data["education_level"] as? String
                            {
                                
                                if EducationList == ""
                                {
                                    self.lblEducation.text = " - "
                                }
                                else{
                                self.lblEducation.text = EducationList
                                }
                            }
                            
                            //self.objUser = (ModelMatches(obj: JSON(data)))
                            if self.isFrom == "whoLikeMe"{
                                self.viewAcceptReject.isHidden = false
                                self.viewStartChat.isHidden = true
                            }else{
                                if self.objUser?.is_approved == "1"
                                {
                                    
                                    self.viewStartChat.isHidden = false
                                    self.viewAcceptReject.isHidden = true
                                    if let report = data["is_report_user"] as? Int {
                                        if report != 0 {
                                            self.viewStartChat.isHidden = true
                                        }
                                    }
                                }
                                else{
                                    self.viewStartChat.isHidden = true
                                    self.viewAcceptReject.isHidden = false
                                }
                            }
                            
                            
                            
                            if let strPassions = data["passions"] as? String
                            {
                                
//                                self.objUser?.passions = strPassions
//                                self.GetPassionList()
                                
                                self.lblPassion.text = strPassions

                                
                            }
                            
                            let strCity = "\(data["city"]!)"

                            let strLat = "\(data["lattitude"]!)"
                            let strLong = "\(data["longitude"]!)"

                            //Here you need to set your origin and destination points and mode
                            let url = NSURL(string: "https://maps.googleapis.com/maps/api/directions/json?origin=\(kCurrentUser.lattitude!),\(kCurrentUser.longitude!)&destination=\(data["lattitude"]!),\(data["longitude"]!)&sensor=false&key=AIzaSyCUIAUr8iTcnyxaFwTG7kv3CRV9XxIcNwk")

                            print(url)
                            

                            
                            if url != nil
                            {
                                
                                let task = URLSession.shared.dataTask(with: url! as URL) { (data, response, error) -> Void in
                                    
                                    do {
                                        if data != nil {
                                            let dic = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableLeaves) as!  [String:AnyObject]
                                            //                        print(dic)
                                            
                                            let status = dic["status"] as! String
                                            var routesArray:String!
                                            
                                            if status == "OK" {
                                                
                                                routesArray = (((((dic["routes"]!as! [Any])[0] as! [String:Any])["legs"] as! [Any])[0] as! [String:Any])["distance"] as! [String:Any])["text"] as! String
                                                
                                                
                                                print(routesArray)
                                                
                                                
                                                
                                                DispatchQueue.main.async {
                                                    
                                                    self.lblCity.text = strCity
                                                    
                                                }
                                                
                                                let geoCoder = CLGeocoder()
                                                
                                                let lat = Double(self.objUser!.lattitude)
                                                let long = Double(self.objUser!.longitude)
                                                
                                                let location = CLLocation(latitude: lat!, longitude:  long!) // <- New York
                                                
                                                var strState = ""
                                                
                                                geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in
                                                    
                                                    placemarks?.forEach { (placemark) in
                                                        
                                                        if let city = placemark.locality { print(city) } // Prints "New York"
                                                        
                                                        if let state = placemark.administrativeArea {
                                                            
                                                            print(state)
                                                            
                                                            strState = state
                                                            
                                                            
                                                            DispatchQueue.main.async {
                                                                
                                                                
                                                                self.lblCity.text = String(format:"\(strCity), \(strState), (\(routesArray!))")
                                                                
                                                            }
                                                            
                                                            
                                                        } // Prints "New York"
                                                        
                                                        
                                                        if let dict = placemark.addressDictionary {
                                                            
                                                            print(dict)
                                                            
                                                        }
                                                        
                                                    }
                                                })
                                                
                                                
                                                //                            print("routesArray: \(String(describing: routesArray))")
                                            }
                                            else
                                            {
                                                
                                                
                                                
                                                
                                                
                                                let strCity = strCity
                                                
                                                
                                                let geoCoder = CLGeocoder()
                                                
                                                let lat = Double(strLat)
                                                let long = Double(strLong)
                                                
                                                //                                let lat = Double("23.050849731756728")
                                                //                                let long = Double("72.51990485784967")
                                                
                                                
                                                let location = CLLocation(latitude: lat!, longitude:  long!) // <- New York
                                                
                                                var strState = ""
                                                
                                                geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in
                                                    
                                                    placemarks?.forEach { (placemark) in
                                                        
                                                        if let city = placemark.locality { print(city) } // Prints "New York"
                                                        
                                                        if let state = placemark.administrativeArea {
                                                            
                                                            print(state)
                                                            
                                                            strState = state
                                                            
                                                            
                                                            DispatchQueue.main.async {
                                                                
                                                                
                                                                
                                                                self.lblCity.text = String(format:"\(strCity), \(strState), \(strDistanceFromRes)")
                                                                
                                                            }
                                                            
                                                            
                                                        } // Prints "New York"
                                                        
                                                        
                                                        if let dict = placemark.addressDictionary {
                                                            
                                                            print(dict)
                                                            
                                                        }
                                                        
                                                    }
                                                })
                                                
                                                
                                                
                                            }
                                            
                                            DispatchQueue.main.async {
                                                //                                let path = GMSPath.init(fromEncodedPath: routesArray!)
                                                //                                let singleLine = GMSPolyline.init(path: path)
                                                //                                singleLine.strokeWidth = 6.0
                                                //                                singleLine.strokeColor = .blue
                                                //                                singleLine.map = mapView
                                            }
                                            
                                        }
                                    } catch {
                                        print("Error")
                                    }
                                }
                                
                                task.resume()
                                
                            }
                            
                            
//                            let geoCoder = CLGeocoder()
//
//                            let lat = Double(self.objUser!.lattitude)
//                            let long = Double(self.objUser!.longitude)
//
//                            let location = CLLocation(latitude: lat!, longitude:  long!) // <- New York
//
//                            let distance = "\(data["distance"]!)"
//
//                            let strCity = self.objUser?.city
//
//                            self.lblCity.text = String(format:"\(strCity!), (\(distance) miles)",distance)
//
//
//                            var strState = ""
//
//                            geoCoder.reverseGeocodeLocation(location, completionHandler: { (placemarks, _) -> Void in
//
//                                placemarks?.forEach { (placemark) in
//
//                                    if let city = placemark.locality { print(city) } // Prints "New York"
//
//                                    if let state = placemark.administrativeArea {
//
//                                        print(state)
//
//                                        strState = state
//
//
//                                        self.lblCity.text = String(format:"\(strCity!), \(strState), (\(distance) miles)",distance)
//
//
//                                    } // Prints "New York"
//
//
//                                    if let dict = placemark.addressDictionary {
//
//                                        print(dict)
//
//                                    }
//
//                                }
//                            })
                            
                            
                            
                           
                            
//                            for obj in data{
//                                self.arrMatches.append(ModelMatches(obj: JSON(obj)))
//                            }
                            
                           // self.cvMatches.reloadData()
                            
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    func openReportUserDialog(){
        let dialog = CustomDialogForConfirmation.ShowCustomDialogForConfirmation(title: "", msg: "Are you sure you want to report this user?", titleBtnLeft: "No".uppercased(), titleBtnRight: "Yes".uppercased())
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {
            dialog.removeFromSuperview()
            self.openReportCommentDialog()
        }
    }
    func openReportCommentDialog(){
        let dialog = CustomDialogForConfirmation.ShowCustomDialogForConfirmation(title: "", msg: "Enter your comment", titleBtnLeft: "", titleBtnRight: "Confirm", isTextfiledHidden: false, tag: 400)
        dialog.onConfirmReport = { text in
            dialog.removeFromSuperview()
            self.callReportUserApi(text: text)
        }
    }
    
    func callReportUserApi(text:String){
        
            let dictParam:[String:Any] = [
                "loginuser_id":kCurrentUser.user_id!,
                "session_token":kCurrentUser.session_token!,
                "report_type":"1",
                "report_detail":text,
                "user_id":objUser!._id
            ]
        Networking.uploadImagesWithParamsForReportUser(.reportUser(dictParam), imageDic: [], dictParams: dictParam, showHud: true, onView: self) { (res) in
            switch res {
            case .success(let upload, _, _):

                upload.responseJSON { response in
                    if let dictData = response.result.value as? [String:Any]{
                        if let status = dictData["status"] as? Int {
                            if status == 1{
                                self.widthForReport.constant = 0
                                self.lblReport.isHidden = true
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                                //self.navigationController?.popViewController(animated: true)
                                
                            }else{
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                            }
                        }else{
                            Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                        }
                    }
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                Common().showAlert(strMsg: "\(encodingError)", view: self)
            }
        }
          
        
    }
}

extension UserDetailsVC: GalleryItemsDataSource {
    func itemCount() -> Int {
        return userImages.count + 1
    }

    func provideGalleryItem(_ index: Int) -> GalleryItem {
        
        var galleryItem: GalleryItem!

        let image = arrImages[index]
        
        galleryItem = GalleryItem.image{ $0(image) }

        
        return galleryItem
    }
}
