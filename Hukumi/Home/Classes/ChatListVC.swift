//
//  ChatListVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 03/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Kingfisher
import SwiftyJSON
class ChatListVC: MasterVC {

    @IBOutlet weak var tblChat: UITableView!
    
    var chatListVM = ChatListVM()
    // var arrChatList : [ModelChat] = [ModelChat]()
   
    var timerCallChatApi = Timer()

    var isFirstTimeAPiCall = false
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        
      //  self.CallGetChatList()

        isFirstTimeAPiCall = true
        self.startApiCounter()
        
        self.CallCheckSubscription()
        
    }
    
    func startApiCounter()
    {
        timerCallChatApi.invalidate() // just in case this button is tapped multiple times

               // start the timer
        timerCallChatApi = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(CallGetChatList), userInfo: nil, repeats: true)
        timerCallChatApi.fire()
    }
    
    func stopApiCounter()
    {
        timerCallChatApi.invalidate() // just in case this button is tapped multiple times

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        self.stopApiCounter()
    }
    
    func initSetup() {
        
        tblChat.dataSource = chatListVM
        tblChat.delegate = chatListVM
        tblChat.reloadData()
        
        chatListVM.didSelectChat = { [weak self] aChatModel in
            
            if self?.chatListVM.isUserSubscribed == false {
                
               // Common().showAlert(strMsg: (self?.chatListVM.subscriptionMsg)!, view: self!)
                self?.showChat(OfUser: aChatModel.senderName, conversationId: aChatModel.conversation_id, isUserSubscribe: false, senderId: aChatModel.to_user_id)
                
            }
           else
            {
                self?.showChat(OfUser: aChatModel.senderName, conversationId: aChatModel.conversation_id, isUserSubscribe: true, senderId: aChatModel.to_user_id)
                
           
            }
        }
    }
    
    func CallCheckSubscription(){
        
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        
        Networking.performApiCall(.checkSubscription(param), false, self) { (res) in
            print(res)
            //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        self.chatListVM.isUserSubscribed = true
                        
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                        }
                        
                    }else{
                        
                        self.chatListVM.isUserSubscribed = false
                        self.chatListVM.subscriptionMsg = dictData["msg"] as? String ?? "something went wrong!"
                        
                       // Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    @objc func CallGetChatList(){
        var param:[String:Any] = [String:Any]()
        param["user_id"] = kCurrentUser.user_id
        param["timezone"] = "Asia/Calcutta"

        var wantLoader = false
        
        if isFirstTimeAPiCall == true
        {
            wantLoader = true
            isFirstTimeAPiCall = false
        }
        else{
            
            wantLoader = false
            
        }
        
        
        Networking.performApiCall(.getConversationList(param), wantLoader, self) { (res) in
            print(res)
          //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                           
                            
                            //self.objUser = (ModelMatches(obj: JSON(data)))
                           
                            
                            self.chatListVM.arrChatList.removeAll()
                            
                            for obj in data{
                                let user = ModelChat(obj: JSON(obj))
                                if user.isBlock != 1 && user.is_report_user == 0 {
                                self.chatListVM.arrChatList.append(user)
                                }
                            }
                            
                            self.initSetup()

                            if let tabItems = self.tabBarController?.tabBar.items {
                                // In this case we want to modify the badge number of the third tab:
                               
                                let tabItemMatches = tabItems[2]
                                
                                
                                    tabItemMatches.badgeValue = nil
                               
                                
                            }
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    func showChat(OfUser aUser: String, conversationId : String, isUserSubscribe : Bool, senderId : String) {
        
        guard let aChatVC = self.storyboard?.instantiateViewController(withIdentifier: "ChatVC") as? ChatVC else { return }
        
        aChatVC.senderID = senderId
        aChatVC.senderName = aUser
        aChatVC.conversationId = conversationId
        aChatVC.isUserSubscribed = isUserSubscribe
        
        self.navigationController?.pushViewController(aChatVC, animated: true)
        
        (parent?.parent as? UINavigationController)?.pushViewController(aChatVC, animated: true)
        
    }
}

class ChatListVM: NSObject {
    
    var arrChatList: [ModelChat] = []
    
    var didSelectChat: ((ModelChat) -> Void)?
    
    var subscriptionMsg = ""
    var isUserSubscribed = false
    
    override init() {
        super.init()
        
//        arrChatList = [
//            ["imgUser": "welcome banner img-1", "userName": "Diana Grace", "messageCount" : 11, "lastMessage": "Hi, Jessica"],
//            ["imgUser": "welcome banner img-2", "userName": "Jessica Mario", "messageCount" : 5, "lastMessage": "Hi, Mario"],
//            ["imgUser": "welcome banner img-3", "userName": "Diana Grace", "messageCount" : 0, "lastMessage": "Hi, Trump"],
//            ["imgUser": "welcome banner img-1", "userName": "Diana Grace", "messageCount" : 8, "lastMessage": "Hi, Obama"]
//        ].map( ChatListModel.init )
    }
}

extension ChatListVM: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        arrChatList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aCell = tableView.dequeueReusableCell(withIdentifier: "ChatlistCell") as! ChatlistCell
        
        let aModelChat = arrChatList[indexPath.row]
        
        aCell.lblUserName.text = aModelChat.senderName
        aCell.lblLastMessage.text = aModelChat.lastMsg
        aCell.lblMassageCount.text = "\(aModelChat.sender_unread)"
        
        aCell.userOnlineStatus.backgroundColor = aModelChat.onlineStatus == "1" ? UIColor.green : UIColor.lightGray
        
        let aImg = aModelChat.recipientProfileImage ?? ""
        
        if aImg != ""
        {
        KingfisherManager.shared.retrieveImage(with: URL(string:aImg)!) { result in
            let image = try? result.get().image
            if let image = image {
                aCell.imgUser.image = image
            }
        }
        }
        
        if aModelChat.sender_unread == "0" || aModelChat.sender_unread == ""
        {
            aCell.lblMassageCount.isHidden = true
        }
        else{
            aCell.lblMassageCount.isHidden = false

        }
        
        if aModelChat.isHukumePlusUser == "0" || aModelChat.isHukumePlusUser == ""
        {
            aCell.imgVerified.isHidden = true
        }
        else{
            aCell.imgVerified.isHidden = false

        }
        
       // aCell.lblMassageCount.isHidden = aModelChat.sender_unread <= 0
        
        aCell.lblDate.text = aModelChat.lastConversationDate
        
        return aCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}

extension ChatListVM: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
//        if self.isUserSubscribed == false{
//            Common().showAlert(strMsg: self.subscriptionMsg, view: ChatListVC)
//        }
//       else
//        {
        
        didSelectChat?(arrChatList[indexPath.row])
       // }
    }
}

class ChatListModel {
    
    var userName = ""
    var lastMessage = ""
    var messageCount = 0
    var imgUser: UIImage?
    
    init(dic: [String: Any]) {
        
        imgUser =  UIImage(named: dic["imgUser"] as? String ?? "")
        userName =  dic["userName"] as? String ?? ""
        lastMessage =  dic["lastMessage"] as? String ?? ""
        messageCount =  dic["messageCount"] as? Int ?? 0
    }
}


class ChatlistCell: UITableViewCell {
    
    @IBOutlet weak var lblDate: UILabel!
    @IBOutlet weak var userOnlineStatus: UIImageView!
    
    @IBOutlet weak var lblMassageCount: UILabel!
    @IBOutlet weak var lblLastMessage: UILabel!
    
    @IBOutlet weak var lblUserName: UILabel!
    @IBOutlet weak var imgUser: UIImageView!
    @IBOutlet weak var imgVerified: UIImageView!
    
    override func awakeFromNib() {
        
    }
}
