//
//  ChatVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 09/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import SwiftyJSON

class collectionMsgCell : MIBubbleCollectionViewCell
{

    @IBOutlet weak var viewBg: UIView!
    @IBOutlet weak var lblMsg: UILabel!
        
}

class ChatVC: MasterVC,UITextViewDelegate {
    
    @IBOutlet weak var constOfTextViewBottom: NSLayoutConstraint!
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var tblChat: UITableView!
    
    var chatVM = ChatVM()
    
    var senderName = "David Dhavan"

    var senderID = "0"

    
    var conversationId = "0"
    
    @IBOutlet weak var txtMsg: UITextView!
    
    let ref = Database.database().reference(withPath: "Conversation")

    var arrchatData : [modelChatObject] = [modelChatObject]()
    
    var isFromRoot = false

    var isUserSubscribed = false

    @IBOutlet weak var viewStaticMsg: UIView!
    
    var window:UIWindow?
    var navController:UINavigationController?
    
    var arrMsgData = [String]()
    
    let kItemPadding = 15

    
    @IBOutlet weak var viewSubscribePopUp: UIView!
    @IBOutlet weak var collectionMsg: UICollectionView!
    
    
    @IBOutlet weak var viewInnerPopUp: INXView!
    override func viewDidLoad() {
        super.viewDidLoad()

        
        

        arrMsgData.append("Hi")
        arrMsgData.append("hello")
        arrMsgData.append("hi \(senderName)")
        arrMsgData.append("How are you?")
        arrMsgData.append("I'm doing well")
        arrMsgData.append("Cool")
        arrMsgData.append("I'm fine")
        arrMsgData.append("What's going on")
        arrMsgData.append("Can we know each other")

        isInChatVc = true
        initSetup()
        
        
        keyboardNotifications()
        
        self.readMessages()


    }
    
    
    func keyboardNotifications() {
          NotificationCenter.default.addObserver(self,
              selector: #selector(keyboardWillShow),
              name: UIResponder.keyboardWillShowNotification,
              object: nil)
          NotificationCenter.default.addObserver(self,
              selector: #selector(keyboardWillHide),
              name: UIResponder.keyboardWillHideNotification,
              object: nil)
      }
    
    @objc func keyboardWillShow(sender: NSNotification) {
        let info = sender.userInfo!
        let keyboardSize = (info[UIResponder.keyboardFrameEndUserInfoKey] as! NSValue).cgRectValue.height

       // tblChat.contentInset.bottom = keyboardSize - 50
        if UIDevice.current.userInterfaceIdiom == .phone && UIScreen.main.bounds.size.height >= 812.0
      //  if UIDevice().userInterfaceIdiom == .phone && UIScreen.main.nativeBounds.height == 2436
        {
            constOfTextViewBottom.constant = keyboardSize - 80
        }
        else{
        constOfTextViewBottom.constant = keyboardSize - 50
        }
        self.view.layoutIfNeeded()
        self.scrollToBottom()

    }

    @objc func keyboardWillHide(sender: NSNotification) {
        
       // tblChat.contentInset.bottom = 0
        constOfTextViewBottom.constant = 0
        self.view.layoutIfNeeded()

        self.scrollToBottom()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(true)
        
        isInChatVc = false
    }
    
    func initSetup() {
        
        viewSubscribePopUp.isHidden = true
        viewStaticMsg.isHidden = true
        tblChat.rowHeight = UITableView.automaticDimension
        tblChat.estimatedRowHeight = 100
        
        tblChat.dataSource = chatVM
        tblChat.delegate = chatVM
        tblChat.reloadData()
        
        txtMsg.delegate = self
        
        chatVM.didChangeData = tblChat.reloadData
        
        let ccref = Database.database().reference(withPath: "Conversation/\(conversationId)/messages")
        
//        chatVM.arrChat.removeAll()
        
        ccref.observe(.value, with: { (snapshot) in
            // Get user value
            
            self.chatVM.arrChat.removeAll()

            
            for child in snapshot.children {
                
               // let snapshot = child as? DataSnapshot
                
                let snapshot = child as? DataSnapshot

                
                
                let key = snapshot?.key
                
                if let msgDict = snapshot?.value as? [String:Any] {
                  // print(msgDict["senderName"] as! String)
                    print(msgDict)

                    let userItem = modelChatObject(obj: JSON(msgDict))
                 
                    if userItem.message == ""
                    {
                        print("msg is blank")
                    }
                    else
                    {
                    self.chatVM.arrChat.append(userItem)
                    }
                    
                    print("------->",userItem.senderName)
                    
                }
                
                
               // let userItem = modelChatObject(obj: JSON(snapshot!.children))
             
              

            }
            
            if self.chatVM.arrChat.count == 0
            {
              
                let date = Int(NSDate().timeIntervalSince1970)

                let dayTimePeriodFormatter = DateFormatter()
                dayTimePeriodFormatter.dateFormat = "dd/MM/YY, hh:mm a"
                var dateString = dayTimePeriodFormatter.string(from: Date())
                
                let sessionItem = dbSessionStruct(kCurrentUser.user_id!, kCurrentUser.user_name!, "\(date)", "" ,dateString)
                
                
                
                let refAdd = Database.database().reference(withPath: "Conversation")
                
                let uid = refAdd.childByAutoId().key ?? "";
                
                let sessionItemRef = refAdd.child(self.conversationId).child("messages").child(uid)

                
                sessionItemRef.setValue(sessionItem.toAnyObject())

                
            }
            else{
                
                self.tblChat.reloadData()
                self.scrollToBottom()
                
            }

            
        }) { (error) in
            print(error.localizedDescription)
        }
        
       // isUserSubscribed = false
        
        if isUserSubscribed == false
        {
            
        viewSubscribePopUp.isHidden = false
            viewStaticMsg.isHidden = false
        let bubbleLayout = MICollectionViewBubbleLayout()
        bubbleLayout.minimumLineSpacing = 6.0
        bubbleLayout.minimumInteritemSpacing = 25.0
        bubbleLayout.delegate = self
        collectionMsg.setCollectionViewLayout(bubbleLayout, animated: false)
        
        
        collectionMsg.delegate = self
        collectionMsg.dataSource = self
        collectionMsg.reloadData()
            
            self.tblChat.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 120, right: 0)

            
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
//        lblTitle.text = "You're Chatting With \(senderName)"
        lblTitle.text = "You're hooked with \(senderName)"

    }
    
    func scrollToBottom(){
        DispatchQueue.main.async {
            
            if self.chatVM.arrChat.count > 0
            {
            
            let indexPath = IndexPath(row: self.chatVM.arrChat.count-1, section: 0)
            self.tblChat.scrollToRow(at: indexPath, at: .bottom, animated: true)
                
            }
        }
    }
    
    //MARK:- action methods
    
    
    @IBAction func actionSubscribeNow(_ sender: Any) {
        
        self.tabBarController?.selectedIndex = 3
        
    }
    
    @IBAction func actionReportUser(_ sender: Any) {
       openReportUserDialog()
    }
    
    @IBAction func actionBlockUser(_ sender: Any) {
        openBlockUserDialog()
    }
    @IBAction func actionOkay(_ sender: Any) {
        
        viewSubscribePopUp.isHidden = true

        
    }
    
    @IBAction func actionBack(_ sender: Any) {
        
        if isFromRoot == false
        {
            self.navigationController?.popViewController(animated: true)
            
        }
        else{
            
            let appdelegate = UIApplication.shared.delegate as! AppDelegate

            
            let aAuthenticationStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(identifier: "CustomTabbarVC")
            appdelegate.window!.rootViewController = aWelcomeNavigation
            navController = aWelcomeNavigation.navigationController
            appdelegate.window!.makeKeyAndVisible()
            
            
        }
        
        
    }
    
    
    
    
    struct dbSessionStruct {
       
        let ref: DatabaseReference?
        var senderId:String = ""
        var senderName:String = ""
        var timeStamp:String = ""
        var message:String = ""
        var datetime:String = ""
        
        init(_ senderId:String, _ senderName:String, _ timeStamp:String, _ message:String, _ senderDateTime:String) {
            
            self.ref = nil
            self.senderId = senderId
            self.senderName = senderName
            self.timeStamp = timeStamp
            self.message = message
            self.datetime = senderDateTime
          
        }
        
        func toAnyObject() -> Any {
            return [
                
                "senderId": senderId,
                "senderName": senderName,
                "timeStamp": timeStamp,
                "message": message,
                "datetime": datetime

            ]
        }
    }
    
    
    @IBAction func actionSendMsg(_ sender: Any) {
        
        let date = Int(NSDate().timeIntervalSince1970)

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/YY, hh:mm a"
        var dateString = dayTimePeriodFormatter.string(from: Date())

        
        let sessionItem = dbSessionStruct(kCurrentUser.user_id!, kCurrentUser.user_name!, "\(date)", txtMsg.text!, dateString)
        
        
        
        let refAdd = Database.database().reference(withPath: "Conversation")
        
        let uid = refAdd.childByAutoId().key ?? "";
        
        let sessionItemRef = refAdd.child(self.conversationId).child("messages").child(uid)

        
        sessionItemRef.setValue(sessionItem.toAnyObject())
        
        self.CallLastReadMsg(strMsg: txtMsg.text!)

        
        txtMsg.text = ""
        
    }
    
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        if textView.text == "Type your message here..."
        {
            textView.text = ""
        }
        
        
        return true
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        
        if textView.text == ""
        {
            textView.text = "Type your message here..."
        }
        
    }
    
}

class ChatVM: NSObject {
    
    var arrChat: [modelChatObject] = []
    
    var didChangeData: (() -> Void)?
    
    override init() {
        super.init()
        
       // initSetup()
    }
    
   
//
//    func initSetup() {
//
//        arrChat = [
//            ["imgUser": "welcome banner img-1", "userId": "1234", "message" : "Hi, Jack\nHow are you?", "date": "1:30"],
//            ["imgUser": "welcome banner img-2", "userId": "2323", "message" : "I'm fine, Thank you.", "date": "1:45"],
//            ["imgUser": "welcome banner img-3", "userId": "1234", "message" : "The relevant section of Cicero as printed in the source is reproduced below with fragments used in Lorem ipsum underlined. Letters in brackets were added to Lorem ipsum and were not present in the source text:", "date": "2:10"],
//            ["imgUser": "welcome banner img-1", "userId": "2323", "message" : "consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? [33] At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat…", "date": "2:50"],
//            ["imgUser": "welcome banner img-1", "userId": "2323", "message" : "Hi, Jack\nHow are you?", "date": "1:30"],
//            ["imgUser": "welcome banner img-2", "userId": "1234", "message" : "I'm fine, Thank you.", "date": "1:45"],
//            ["imgUser": "welcome banner img-3", "userId": "2323", "message" : "The relevant section of Cicero as printed in the source is reproduced below with fragments used in Lorem ipsum underlined. Letters in brackets were added to Lorem ipsum and were not present in the source text:", "date": "2:10"],
//            ["imgUser": "welcome banner img-1", "userId": "1234", "message" : "consequatur, vel illum, qui dolorem eum fugiat, quo voluptas nulla pariatur? [33] At vero eos et accusamus et iusto odio dignissimos ducimus, qui blanditiis praesentium voluptatum deleniti atque corrupti, quos dolores et quas molestias excepturi sint, obcaecati cupiditate non provident, similique sunt in culpa, qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio, cumque nihil impedit, quo minus id, quod maxime placeat, facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet, ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat…", "date": "2:50"]
//        ].map( ChatModel.init )
//
//        didChangeData?()
//    }
}

extension ChatVM: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        arrChat.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let aModelChat = arrChat[indexPath.row]
        
       //  let aIdentifier = aModelChat.userId == UserSessionManager.shared.currentUser?.userId ? "ChatReceiverTblCell" : "ChatSenderTblCell"
        
        
        
        var aIdentifier = ""
        
        if aModelChat.senderId == kCurrentUser.user_id
        {
            aIdentifier = "ChatSenderTblCell"
        }
        else{
            aIdentifier = "ChatReceiverTblCell"

        }
        
        
        
        let aCell = tableView.dequeueReusableCell(withIdentifier: aIdentifier) as! ChatTblCell
        
        
        let strTime = aModelChat.timeStamp.prefix(10)
                       
       // objCellSender.lblTimeSenderSide.text = getDateFromTimeStamp(timeStamp: Double(strTime)!)
        
        aCell.lblTime.text = getDateFromTimeStamp(timeStamp: Double(strTime)!)
        aCell.lblMessage.text = aModelChat.message
     //   aCell.imgUser?.image = aModelChat.imgUser
        
        return aCell
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        tableView.dequeueReusableCell(withIdentifier: "ChatHeaderCell")
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        0
       // 70
    }
    
    
    func getDateFromTimeStamp(timeStamp : Double) -> String {
        
        let date = NSDate(timeIntervalSince1970: timeStamp)
        
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        // UnComment below to get only time
        //  dayTimePeriodFormatter.dateFormat = "hh:mm a"
        
        
        let isCheckToday = Calendar.current.isDateInToday(date as Date)
        
        if isCheckToday == true
        {
            dayTimePeriodFormatter.dateFormat = "hh:mm a"
            
            var dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            dateString = "Today \(dateString)"
            
            return dateString
        }
        else
        {
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            return dateString
        }
        
        
    }
}

extension ChatVM: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
    }
}

class ChatModel {
    
    var userId = ""
    var message = ""
    var date = ""
    var imgUser: UIImage?
    
    init(dic: [String: Any]) {
        
        imgUser =  UIImage(named: dic["imgUser"] as? String ?? "")
        message =  dic["message"] as? String ?? ""
        date =  dic["date"] as? String ?? ""
        userId =  dic["userId"] as? String ?? ""
    }
}

class ChatTblCell: UITableViewCell {
    
    @IBOutlet weak var lblMessage: UILabel!
    @IBOutlet weak var lblTime: UILabel!
    @IBOutlet weak var imgUser: UIImageView?
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        imgUser?.layer.borderWidth = 1
        imgUser?.layer.borderColor = Constant.Color.themeRed.cgColor
    }
}


//MARK:- API Call
extension ChatVC : UICollectionViewDelegate,UICollectionViewDataSource,MICollectionViewBubbleLayoutDelegate
{

    // MARK: -
    // MARK: - UICollectionView Delegate & Datasource
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.arrMsgData.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        let indentifier = "collectionMsgCell"

        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: indentifier, for: indexPath) as! collectionMsgCell
        
        cell.lblMsg.text = "\(arrMsgData[indexPath.row])"
        cell.lblMsg.textColor = .white
        
       // cell.lblMsg!.text = "hi"

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        
        let tmpMsg = self.arrMsgData[indexPath.row]
        
        let date = Int(NSDate().timeIntervalSince1970)

        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd/MM/YY, hh:mm a"
        var dateString = dayTimePeriodFormatter.string(from: Date())
        
        let sessionItem = dbSessionStruct(kCurrentUser.user_id!, kCurrentUser.user_name!, "\(date)", tmpMsg, dateString)
        
        let refAdd = Database.database().reference(withPath: "Conversation")
        
        let uid = refAdd.childByAutoId().key ?? "";
        
        let sessionItemRef = refAdd.child(self.conversationId).child("messages").child(uid)

        
        sessionItemRef.setValue(sessionItem.toAnyObject())
        txtMsg.text = ""
        
        
        self.CallLastReadMsg(strMsg: tmpMsg)
        self.arrMsgData.remove(at: indexPath.row)
        self.collectionMsg.reloadData()
        
        
    }
    
    // MARK: -
    // MARK: - MICollectionViewBubbleLayoutDelegate
    
    func collectionView(_ collectionView:UICollectionView, itemSizeAt indexPath:NSIndexPath) -> CGSize
    {
        let title = self.arrMsgData[indexPath.row] as NSString
        var size = title.size(withAttributes: [NSAttributedString.Key.font: UIFont(name: "HelveticaNeue-Bold", size: 15)!])
        size.width = CGFloat(ceilf(Float(size.width + CGFloat(kItemPadding * 2))))
        size.height = 34
        
        //...Checking if item width is greater than collection view width then set item width == collection view width.
        if size.width > collectionView.frame.size.width {
            size.width = collectionView.frame.size.width
        }
        
        return size;
    }
    
    func readMessages(){
        var param:[String:Any] = [String:Any]()
        

        
        param["conversation_id"] = conversationId
        param["sender_id"] = kCurrentUser.user_id
        param["recipient_id"] = senderID

        print("\n ======= \n read message api params \n ====== \n \(param)")

        

        Networking.performApiCall(.readMessages(param), false, self) { (res) in
            print(res)
          //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [[String:Any]]
                        {
                            
                        }
                    }else{
                       // Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    
    func CallLastReadMsg(strMsg:String){
        var param:[String:Any] = [String:Any]()
        
        let strTimeZone = TimeZone.current.identifier

        
        param["conversation_id"] = conversationId
        param["lastMsg"] = strMsg
        param["sender_id"] = kCurrentUser.user_id
        param["recipient_id"] = senderID
//        param["recipient_id"] = kCurrentUser.user_id
//        param["sender_id"] = senderID
        param["timezone"] = strTimeZone

        
        print("\n ======= \n last conversation api params \n  ======= \n \(param)")
        

        Networking.performApiCall(.lastConversation(param), false, self) { (res) in
            print(res)
          //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [[String:Any]]
                        {
                            
                        }
                    }else{
                       // Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    func CallBlockUserAPI(){
        let dictParam:[String:Any] = [
            "loginuser_id":kCurrentUser.user_id!,
            "session_token":kCurrentUser.session_token!,
            "user_id":senderID
        ]
        Networking.performApiCall(.blockUser(dictParam), true, self) { (response) in
            if let dictData = response.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.navigationController?.popViewController(animated: true)
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
            
        }
    }
    func openBlockUserDialog(){
        let dialog = CustomDialogForConfirmation.ShowCustomDialogForConfirmation(title: "", msg: "Are you sure you want to block this user?", titleBtnLeft: "No".uppercased(), titleBtnRight: "Yes".uppercased())
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {
            dialog.removeFromSuperview()
            self.CallBlockUserAPI()
        }
    }
    func openReportUserDialog(){
        let dialog = CustomDialogForConfirmation.ShowCustomDialogForConfirmation(title: "", msg: "Are you sure you want to report this user?", titleBtnLeft: "No".uppercased(), titleBtnRight: "Yes".uppercased())
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {
            dialog.removeFromSuperview()
            self.gotoReportUser()
        }
    }
    func gotoReportUser(){
        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "FlagVC") as! FlagVC
        vc.reportId = senderID
        self.navigationController?.pushViewController(vc, animated: true)
    }
}
