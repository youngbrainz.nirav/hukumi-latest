//
//  ExploreVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Koloda
import SwiftyJSON


protocol ProcessStatusDelegate:NSObjectProtocol{
func reloadData()
}

var strMatchCount = "0"
var strChatCount = "0"
var isPurchased = false

var isFromSignUp = false
var isUserSubscribe = false
var is_use_filter = "2"

var strServerPurchaseMsg = ""

var isFirstTimeApicall = false
var isFirstTimeApicall2 = false
var isReloadData = false

class ExploreVC: MasterVC,ProcessStatusDelegate {
    
    @IBOutlet weak var viewCard: KolodaView!
    
    var arrUsers: [ModelMatches] = [ModelMatches]()

    var tmpUserdata : [ModelMatches] = [ModelMatches]()
   
    @IBOutlet weak var viewLikeDislike: UIView!
    
    
    @IBOutlet weak var btnLike: UIButton!
    
    @IBOutlet weak var btnDislike: UIButton!
    @IBOutlet weak var btnHukumePlus: UIButton!
    @IBOutlet weak var imgHukumePlus: UIImageView!
    @IBOutlet weak var imgRenew: UIImageView!
    
    @IBOutlet weak var imgHukumePlusTitle: UIImageView!
    @IBOutlet weak var lblHukumePlusTitle: UILabel!
    
    @IBOutlet weak var imgDislike: UIImageView!
    @IBOutlet weak var horizontalConstButton: NSLayoutConstraint!
    
    
    @IBOutlet weak var viewPopUpNodata: UIView!
    
    @IBOutlet weak var viewInnerPopUp: INXView!
    
    
    @IBOutlet weak var viewPopUpTermsService: UIView!
    
    @IBOutlet weak var viewInnerPopUpTermsService: INXView!
    
    
    
    @IBOutlet weak var lblTermsService: UILabel!
    
    
    @IBOutlet weak var imgLike: UIImageView!
    
    
    
    @IBOutlet weak var viewUpdateProfilePopUp: UIView!
    @IBOutlet weak var viewPlusmember: UIView!
    @IBOutlet weak var btnClose: UIButton!
    @IBOutlet weak var btnClose2: UIButton!
    @IBOutlet weak var viewconfirmUpgrademember: UIView!
    @IBOutlet weak var lblUpgradeTextTitle: UILabel!
    @IBOutlet weak var viewPlanExpire: UIView!
    @IBOutlet weak var lblExpireTitle: UILabel!
    
    
    @IBOutlet weak var viewInnerUpdateProfilePopUp: INXView!
    
    
    
    //MARK:- viewDidLoad
    override func viewDidLoad() {
        super.viewDidLoad()
        
       // isFromSignUp = true
        btnClose.setTitle("", for: .normal)
        btnClose2.setTitle("", for: .normal)
        self.btnHukumePlus.isHidden =  true
        self.imgHukumePlus.isHidden =  true
        self.imgRenew.isHidden =  true
        self.imgHukumePlusTitle.isHidden =  true
        self.lblHukumePlusTitle.isHidden =  true
        self.viewPlanExpire.isHidden =  true
        self.lblExpireTitle.isHidden =  true
        self.lblUpgradeTextTitle.text = "Steps: 1. You will be required to get verified through our Security and Artificial Intelligence system. \n \n2. If verification is successful, you will have to go through our upgrade process to get Hukume Plus setup for you. \n \nBenefits: 1. With Hukume Plus our system matches and get you hooked automatically. \n \n2. We provide you with profiles which have been verified through our Artificial Intelligence system. \n \n3. Hukume Plus provides you with more Privacy."
        //self.tabBarController?.tabBar.delegate = self
        
        
        
        
        var isAgreeTermsCond = ""
        
        let defaults = UserDefaults.standard

        if let strToken = defaults.string(forKey: "agreeTermsCond") {
            isAgreeTermsCond = strToken
        } else {
            print("not found")
        }
        
        if isFromSignUp == true || isAgreeTermsCond == "0"
        {
//            self.showSearchFilter()
            
            isFromSignUp = true
            self.viewPopUpTermsService.isHidden = false
            self.viewPopUpTermsService.bringSubviewToFront(self.view)
            self.tabBarController?.tabBar.isHidden = true
            
            
            UserDefaults.standard.set("0", forKey: "agreeTermsCond")
            UserDefaults.standard.synchronize()
            
//            isFromSignUp = false
        }
        else
        {
            self.viewPopUpTermsService.isHidden = true
        }
        
        isFirstTimeApicall = true
        isFirstTimeApicall2 = true
        
        self.viewPopUpNodata.isHidden = true
        imgDislike.image = imgDislike.image?.imageWithColor(color1: UIColor.red)
        imgLike.image = imgLike.image?.imageWithColor(color1: UIColor(red: 43.0/255.0, green: 189.0/255.0, blue: 172.0/255.0, alpha: 1.0))

        
        btnDislike.layer.borderWidth = 2
        btnDislike.layer.borderColor = UIColor.red.cgColor
        btnDislike.layer.masksToBounds = true
        
        btnLike.layer.borderWidth = 2
        btnLike.layer.borderColor = UIColor(red: 43.0/255.0, green: 189.0/255.0, blue: 172.0/255.0, alpha: 1.0).cgColor
        btnLike.layer.masksToBounds = true
        
        btnHukumePlus.layer.borderWidth = 2
        btnHukumePlus.layer.borderColor = UIColor(red: 127.0/255.0, green: 244.0/255.0, blue: 90.0/255.0, alpha: 1.0).cgColor
        btnHukumePlus.layer.masksToBounds = true
        
        btnLike.layer.cornerRadius = btnLike.frame.size.width/2
        btnDislike.layer.cornerRadius = btnDislike.frame.size.width/2
        btnHukumePlus.layer.cornerRadius = btnHukumePlus.frame.size.width/2
        

        
       
        
        initSetup()
        
        
       
        
        
        
        isPurchased = IAPManager.shared.isPurchased
        if isPurchased
        {
            
            print("purchased")
            
        }
        else{
            
            print("NO purchased")
            isUserSubscribe = false

        }
        
        self.CallUpdateSubscription()

        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("updateTabCounterinExplore"), object: nil)
        
        NotificationCenter.default.addObserver(self,
            selector: #selector(updateChatBadgeC0unter),
            name: NSNotification.Name("updateTabCounterinExplore"),
            object: nil)
        
        
        NotificationCenter.default.removeObserver(self, name: NSNotification.Name("reloadData"), object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadDatafromTab), name: Notification.Name("reloadData"), object: nil)

        self.viewUpdateProfilePopUp.isHidden = true

        
    }
    
    
    @objc func updateChatBadgeC0unter()
    {
        if let tabItems = self.tabBarController?.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            
            let tabItemChat = tabItems[2]
            
            if strChatCount == "0"
            {
                tabItemChat.badgeValue = nil
            }
            else{
                tabItemChat.badgeValue = strChatCount
            }
            
            
            let tabItemMatches = tabItems[1]
            
            if strMatchCount == "0"
            {
                tabItemMatches.badgeValue = nil
            }
            else{
                tabItemMatches.badgeValue = strMatchCount
            }
            
        }
        
    }
    
    @IBAction func actionOkPOpUp(_ sender: Any) {
        
        viewPopUpNodata.isHidden = true
    }
    
   
    func initSetup() {
        
//        let aArrData: [[String: Any]] = [
//            ["name": "Louise Brooks, 25", "location": "New York", "photos": ["welcome banner img-1", "welcome banner img-2", "welcome banner img-3", "welcome banner img-1", "welcome banner img-2", "welcome banner img-3"]],
//
//            ["name": "Katharine Hepburn , 19", "location": "New York", "photos": ["welcome banner img-2", "welcome banner img-2", "welcome banner img-3", "welcome banner img-1", "welcome banner img-2", "welcome banner img-3"]],
//
//            ["name": "Veronica Lake, 23", "location": "New York", "photos": ["welcome banner img-3", "welcome banner img-2", "welcome banner img-3", "welcome banner img-1", "welcome banner img-2", "welcome banner img-3"]],
//
//            ["name": "Ava Gardner, 21", "location": "New York", "photos": ["welcome banner img-1", "welcome banner img-2", "welcome banner img-3", "welcome banner img-1", "welcome banner img-2", "welcome banner img-3"]]
//        ]
        
//        arrUsers = aArrData.map( userCardModel.init )
        
        viewCard.countOfVisibleCards = 2
        viewCard.backgroundCardsScalePercent = 1
        viewCard.backgroundCardsTopMargin = 0
        
        viewCard.delegate = self
        viewCard.dataSource = self
        self.GetHomeScreenValues()
        self.CallCheckSubscription()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.viewPlusmember.isHidden = true
        self.viewconfirmUpgrademember.isHidden = true
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        userOnlineStatus = "1"
        appdelegate.CallUpdateUserStatus()
        IAPManager.shared.retrivePricing()
        

        
    }
    
    func reloadData()
    {
        
        isReloadData = true
        self.GetHomeScreenValues()

    }
    
    @objc func reloadDatafromTab()
    {
        
        
        self.GetHomeScreenValues()

    }
   
    
    
    
    @IBAction func btnSearchAction() {
        
        showSearchFilter()
    }
    
    @IBAction func btnLikeAction() {
        
        if self.arrUsers.count > 0
        {
        
        self.CallAcceptReject(strID: self.arrUsers[viewCard.currentCardIndex]._id, status: "1")
        viewCard.swipe(.right)
        
        }
        
    }
    
    @IBAction func btnHukumePlusNewAction(_ sender: Any) {
        print("Vatsal click")
        if self.lblHukumePlusTitle.text == "Hukume"{
            self.viewconfirmUpgrademember.isHidden = false
        }else{
            let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
            vc.isFrom = "verificationVC"
            vc.hidesBottomBarWhenPushed =  true
            self.navigationController?.pushViewController(vc, animated: true)
        }
        
    }

    
    @IBAction func btnDislikeAction() {
        
        if self.arrUsers.count > 0
        {
        
        self.CallAcceptReject(strID: self.arrUsers[viewCard.currentCardIndex]._id, status: "2")
        viewCard.swipe(.left)
        }
    }
    
    func showSearchFilter() {
        
        guard let aSearchFilterVC = storyboard?.instantiateViewController(identifier: "SearchFilterVC") as? SearchFilterVC, let aTabBar = parent else { return }
        
        aSearchFilterVC.view.alpha = 0
        aSearchFilterVC.delegate = self
        let aSafeArea = aTabBar.view.safeAreaInsets
        
        aSearchFilterVC.view.frame = CGRect(x: 0, y: aSafeArea.top, width: aTabBar.view.bounds.width, height: aTabBar.view.bounds.height - (aSafeArea.top + aSafeArea.bottom))
        
        aTabBar.view.addSubview(aSearchFilterVC.view)
        
        UIView.animate(withDuration: 0.3, animations: {
            aSearchFilterVC.view.alpha = 1

        }) { (tr) in
            aTabBar.addChild(aSearchFilterVC)
        }
    }
    
    
    
    @IBAction func actionAgreeTermsService(_ sender: Any) {
        
        
        UserDefaults.standard.set("1", forKey: "agreeTermsCond")
        UserDefaults.standard.synchronize()
        
        self.tabBarController?.tabBar.isHidden = false
        self.viewPopUpTermsService.isHidden = true

        self.showSearchFilter()

        
    }
    
    
    @IBAction func actionTermsService(_ sender: Any) {
        
        guard let aWebViewVC = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "WebViewLoaderVC") as? WebViewLoaderVC else { return }
        aWebViewVC.pageTitle = "Terms of Services"
        self.navigationController?.pushViewController(aWebViewVC, animated: true)
        
        
    }
    
    
    @IBAction func actionOkUpdateProfile(_ sender: Any) {
        self.hideUserProfilePopUp()

        guard let aEditProfileVC = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC else { return }
        
        self.navigationController?.pushViewController(aEditProfileVC, animated: true)
        
    }
    
    @IBAction func actionNotNowUpdateProfile(_ sender: Any) {
        
        self.hideUserProfilePopUp()
    }
    
    @IBAction func btnCloseViewPlusAction(_ sender: Any) {
        
        self.viewPlusmember.isHidden = true
    }
    @IBAction func btnCloseViewMemberPlusAction(_ sender: Any) {
        
        self.viewconfirmUpgrademember.isHidden = true
        self.viewPlusmember.isHidden = true
    }
    
    @IBAction func btnUpgradeAction(_ sender: Any) {
        self.viewPlusmember.isHidden = true
        self.viewconfirmUpgrademember.isHidden = false
     
    }
    @IBAction func btnPaynowAction(_ sender: Any) {
        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "SubscriptionVC") as! SubscriptionVC
        vc.isFrom = "verificationVC"
        vc.hidesBottomBarWhenPushed =  true
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func btnIgnorAction(_ sender: Any) {
        self.viewPlanExpire.isHidden = true
        
    }
    
    @IBAction func btnFinalUpgradeAction(_ sender: Any) {
        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "VerificationProcessVC") as! VerificationProcessVC
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func showUserProfilePopUp()
    {
        self.viewUpdateProfilePopUp.isHidden = false
        self.viewInnerUpdateProfilePopUp.alpha = 0

        UIView.animate(withDuration: 0.5, animations: {
            self.viewInnerUpdateProfilePopUp.alpha = 1
        }) { (finished) in


        }
    }

    func hideUserProfilePopUp()
    {
        self.view.endEditing(true)
        
        UIView.animate(withDuration: 0.5, animations: {
            self.viewInnerUpdateProfilePopUp.alpha = 0
        }) { (finished) in

            self.viewUpdateProfilePopUp.isHidden = true

        }

    }
}

extension ExploreVC: KolodaViewDataSource {
    func koloda(_ koloda: KolodaView, viewForCardAt index: Int) -> UIView {
        
        let aCardView: HUCardView = .fromNib()
        
        let aModel = arrUsers[index]
        
        aCardView.model = aModel
        
        return aCardView
    }
    
    func kolodaNumberOfCards(_ koloda: KolodaView) -> Int {
        arrUsers.count
    }
    func koloda(_ koloda: KolodaView, didSelectCardAt index: Int) {
        let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "UserDetailsVC") as! UserDetailsVC
        vc.objUser = self.arrUsers[index]
        vc.homeDelegate =  self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    func koloda(_ koloda: KolodaView, didSwipeCardAt index: Int, in direction: SwipeResultDirection) {
        
        if direction == .left || direction == .topLeft || direction == .bottomLeft {
            self.CallAcceptReject(strID: self.arrUsers[index]._id, status: "2")

        } else if direction == .right || direction == .topRight || direction == .bottomRight {
            self.CallAcceptReject(strID: self.arrUsers[index]._id, status: "1")
        }
    }
    
   
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.stopApiCounter()
    }
}

extension ExploreVC: KolodaViewDelegate {
    
    func kolodaDidRunOutOfCards(_ koloda: KolodaView) {
        
        GetHomeScreenValues()
       // self.viewPopUpNodata.isHidden = false
        //self.viewLikeDislike.isHidden = true
    }
    
}
//MARK:- API Call

extension ExploreVC{
    func GetHomeScreenValues(){
        var param:[String:Any] = [String:Any]()
        var isHukumePlusUser = false
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["is_use_filter"] = is_use_filter

        print("\n ========= \n home screen userList api params \n ======= \n \(param)")

        
        Networking.performApiCall(.homeScreenUserList(param), !isFromSignUp, self) { (res) in
            print(res)
            if let dictData = res.result.value as? [String:Any]{
                
                if isFromSignUp == true
                {
                    isFromSignUp = false
                }
                
                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let str = dictData["isHukumePlusUser"] as? Int{
                            if str == 0{  //original - 0
                                self.btnHukumePlus.isHidden =  false
                                self.imgHukumePlus.isHidden =  false
                                self.horizontalConstButton.constant = -40
                                self.imgHukumePlusTitle.isHidden =  false
                                self.lblHukumePlusTitle.isHidden =  false
//                                self.viewPlusmember.isHidden = false
                                self.imgRenew.isHidden =  true
                                self.btnHukumePlus.layer.borderWidth = 2
                                isHukumePlusUser =  false
                            }else{
                                isHukumePlusUser =  true
                                self.btnHukumePlus.isHidden =  true
                                self.imgHukumePlus.isHidden =  true
                                self.imgHukumePlusTitle.isHidden =  true
                                self.lblHukumePlusTitle.isHidden =  true
                                self.horizontalConstButton.constant = 0
                                self.imgRenew.isHidden =  true
                                
//                                self.viewPlusmember.isHidden = true
                                
                            }
                        }
                        
                        if let str = dictData["isHukumePlanExpired"] as? Int{
                            if str == 2{  //original - 2
//                                self.imgHukumePlus.image = UIImage(named: "ic_Renew")
                                self.lblHukumePlusTitle.text = "Renew Plan"
                                self.imgRenew.isHidden =  false
                                self.imgHukumePlus.isHidden =  true
                                self.imgHukumePlusTitle.isHidden =  true
                                self.lblHukumePlusTitle.isHidden =  false
                                self.imgHukumePlus.isHidden =  false
                                self.btnHukumePlus.isHidden =  false
                                self.btnHukumePlus.layer.borderWidth = 0
                                self.btnHukumePlus.backgroundColor =  .clear
                                self.horizontalConstButton.constant = -40
                                if let value = UserDefaults.standard.value(forKey: "isExpiredPlan") as? String{
                                    print(value)
                                    if value ==  "1"{
                                        self.viewPlanExpire.isHidden =  true
                                        self.lblExpireTitle.isHidden =  true
                                    }else{
                                        self.viewPlanExpire.isHidden =  false
                                        self.lblExpireTitle.isHidden =  false
                                        self.lblExpireTitle.text = "Hello \(kCurrentUser.user_name ?? ""), we noticed you have a payment due which needs to be paid.We have downgraded your account to default access. Please make payment now to have full access to Hukume platform."
                                    }
                                }else{
                                    UserDefaults.standard.setValue("1", forKey: "isExpiredPlan")
                                    self.viewPlanExpire.isHidden =  false
                                    self.lblExpireTitle.isHidden =  false
                                    self.lblExpireTitle.text = "Hello \(kCurrentUser.user_name ?? ""), we noticed you have a payment due which needs to be paid.We have downgraded your account to default access. Please make payment now to have full access to Hukume platform."
                                }
                                
                                

                            }else{
                                UserDefaults.standard.setValue("0", forKey: "isExpiredPlan")
                                if !isHukumePlusUser {
                                    
                                
                                self.viewPlanExpire.isHidden =  true
                                self.lblExpireTitle.isHidden =  true
                                self.imgHukumePlus.image = UIImage(named: "ic_plus_member")
                                self.lblHukumePlusTitle.text = "Hukume"
                                self.imgHukumePlusTitle.isHidden =  false
                                self.lblHukumePlusTitle.isHidden =  false
                                self.imgHukumePlus.isHidden =  false
                                self.btnHukumePlus.isHidden =  false
                                self.horizontalConstButton.constant = -40
                                self.btnHukumePlus.layer.borderWidth = 2
                                self.btnHukumePlus.backgroundColor =  .white
                                }else{
                                    self.btnHukumePlus.isHidden =  true
                                    self.imgHukumePlus.isHidden =  true
                                    self.imgHukumePlusTitle.isHidden =  true
                                    self.lblHukumePlusTitle.isHidden =  true
                                    self.horizontalConstButton.constant = 0
                                    self.imgRenew.isHidden =  true
                                }
                                
                                
                            }
                        }
                        if let data = dictData["data"] as? [[String:Any]]{
                           
                            
                            if isReloadData == true
                            {
                                self.arrUsers.removeAll()
                                isReloadData = false
                            }
                            self.arrUsers.removeAll()
                            for obj in data{
                                let user = ModelMatches(obj: JSON(obj))
                                
                               // if user.is_report_user != 1  {
                                    self.arrUsers.append(user)
                               // }
//                                if user.is_report_user == 1 && user.is_chat_deleted == 0 {
//                                  //  self.CallDeleteChatHistory(id: user._id)
//                                }
                            }
                            self.tmpUserdata = self.arrUsers
                            
                            self.viewPopUpNodata.isHidden = true
                            self.viewLikeDislike.isHidden = false
                            
//                            self.tmpUserdata.removeAll()
//
//                            self.tmpUserdata.append(self.arrUsers[0])
//                            self.tmpUserdata.append(self.arrUsers[1])
//
//
//                             self.arrUsers.removeAll()
//
//                            self.arrUsers = self.tmpUserdata
                            
                            if self.arrUsers.count > 0
                            {
                                self.viewLikeDislike.isHidden = false
                            }
                            else{
                                self.viewLikeDislike.isHidden = true

                            }
                            
                            if let matchCOunt = dictData["matchCount"]
                            {
                                strMatchCount = "\(matchCOunt)"
                            }
                            
                            if let matchCOunt = dictData["unReadCount"]
                            {
                                strChatCount = "\(matchCOunt)"
                            }

                            self.viewCard.resetCurrentCardIndex()
                            self.viewCard.reloadData()
                            
                            
                            var isProfileComplete = ""
                            
                            if let profileComplete = dictData["isPofileComplete"]
                            {
                                isProfileComplete = "\(profileComplete)"
                            }
                            
                            var isHukumePlusUser = ""
                            if let profileComplete = dictData["isHukumePlusUser"]
                            {
                                isHukumePlusUser = "\(profileComplete)"
                            }
                            
                            
                            if isProfileComplete == "0"
                            {
                                if isFirstTimeApicall == true
                                {
                                isFirstTimeApicall = false
                                self.showUserProfilePopUp()
                                }
                            }
                            
                            if isHukumePlusUser == "1"
                            {
                                self.viewPlusmember.isHidden = true
                            }else{
                                if let value = UserDefaults.standard.value(forKey: "isNewLogin") as? String{
                                    print(value)
                                    if value == "1"{
                                        UserDefaults.standard.set("0", forKey: "isNewLogin")
                                        self.viewPlusmember.isHidden = false
                                    }else{
                                        self.viewPlusmember.isHidden = true

                                    }
                                }else{
                                    UserDefaults.standard.set("0", forKey: "isNewLogin")
                                }
                            }
                            
                            
                            self.removeSpinner()

                            
                            let appdelegate = UIApplication.shared.delegate as! AppDelegate

                            appdelegate.startApiCounter()
                            
                            
                            
                        }
                        
                    }
                    else
                    {
                      
                        //Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                        
                        self.arrUsers.removeAll()
                        self.viewCard.reloadData()
                        self.viewPopUpNodata.isHidden = false
                        self.viewLikeDislike.isHidden = true
                        self.removeSpinner()
                        
                    }
                }
                else
                {
                    self.arrUsers.removeAll()
                    self.viewCard.reloadData()
                    self.viewPopUpNodata.isHidden = false
                    self.viewLikeDislike.isHidden = true
                    self.removeSpinner()
                }
            }
            else{
                
                self.arrUsers.removeAll()
                self.viewCard.reloadData()
                self.viewPopUpNodata.isHidden = false
                self.viewLikeDislike.isHidden = true
                self.removeSpinner()
                
            }
        }
    }
    
    func CallDeleteChatHistory(id:String){
        // handling from backend side so not needed this api
        var dictParam:[String:Any] = [:]
        dictParam["loginuser_id"] = kCurrentUser.user_id
        dictParam["session_token"] = kCurrentUser.session_token
        dictParam["report_user_id"] = id
        Networking.performApiCall(.deleteChat(dictParam), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                
                
                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        print("chat deleted")
                        
                    }else{
                       
                      

                    }
                    
                  
                    
                }
            }
        }
    }
    
    func CallUpdateSubscription(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["price"] = ""
        param["product_id"] = ""
        param["is_expired"] = "1"

        
        print("\n ====== \n make subscription api params \n ======== \n \(param)")
        
        Networking.performApiCall(.makeSubscription(param), false, self) { (res) in
            print(res)
            //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                
                print("\n ===== \n make subscription api responce \n ======== \n \(dictData)")
                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                        }
                        
                        isUserSubscribe = true
                        
                        
                    }else{
                       
                        isUserSubscribe = false

                    }
                    
                    self.GetSubscriptionPLans()
                    
                }
            }
        }
    }
    
    
    
    func CallCheckSubscription() {
        
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        
        print("\n ========= \n check subscription api params \n ======= \n \(param)")
        
        
        Networking.performApiCall(.checkSubscription(param), false, self) { (res) in
            print(res)
            //  self.arrMatches.removeAll()
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        isUserSubscribe = true
                        
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                        }
                        self.removeSpinner()
                    }else{
                        
                        isUserSubscribe = false
                        self.removeSpinner()
                        
                        
                        // Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    func CallAcceptReject(strID:String,status:String){
        var param:[String:Any] = [String:Any]()
        
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["screen_type"] = "2"
        param["requested_id"] = strID
        param["requsted_status"] = status

        
        print("\n ========= \n match api params \n ======= \n \(param)")
        
        Networking.performApiCall(.changeRequstedMatchesRecord(param), false, self) { (res) in
            print("\n ======== \n match api responce \n ======= \n \(res)")
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
//                        self.tmpUserdata.removeAll(where: {$0._id == strID})
//                        if self.tmpUserdata.count == 0 {
//                            self.GetHomeScreenValues()
//                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    
    func GetSubscriptionPLans(){
        
        var param:[String:Any] = [String:Any]()

        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        
        print("\n ========= \n GetSubscriptionPLans api params \n ======= \n \(param)")

        
        Networking.performApiCall(.getSubscriptionPlan(param), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                                
                        print(dictData)
                      
                        strServerPurchaseMsg = ""

                        if let planArr = dictData["data"] as? [[String:Any]]
                        {
                        
                            
                            for i in 0..<planArr.count
                            {
                                
                                let dictData = planArr[i]
                                
                                let strIsPlanSubscribe = "\(dictData["isPlanSubscribed"]!)"

                                
                                let strPrice = "\(dictData["name"]!)"
                                
                                if strIsPlanSubscribe == "1"
                                {
                                    
                                    strServerPurchaseMsg = "Your \(strPrice) is currently active. Thank you for finding love with Hukume"
                                    isUserSubscribe = true
                                    break
                                }
                                
                            }
                        
                            if dictData["isSubscribe"] as! Int == 1
                            {
                               // strServerPurchaseMsg = "Hello, you're currently enjoying One (1) Month Free Access on Hukume. This free access will end July 31, 2021. Thank you for using Hukume to find love, date, and companionship."
                               // strServerPurchaseMsg = ""

                               // isUserSubscribe = true
                            }
                            else{
                               // strServerPurchaseMsg = ""
                            }
                            
                        }
                        
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    
    
    
}
