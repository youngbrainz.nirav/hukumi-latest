//
//  SearchFilterVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import RangeSeekSlider
import SwiftyJSON
import GooglePlaces
import GoogleMaps
import GooglePlacePicker

class SearchFilterVC: UIViewController,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate, RangeSeekSliderDelegate {
    
    @IBOutlet weak var sliderAge: RangeSeekSlider!
    @IBOutlet weak var sliderDistance: RangeSeekSlider!

    var objGetFilfer:ModelGetFilters = ModelGetFilters(obj: JSON(["":""]))

    @IBOutlet weak var btnOther: HURadioButton!
    
    @IBOutlet weak var btnFemale: HURadioButton!
    @IBOutlet weak var btnMale: HURadioButton!
    
    
    @IBOutlet weak var txtLocation: HUTextField!
    
    var delegate:ProcessStatusDelegate?

    override func viewDidLoad() {
        

        super.viewDidLoad()

        let aFont = Constant.Font.gilroy(size: 15, type: .bold) ?? .systemFont(ofSize: 15)
        
        sliderAge.minLabelFont = aFont
        sliderAge.maxLabelFont = aFont
        
        sliderDistance.minLabelFont = aFont
        sliderDistance.maxLabelFont = aFont
        
        self.txtLocation.delegate = self
        initSetup()

        CallFiletrs()

    }
    
    func initSetup() {
        txtLocation.addRightImage(#imageLiteral(resourceName: "gps"), width: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(animated)
        
    }
    
    @IBAction func btnCloseAction() {
        
        remove()
    }
    
    @IBAction func btnSearchAction() {
        
        objGetFilfer.filter_distance = "\(sliderAge.selectedMaxValue)"
        
        if objGetFilfer.filter_gender == ""
        {
            Common().showAlert(strMsg:"Please select gender", view: self)
        }
        else if objGetFilfer.filter_distance == "0" || objGetFilfer.filter_distance == "0.0"
        {
            Common().showAlert(strMsg:"Please select age", view: self)
        }
        else if objGetFilfer.filter_city == ""
        {
            Common().showAlert(strMsg:"Please select city", view: self)
        }
        else{
        CallSetFiletrs()
        }
            //remove()
    }
    
    func remove() {
        
        UIView.animate(withDuration: 0.3, animations: {
            self.view.alpha = 0

        }) { (tr) in
            self.view.removeFromSuperview()
            self.removeFromParent()
        }

    }
    
    func DisplayData()
    {
        let distance = (Float(objGetFilfer.filter_distance)!)
        
        

       // sliderAge.selectedMaxValue = 0
        sliderAge.delegate = self
         sliderAge.selectedMaxValue = CGFloat(distance)

       // sliderAge.selectedMaxValue = 20.0
        sliderAge.setNeedsLayout()
        sliderAge.layoutIfNeeded()
       // sliderAge.maxValue = CGFloat(distance)
        //sliderAge.maxValue = CGFloat(distance)
        
        if objGetFilfer.filter_gender == "1"{
             
            self.btnMale.isRadioSelected = true
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = false
            
        }else if objGetFilfer.filter_gender == "2"{
            
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = true

        }else if objGetFilfer.filter_gender == "3"{
            
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = true
            self.btnFemale.isRadioSelected = false
        }
        else if objGetFilfer.filter_gender == "" {
            
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = false
        }
        
        txtLocation.text = objGetFilfer.filter_city
        
    }
    
    
    @IBAction func btnGenderClick(_ sender: HURadioButton) {
        if sender == btnMale{
            self.objGetFilfer.filter_gender = "1"
            self.btnMale.isRadioSelected = true
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = false

        }else if sender == btnFemale{
            
            self.objGetFilfer.filter_gender = "2"

            
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = true

        }else if sender == btnOther{
            
            self.objGetFilfer.filter_gender = "3"

            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = true
            self.btnFemale.isRadioSelected = false

        }
    }
    
    //MARK: - GMSAutocompleteViewController Delegate -
    func tapLocation(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {

        self.objGetFilfer.filter_city = place.formattedAddress!

        self.objGetFilfer.filter_new_lattitude = "\(place.coordinate.latitude)"
        self.objGetFilfer.filter_new_longitude = "\(place.coordinate.longitude)"

        if place.addressComponents != nil {
            for component in place.addressComponents! {
                if component.type == "city" {
                print(component.name)
                    self.objGetFilfer.filter_city = component.name
                    self.objGetFilfer.filter_new_city = component.name
                }
                
                if component.type == "locality" {
                print(component.name)
                    self.objGetFilfer.filter_city = component.name
                    self.objGetFilfer.filter_new_city = component.name

                }
                
                if component.type == "administrative_area_level_1" {
                print(component.name)
                    self.objGetFilfer.filter_new_state = component.name
                }
                
                if component.type == "country" {
                print(component.name)
                    self.objGetFilfer.filter_new_country = component.name
                }
                
                
                
            }
        }
        
        
        print(place)
        txtLocation.text = place.formattedAddress!
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }
    
    // Turn the network activity indicator on and off again.
    func didRequestAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = true
    }
    
    func didUpdateAutocompletePredictions(_ viewController: GMSAutocompleteViewController) {
        UIApplication.shared.isNetworkActivityIndicatorVisible = false
    }
    
    //MARK:-
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        
        if textField == txtLocation {
            self.tapLocation()
            return false
        }
        return true
    }
    
}

extension SearchFilterVC
{
    
    func CallFiletrs(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token

        Networking.performApiCall(.getFilters(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [String:Any]{
                            
                            print(data)
                            
                            self.objGetFilfer = ModelGetFilters(obj: JSON(data))
                            self.DisplayData()
                            
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
 
    
    func CallSetFiletrs(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["filter_city"] = objGetFilfer.filter_city
        param["filter_gender"] = objGetFilfer.filter_gender
        param["filter_distance"] = objGetFilfer.filter_distance
        param["filter_new_city"] = objGetFilfer.filter_new_city
        param["filter_new_state"] = objGetFilfer.filter_new_state
        param["filter_new_country"] = objGetFilfer.filter_new_country
        param["filter_new_lattitude"] = objGetFilfer.filter_new_lattitude
        param["filter_new_longitude"] = objGetFilfer.filter_new_longitude

        print("\n set filter param \n ======== \n  \(param)")
        
        Networking.performApiCall(.setFilters(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1
                    {
                        
                        self.remove()
                        
                        is_use_filter = "1"
                        self.delegate?.reloadData()


                    }
                    else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
}
