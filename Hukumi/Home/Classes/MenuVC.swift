//
//  MenuVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class MenuVC: UIViewController {
    
    @IBOutlet weak var imgViewUser: UIImageView!
    @IBOutlet weak var lblUserName: UILabel!
    
    @IBOutlet weak var tblMenu: UITableView!
    
    var menuVM = MenuVM()

    override func viewDidLoad() {
        super.viewDidLoad()

        initSetup()
    }
    
    func initSetup() {
        
        tblMenu.delegate = menuVM
        tblMenu.dataSource = menuVM
        
        menuVM.didChangeSelection = tblMenu.reloadData
        
        imgViewUser.clipsToBounds = false
        imgViewUser.layer.masksToBounds = true
        
        imgViewUser.layer.cornerRadius = 10
        imgViewUser.layer.shadowColor = UIColor.black.cgColor
        imgViewUser.layer.shadowRadius = 3
        imgViewUser.layer.shadowOpacity = 1
        imgViewUser.layer.shadowOffset = .zero
    }
    
    @IBAction func btnLogoutAction() {
        
    //    UserSessionManager.shared.logout()
    }
    
    @IBAction func btnViewProfileAction() {
        
        guard let aEditProfileVC = self.storyboard?.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC else { return }
        
        (Constant.appDel?.window?.rootViewController as? UINavigationController)?.pushViewController(aEditProfileVC, animated: true)
    }
}

class MenuVM: NSObject {
    
    var arrMenuItems: [String] = []
    
    var selectedIndex = 0 {
        didSet {
            didChangeSelection?()
        }
    }
    
    var didChangeSelection: (() -> Void)?
    
    
    override init() {
        super.init()
        
        arrMenuItems = ["Explore", "Matches", "Chats", "Packages", "Settings"]
    }
    
}

extension MenuVM: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return arrMenuItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let aCell = tableView.dequeueReusableCell(withIdentifier: "MenuTblCell") as! MenuTblCell
        
        aCell.lblTitle.text = arrMenuItems[indexPath.row]
        
        let aIsSelected = selectedIndex == indexPath.row
        
        let aTextColor: UIColor = aIsSelected ? .white : .darkGray
        let aBackgroundColor: UIColor = aIsSelected ? Constant.Color.themeRed : .clear
        let aFontType: Constant.Font.type = aIsSelected ? .semiBold : .regular
        
        
        aCell.lblTitle.textColor = aTextColor
        aCell.viewBackground.backgroundColor = aBackgroundColor
        aCell.lblTitle.font = Constant.Font.gilroy(size: 15, type: aFontType)
        
        return aCell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

extension MenuVM: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedIndex = indexPath.row
        tableView.reloadData()
    }
}

class MenuTblCell: UITableViewCell {
    
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var viewBackground: UIView!
    
}
