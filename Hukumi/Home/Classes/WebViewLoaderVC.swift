//
//  WebViewLoaderVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

class WebViewLoaderVC: MasterVC {
    
    @IBOutlet weak var lblTitle: UILabel!
    
    var pageTitle = ""
    var webLink = ""

    @IBOutlet weak var lblDesc: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()

        self.GetPages()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        lblTitle.text = pageTitle
    }
    
    
    func GetPages(){
        
        Networking.performApiCall(.getPages(["":""]), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        if let ArrData = dictData["data"] as? [[String:Any]]{

                            if self.lblTitle.text == "Privacy Policy"
                            {
                                let filterdArr = ArrData.filter{(x) -> Bool in
                                    (x["sc_title"] as! String == "Privacy Policy")
                                                       }
                                
                                if filterdArr.count > 0
                                {
                                    if let dict = filterdArr[0] as? [String:Any]
                                    {
                                        
                                        if let strDesc = dict["sc_content"] as? String
                                        {
                                            
                                         //   self.lblDesc.text = strDesc
                                            
                                            self.lblDesc.attributedText = strDesc.htmlAttributedString()

                                            self.view.layoutIfNeeded()
                                            
//                                            guard let data = strDesc.data(using: String.Encoding.unicode) else { return }
//
//                                            try? self.lblDesc.attributedText =
//                                               NSAttributedString(data: data,
//                                                               options: [.documentType:NSAttributedString.DocumentType.html],
//                                                    documentAttributes: nil)
                                        }
                                        
                                       
                                        
                                    }
                                    
                                }
                                
                                print("\(filterdArr)")
                                
                            }
                            else
                            {
                                let filterdArr = ArrData.filter{(x) -> Bool in
                                    (x["sc_title"] as! String == "Hukume Terms of Use")
                                                       }
                                
                                if filterdArr.count > 0
                                {
                                    if let dict = filterdArr[0] as? [String:Any]
                                    {
                                        
                                        if let strDesc = dict["sc_content"] as? String
                                        {
                                            
                                           // self.lblDesc.text = strDesc
                                            
                                            self.lblDesc.attributedText = strDesc.htmlAttributedString()

                                            self.view.layoutIfNeeded()
                                            
//                                            guard let data = strDesc.data(using: String.Encoding.unicode) else { return }
//
//                                            try? self.lblDesc.attributedText =
//                                               NSAttributedString(data: data,
//                                                               options: [.documentType:NSAttributedString.DocumentType.html],
//                                                    documentAttributes: nil)
                                        }
                                        
                                       
                                        
                                    }
                                    
                                }
                                
                                print("\(filterdArr)")
                                
                                
                                
                            }
                            
                            
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
    
    
    
}

extension String {
    func htmlAttributedString() -> NSAttributedString? {
        guard let data = self.data(using: String.Encoding.utf16, allowLossyConversion: false) else { return nil }
        guard let html = try? NSMutableAttributedString(
            data: data,
            options: [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html],
            documentAttributes: nil) else { return nil }
        return html
    }
}
