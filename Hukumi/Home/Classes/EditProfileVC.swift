//
//  EditProfileVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 09/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import SwiftyJSON
import Kingfisher
import DatePickerDialog
import DropDown
import GooglePlaces
import GoogleMaps
import GooglePlacePicker

class EditProfileVC: MasterVC,UITextFieldDelegate,GMSAutocompleteViewControllerDelegate {
    let startDayDropDown = DropDown()

    var imagePicker = UIImagePickerController()

    @IBOutlet var cvUserImages: UICollectionView!
    @IBOutlet weak var cvPassion: HUPassionCollectionView!
    @IBOutlet var imgViewUserPrimary: UIImageView!
    @IBOutlet var imgVerified: UIImageView!
    @IBOutlet var lblVerified: UILabel!
    @IBOutlet var viewVerified: UIView!
    @IBOutlet var constCVPassionHeight: NSLayoutConstraint!
    
    @IBOutlet weak var txtEducation: UITextField!
    @IBOutlet weak var txtLocation: UITextField!
    
    @IBOutlet weak var lblName: HUTextFieldUnderLine!
    
    @IBOutlet weak var lblYear: HUTextFieldUnderLine!
    @IBOutlet weak var lblMonth: HUTextFieldUnderLine!
    @IBOutlet weak var lblDay: HUTextFieldUnderLine!
    
    @IBOutlet weak var btnMale: HURadioButton!
    
    @IBOutlet weak var lblCareer: HUTextFieldUnderLine!
    @IBOutlet weak var lblCity: HUTextFieldUnderLine!
    @IBOutlet weak var btnOther: HURadioButton!
    @IBOutlet weak var btnFemale: HURadioButton!
    @IBOutlet weak var btnCamera: UIButton!
    var arrImages: [UIImage] = []
    var arrNewImages: [UIImage] = []

    @IBOutlet weak var lblBriefIntroduction: HUTextFieldUnderLine!
    var objProfile:ModelProfile = ModelProfile(obj: JSON(["":""]))
    
    var isCollectionHeightSet = false
    var ArrEducation:[Model_EducationList] = [Model_EducationList(obj: JSON(["":""]))]
    var removeImageId = "0"
    
    var isFromAddImages = false
    var isfromSearchCity = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imagePicker.delegate = self

        self.CallGetProfile()
       // arrImages = ["welcome banner img-1", "welcome banner img-2", "welcome banner img-3", "welcome banner img-1", "welcome banner img-2", "welcome banner img-3"].compactMap( { UIImage.init(named: $0) } )
       
        cvUserImages.dataSource = self
        cvUserImages.delegate = self
        
        
        txtEducation.delegate = self
        txtLocation.delegate = self
        lblCity.delegate = self
       // cvPassion.loadPassion([Model_Passion]())
        
        txtLocation.addRightImage(#imageLiteral(resourceName: "gps"), width: 20)
        txtEducation.addRightImage(#imageLiteral(resourceName: "dropdown red"), width: 20)
        
        lblCity.isUserInteractionEnabled = false
    }
    
    func OpenDatePicker(){
        DatePickerDialog().show("DatePicker", doneButtonTitle: "Done", cancelButtonTitle: "Cancel", datePickerMode: .date) { date in
            if let dt = date {
                let formatter = DateFormatter()
                formatter.dateFormat = "dd-MM-yyyy"
                let Arrdate = formatter.string(from: dt)
                self.objProfile.DOB = Arrdate
                var dates = Arrdate.split{$0 == "-"}.map(String.init)
                self.lblDay.text = dates[0]
                self.lblMonth.text = dates[1]
                self.lblYear.text = dates[2]
            }
        }
    }
    //MARK:- Button Click
    
    @IBAction func btnEditImageClick(_ sender: Any) {
        isFromAddImages = false
        self.OpenAlertForImagePicker()
    }
    
    @IBAction func btnGenderClick(_ sender: HURadioButton) {
        if sender == btnMale{
            self.objProfile.gender = "1"
            self.btnMale.isRadioSelected = true
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = false

        }else if sender == btnFemale{
            
            self.objProfile.gender = "2"

            
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = true

        }else if sender == btnOther{
            
            self.objProfile.gender = "3"

            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = true
            self.btnFemale.isRadioSelected = false

        }
    }
    @IBAction func btnDateClick(_ sender: Any) {
        self.OpenDatePicker()
    }
    @IBAction func btnUpdateProfileClick(_ sender: Any) {
        
        
       // if objProfile.images.count < 1
      
//        if self.arrImages.count < 1
//
//       {
//           Common().showAlert(strMsg: "Please select atleast 1 image.", view: self)
//
//       }
     //   else
        if cvPassion.selectedPassions.count > 2{
            let passion = cvPassion.arrSelectedPassions.map{$0._id}
            let characterArray = passion.flatMap { $0 }
            let stringArray2 = characterArray.map { String($0) }
            let string = stringArray2.joined(separator: ", ")
            self.objProfile.passions = string
            self.CallUpdateProfile()
        }
        else{
            Common().showAlert(strMsg: "Please select atleast 3 passion.", view: self)
        }
        
//        self.CallUpdateProfile()
        
    }
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        guard !isCollectionHeightSet else { return }
        
        isCollectionHeightSet = true
        
        cvPassion.layoutIfNeeded()
        view.layoutIfNeeded()
    }
    func DisplayData(){
        var dates = self.objProfile.DOB.split{$0 == "-"}.map(String.init)
        
        self.lblDay.text = dates[0]
        self.lblMonth.text = dates[1]
        self.lblYear.text = dates[2]
        
        self.lblName.text = self.objProfile.user_name
        self.lblCity.text = self.objProfile.city
        self.lblCareer.text = self.objProfile.career_experiance
        self.txtLocation.text = self.objProfile.user_address
        
        self.lblBriefIntroduction.text = self.objProfile.career_experiance
        if objProfile.gender == "1"{
            self.btnMale.isRadioSelected = true
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = false
        }else if objProfile.gender == "2"{
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = false
            self.btnFemale.isRadioSelected = true

        }else if objProfile.gender == "3"{
            self.btnMale.isRadioSelected = false
            self.btnOther.isRadioSelected = true
            self.btnFemale.isRadioSelected = false
        }
        
            if objProfile.isHukumePlusUser == "1"{
                self.lblVerified.isHidden =  false
                self.imgVerified.isHidden =  false
                self.viewVerified.isHidden = false
                self.btnCamera.isHidden =  true
            }else{
                self.lblVerified.isHidden =  true
                self.imgVerified.isHidden =  true
                self.viewVerified.isHidden = true
                self.btnCamera.isHidden =  false
                
            }
        
        let primaryImage = self.objProfile.profile_image
        if primaryImage != ""{
            KingfisherManager.shared.retrieveImage(with: URL(string:primaryImage)!) { result in
                let image = try? result.get().image
                if let image = image {
                    self.imgViewUserPrimary.image = image
                }
            }
        }
        
        self.arrImages.removeAll()
        
        let group = DispatchGroup()
        group.enter()
        DispatchQueue.global(qos: .userInitiated).async { [self] in
            // Do work asyncly and call group.leave() after you are done
            
            for i in 0..<self.objProfile.images.count
            {
                
                let aImage = objProfile.images[i]
                
                
                KingfisherManager.shared.retrieveImage(with: URL(string:aImage["image"]!)!) { result in
                    let image = try? result.get().image
                    if let image = image {
                       // aCell.imgViewUser.image = image
                        self.arrImages.append(image)
                        
                        if i == objProfile.images.count - 1
                        {
                            group.leave()

                        }
                        
                    }
                }
            }
        }
        
        group.notify(queue: .main, execute: {
            // This will be called when block ends
            
            
            self.cvUserImages.reloadData()

        })
        
//        self.cvUserImages.reloadData()

    }
    func SetupDropdown(){
        startDayDropDown.dismissMode = .onTap
        //self.btnBG.isHidden = false
        startDayDropDown.dataSource = ArrEducation.map{$0.name}
        
        startDayDropDown.width = self.txtEducation.frame.size.width
        startDayDropDown.anchorView = self.txtEducation
        startDayDropDown.selectionAction = {
            (index: Int, item: String) in
            
            self.objProfile.education_level = self.ArrEducation[index]._id
            self.txtEducation.text = item
        }
        
        startDayDropDown.show()
       // startDayDropDown.bringSubviewToFront(self.btnBG)

    }
    
}


extension EditProfileVC: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
      //  guard indexPath.row < objProfile.images.count else {

        guard indexPath.row < self.arrImages.count else {

            let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserAddImagesTblCell", for: indexPath)

            return aCell
        }
        
        let aCell = collectionView.dequeueReusableCell(withReuseIdentifier: "UserImagesTblCell", for: indexPath) as! UserImagesTblCell
        
        
//        if indexPath.row == 0
//        {
//            self.arrImages.removeAll()
//        }
        
//        let aImage = objProfile.images[indexPath.row]
//
//        KingfisherManager.shared.retrieveImage(with: URL(string:aImage)!) { result in
//            let image = try? result.get().image
//            if let image = image {
//                aCell.imgViewUser.image = image
//                self.arrImages.append(image)
//
//            }
//        }

        aCell.imgViewUser.image = arrImages[indexPath.row]
        
        aCell.didSelectOption = { [weak self] in
            
            print("image picker")
            
//            if self!.objProfile.images.count > 1
//            {
                
                self?.CallRemoveImage(index: indexPath.row)
                
                // old code
//                self!.objProfile.images.remove(at: indexPath.row)
//                self?.arrImages.remove(at: indexPath.row)
//                self!.cvUserImages.reloadData()
//            }
//            else
//            {
//
//
//            }

        }
        
        return aCell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
       // objProfile.images.count + 1
        
        self.arrImages.count + 1
        
    }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard indexPath.row < objProfile.images.count else {
            
            isFromAddImages = true
            self.OpenAlertForImagePicker()
            return
        }
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if textField == txtLocation{
            isfromSearchCity = false
            self.tapLocation()
            return false
        }
        else if textField == lblCity{
            isfromSearchCity = true
            self.tapLocation()
            return false
        }
        else if textField == txtEducation{
            self.SetupDropdown()
            return false
        }
        
        return true
    }
    func tapLocation(){
        let autocompleteController = GMSAutocompleteViewController()
        autocompleteController.delegate = self
        present(autocompleteController, animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
        
        if isfromSearchCity == false
        {
        
        self.objProfile.lattitude = "\(place.coordinate.latitude)"
        self.objProfile.longitude = "\(place.coordinate.longitude)"
        self.objProfile.user_address = place.formattedAddress!
        
        print(place)
        txtLocation.text = place.formattedAddress!
        }
        else
        {
            lblCity.text = place.formattedAddress!
        }
        
        if place.addressComponents != nil {
            for component in place.addressComponents! {
                if component.type == "city" {
                print(component.name)
                    lblCity.text = component.name
                }
               
                if component.type == "locality" {
                print(component.name)
                    lblCity.text = component.name
                }
                
                if component.type == "administrative_area_level_1" {
                print(component.name)
                    self.objProfile.state = component.name
                }
                
                if component.type == "country" {
                print(component.name)
                    self.objProfile.country = component.name
                }
                
                
            }
        }
        
        dismiss(animated: true, completion: nil)
    }
    
    func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
        // TODO: handle the error.
        print("Error: ", error.localizedDescription)
    }
    
    // User canceled the operation.
    func wasCancelled(_ viewController: GMSAutocompleteViewController) {
        dismiss(animated: true, completion: nil)
    }

}

extension EditProfileVC: UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        return CGSize(width: 90, height: 110)
    }
}

class UserImagesTblCell: UICollectionViewCell {
    
    @IBOutlet var imgViewUser: UIImageView!
    
    var didSelectOption: (() -> Void)?
    
    @IBAction func btnOptionAction() {
        didSelectOption?()
    }
}

//MARK:- API Call
extension EditProfileVC{
    func CallGetProfile(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token

        Networking.performApiCall(.getUserProfile(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        print("\n ======= \n get profile data \n ======= \n \(dictData)")
                        
                        if let data = dictData["data"] as? [String:Any]{
                            
                            self.objProfile = ModelProfile(obj: JSON(data))
                            self.DisplayData()
                            self.GetPassionList()
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    

    
    func CallRemoveImage(index : Int){
        var param:[String:Any] = [String:Any]()
      
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        
        let dict = self.objProfile.images[index]
        
        param["image_id"] = dict["id"]

        Networking.performApiCall(.removeImage(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                       
//                        if self.objProfile.images.count > 1
//                        {
                            self.objProfile.images.remove(at: index)
                            self.arrImages.remove(at: index)
                            self.cvUserImages.reloadData()
//                        }
//                        else
//                        {
//
//
//                        }

                        
                        
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
    func GetPassionList(){
        
        Networking.performApiCall(.getPassionList(["":""]), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                
                print("\n ========= \n get passion api responce \n ======= \n \(dictData)")

                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.GetEducationList()
                        self.cvPassion.arrPassions.removeAll()
                        if let ArrData = dictData["data"] as? [[String:Any]]{
                            for obj in ArrData{
                                let str = self.objProfile.passions
                                let arrStr = str.map{ String($0) }
                                for str in arrStr{
                                    if str == obj["_id"] as? String{
                                        self.cvPassion.arrSelectedPassions.append(Model_Passion(obj: JSON(obj)))
                                    }else{
                                        
                                    }
                                }
                                self.cvPassion.arrPassions.append(Model_Passion(obj: JSON(obj)))
                            }
                            self.cvPassion.loadPassion(self.cvPassion.arrPassions)
                            
                            self.constCVPassionHeight.constant = CGFloat(Double((self.cvPassion.arrPassions.count * 50)) / 3)
                            
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
    func GetEducationList(){
        
        Networking.performApiCall(.getEducationList(["":""]), false, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                
                print("\n ========= \n get education api responce \n ======= \n \(dictData)")

                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.ArrEducation.removeAll()
                        if let ArrData = dictData["data"] as? [[String:Any]]{
                            for obj in ArrData{
                                if obj["_id"] as? String == self.objProfile.education_level{
                                    self.txtEducation.text = obj["name"] as? String ?? ""
                                    
                                }
                                self.ArrEducation.append(Model_EducationList(obj: JSON(obj)))
                            }
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                    }
                }
            }
        }
    }
    
}

//MARK:-image picker

extension EditProfileVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func OpenAlertForImagePicker(){
        let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
            self.openCamera()
        }))

        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
            self.openGallary()
        }))

        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: {_ in
            
            self.isFromAddImages = false
        }))

        self.present(alert, animated: true, completion: nil)

    }
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerController.SourceType.camera))
        {
            imagePicker.sourceType = UIImagePickerController.SourceType.camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }

    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerController.SourceType.photoLibrary
        imagePicker.allowsEditing = true
        self.present(imagePicker, animated: true, completion: nil)
    }
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        
        if isFromAddImages == true{
            if let pickedImage = info[.editedImage] as? UIImage {
                
                let compressData = pickedImage.jpegData(compressionQuality: 0.5) //max value is 1.0 and minimum is 0.0
                let compressedImage = UIImage(data: compressData!)
                
  
                self.arrImages.append(compressedImage!)
                self.arrNewImages.append(compressedImage!)

//                self.arrImages.append(pickedImage)
                
                let dict = ["id":"0","image":""]
                
                self.objProfile.images.append(dict)
                
            }
            else if let pickedImage = info[.originalImage] as? UIImage {
                
                let compressData = pickedImage.jpegData(compressionQuality: 0.5) //max value is 1.0 and minimum is 0.0
                let compressedImage = UIImage(data: compressData!)
                
  
                self.arrImages.append(compressedImage!)
                self.arrNewImages.append(compressedImage!)

               // self.arrImages.append(pickedImage)
                
                let dict = ["id":"0","image":""]
                
                self.objProfile.images.append(dict)
            }
            
            self.cvUserImages.reloadData()
        }
        else{
            
            if let pickedImage = info[.editedImage] as? UIImage {
                
                let compressData = pickedImage.jpegData(compressionQuality: 0.4) //max value is 1.0 and minimum is 0.0
                let compressedImage = UIImage(data: compressData!)
                
                self.imgViewUserPrimary.image = compressedImage

                //self.imgViewUserPrimary.image = pickedImage
            }
            else if let pickedImage = info[.originalImage] as? UIImage {
                
                let compressData = pickedImage.jpegData(compressionQuality: 0.4) //max value is 1.0 and minimum is 0.0
                let compressedImage = UIImage(data: compressData!)
                
                self.imgViewUserPrimary.image = compressedImage
                
               // self.imgViewUserPrimary.image = pickedImage
            }
        }

        self.imagePicker.dismiss(animated: true, completion: nil)

        
    }
}


//MARK:- API Calling

extension EditProfileVC{

    func CallUpdateProfile(){
        var param:[String:Any] = [String:Any]()
        
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        param["mobile_no"] = self.objProfile.phone_number
        param["country_code"] = self.objProfile.country_code
        param["user_name"] = self.lblName.text
        param["email_id"] = self.objProfile.email_id
//        param["career_experiance"] = self.lblCareer.text
        param["career_experiance"] = self.lblBriefIntroduction.text

        param["DOB"] = self.objProfile.DOB
        param["gender"] = self.objProfile.gender
        param["city"] = self.lblCity.text
        param["education_level"] = self.objProfile.education_level
        param["passions"] = self.objProfile.passions
        param["user_address"] = self.txtLocation.text
        param["intro_message"] = self.lblBriefIntroduction.text

      //  param["access_type"] = "1"
      //  param["device_token"] = DELEGATE.strDeviceToken
       // param["device_type"] = "1"
        param["lattitude"] = self.objProfile.lattitude
      //  param["location"] = self.objProfile.user_address
        param["longitude"] = self.objProfile.longitude
      //  param["password"] = self.objProfile.password
        param["state"] = self.objProfile.state
        param["country"] = self.objProfile.country

        
       print("\n ========= \n update profile parama \n ====== \n \(param)")
        
//
        Networking.uploadImagesWithParams(.updateProfile(param), profile_image: self.imgViewUserPrimary.image ?? UIImage(), imageDic: self.arrNewImages , dictParams: param, showHud: true,onView:self) { (res) in

            switch res {
            case .success(let upload, _, _):

                upload.responseJSON { response in
                    if let dictData = response.result.value as? [String:Any]{
                        if let status = dictData["status"] as? Int {
                            if status == 1{
                                if let data = dictData["data"] as? [String:Any] {
                                    
                                    Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                                    self.navigationController?.popViewController(animated: true)
                                    
                                    kCurrentUser.update(info: JSON(data))
//                                    let vc = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "CustomTabbarVC") as! CustomTabbarVC
//                                    self.navigationController?.pushViewController(vc, animated: true)
                                }
                            }else{
                                Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                            }
                        }else{
                            Common().showAlert(strMsg: dictData["msg"] as? String ?? "", view: self)
                        }
                    }
                }
            case .failure(let encodingError):
                print("encodingError:\(encodingError)")
                Common().showAlert(strMsg: "\(encodingError)", view: self)
            }
            
            
        }
        
    }
}

extension UIImage {
    enum JPEGQuality: CGFloat {
        case lowest  = 0
        case low     = 0.25
        case medium  = 0.5
        case high    = 0.75
        case highest = 1
    }

    /// Returns the data for the specified image in JPEG format.
    /// If the image object’s underlying image data has been purged, calling this function forces that data to be reloaded into memory.
    /// - returns: A data object containing the JPEG data, or nil if there was a problem generating the data. This function may return nil if the image has no data or if the underlying CGImageRef contains data in an unsupported bitmap format.
    func jpeg(_ jpegQuality: JPEGQuality) -> Data? {
        return jpegData(compressionQuality: jpegQuality.rawValue)
    }
}

extension UIImage {
    func resizeWithPercent(percentage: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: size.width * percentage, height: size.height * percentage)))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
    func resizeWithWidth(width: CGFloat) -> UIImage? {
        let imageView = UIImageView(frame: CGRect(origin: .zero, size: CGSize(width: width, height: CGFloat(ceil(width/size.width * size.height)))))
        imageView.contentMode = .scaleAspectFit
        imageView.image = self
        UIGraphicsBeginImageContextWithOptions(imageView.bounds.size, false, scale)
        guard let context = UIGraphicsGetCurrentContext() else { return nil }
        imageView.layer.render(in: context)
        guard let result = UIGraphicsGetImageFromCurrentImageContext() else { return nil }
        UIGraphicsEndImageContext()
        return result
    }
}
