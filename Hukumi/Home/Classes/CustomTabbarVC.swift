//
//  CustomTabbarVC.swift
//  Hukumi
//
//  Created by hiren  mistry on 31/01/21.
//  Copyright © 2021 Hiren Mistry. All rights reserved.
//

import UIKit

class CustomTabbarVC: UITabBarController {

    var delegateMy:ProcessStatusDelegate?

    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
    }
    
    
    @objc func setBadge()
    {
        
        if let tabItems = self.tabBar.items {
            // In this case we want to modify the badge number of the third tab:
            
            let tabItemChat = tabItems[2]
            
            if strChatCount == "0"
            {
                tabItemChat.badgeValue = nil
            }
            else{
                tabItemChat.badgeValue = strChatCount
            }
            
            
            let tabItemMatches = tabItems[1]
            
            if strMatchCount == "0"
            {
                tabItemMatches.badgeValue = nil
            }
            else{
                tabItemMatches.badgeValue = strMatchCount
            }
            
        }
        
//        self.tabBar.items![1].badgeValue = strMatchCount
//
//        self.tabBar.items![2].badgeValue = strChatCount

    }

   
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
          // print(tabBar.items![1]) // The number is tab index
        
        if item.title == "Find Love"
        {
            //self.delegateMy?.reloadData()
            NotificationCenter.default.post(name: Notification.Name("reloadData"), object: nil)

        }
        
       }
    
}
