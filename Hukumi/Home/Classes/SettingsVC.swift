//
//  SettingsVC.swift
//  Hukumi
//
//  Created by Govind Prajapati on 08/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import Kingfisher
import UserNotifications
import PWSwitch
import SwiftyJSON
class SettingsVC: MasterVC {
    
    @IBOutlet weak var lblEmail: UILabel!
    @IBOutlet weak var viewUserDetailBackground: UIView!
    @IBOutlet weak var viewSettingBackground: UIView!

    @IBOutlet weak var lblPhoneNumber: UILabel!
    
    
    @IBOutlet weak var switchHideProfile: PWSwitch!
    @IBOutlet weak var imgUserProfile: UIImageView!
    @IBOutlet weak var lblUsername: UILabel!
    
    @IBOutlet weak var switchNotification: PWSwitch!
    
    
    @IBOutlet weak var lblHeaderTitle: UILabel!
    
    @IBOutlet weak var btnLogout: UIButton!
    @IBOutlet weak var btnRemoveSubscription: UIButton!
    @IBOutlet weak var heightForRemoveSubscription: NSLayoutConstraint!
    @IBOutlet weak var lblSep: UILabel!
    
    var objProfile:ModelProfile = ModelProfile(obj: JSON(["":""]))
    override func viewDidLoad() {
        super.viewDidLoad()
        self.btnRemoveSubscription.isHidden = true
        self.lblSep.isHidden = true
        self.heightForRemoveSubscription.constant = 0
        viewSettingBackground.layer.borderWidth = 1
        viewSettingBackground.layer.borderColor = UIColor.white.cgColor
        
        viewUserDetailBackground.layer.borderWidth = 1
        viewUserDetailBackground.layer.borderColor = UIColor.white.cgColor
        
        imgUserProfile.layer.cornerRadius = imgUserProfile.frame.size.height/2
        
        
        
        lblHeaderTitle.font = Constant.Font.gilroy(size: 20, type: .bold)
        btnLogout.titleLabel?.font = Constant.Font.gilroy(size: 20, type: .bold)
        
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.startApiCounter()
        CallGetProfile()
        self.DisplayData()
        

    }
    
    
  
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        appdelegate.stopApiCounter()
    }
    
    
    func DisplayData(){
        
        self.lblEmail.text = kCurrentUser.email_id
        self.lblUsername.text = kCurrentUser.user_name
        self.lblPhoneNumber.text = "\(kCurrentUser.country_code ?? "") \(kCurrentUser.phone_number ?? "")"
        
        
        let aImg = kCurrentUser.profile_image ?? ""
        
        if aImg != ""
        {
            KingfisherManager.shared.retrieveImage(with: URL(string:aImg)!) { result in
                    let image = try? result.get().image
                    if let image = image {
                        self.imgUserProfile.image = image
                    }
                }
        }
        

        let current = UNUserNotificationCenter.current()
                current.getNotificationSettings(completionHandler: { permission in
                    switch permission.authorizationStatus  {
                    case .authorized:
                        print("User granted permission for notification")
                        self.switchNotification.setOn(true, animated: true)
                    case .denied:
                        print("User denied notification permission")
                        self.switchNotification.setOn(false, animated: true)

                    case .notDetermined:
                        print("Notification permission haven't been asked yet")
                        self.switchNotification.setOn(false, animated: true)

                    case .provisional:
                        // @available(iOS 12.0, *)
                        print("The application is authorized to post non-interruptive user notifications.")
                    case .ephemeral:
                        // @available(iOS 14.0, *)
                        print("The application is temporarily authorized to post notifications. Only available to app clips.")
                    @unknown default:
                        print("Unknow Status")
                    }
                })
        
    }
    @IBAction func btnPrivacyPolicyAction() {
        
        pushPolicyVC()
    }
    
    @IBAction func btnTermsAndConditionsAction() {
        
        pushPolicyVC(isPushForPolicy: false)
    }
    
    @IBAction func btnLogoutAction() {
        
        kCurrentUser.logOut()
        
        var navController:UINavigationController?
        var window:UIWindow?

       // window = UIWindow(frame: UIScreen.main.bounds)
        let appdelegate = UIApplication.shared.delegate as! AppDelegate

        
        let aAuthenticationStoryboard = UIStoryboard(name: "Auth", bundle: nil)
        let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(identifier: "WelcomeNavigation")
        
        UserDefaults.standard.removeObject(forKey: "isExpiredPlan")
        appdelegate.window!.rootViewController = aWelcomeNavigation
        navController = aWelcomeNavigation.navigationController
        appdelegate.window!.makeKeyAndVisible()
        

        
       // UserSessionManager.shared.logout()
    }
    
    @IBAction func btnDeactivateAccountAction(_ sender: Any) {
       // CallDeactivateAccountAPI()
        openDeactivateAccountDialog()
    }
    
    @IBAction func btnRemoveSubscriptionAction(_ sender: Any) {
        openRemoveSubscriptionDialog()
    }
    
    @IBAction func btnViewProfileAction() {
        
        guard let aEditProfileVC = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "EditProfileVC") as? EditProfileVC else { return }
        
        self.navigationController?.pushViewController(aEditProfileVC, animated: true)
    }
    
    func pushPolicyVC(isPushForPolicy: Bool = true) {
        
        guard let aWebViewVC = Common.HomeStoryBoard.instantiateViewController(withIdentifier: "WebViewLoaderVC") as? WebViewLoaderVC else { return }
        aWebViewVC.pageTitle = isPushForPolicy ? "Privacy Policy" : "Terms of Services"
        self.navigationController?.pushViewController(aWebViewVC, animated: true)
    }
    
    func CallDeactivateAccountAPI(){
        let dictParam:[String:Any] = [
            "loginuser_id":kCurrentUser.user_id!,
            "session_token":kCurrentUser.session_token!
        ]
        Networking.performApiCall(.deactivateAccount(dictParam), true, self) { (response) in
            if let dictData = response.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        self.btnLogoutAction()
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
            
        }
    }
    func CallGetProfile(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token
        
        Networking.performApiCall(.getUserProfile(param), true, self) { (res) in
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                        print("\n ======= \n get profile data \n ======= \n \(dictData)")
                        
                        if let data = dictData["data"] as? [String:Any]{
                           
                            self.objProfile = ModelProfile(obj: JSON(data))
                            //self.DisplayData()
                            if self.objProfile.isHukumePlusUserAutoPlan == 1 {
                                self.btnRemoveSubscription.isHidden = false
                                self.lblSep.isHidden = false
                                self.heightForRemoveSubscription.constant = 58
                            } else {
                                self.btnRemoveSubscription.isHidden = true
                                self.lblSep.isHidden = true
                                self.heightForRemoveSubscription.constant = 0
                            }
                        }
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    func CallRemoveSubscriptionAPI(){
        let dictParam:[String:Any] = [
            "loginuser_id":kCurrentUser.user_id!,
            "session_token":kCurrentUser.session_token!
        ]
        Networking.performApiCall(.removeSubscription(dictParam), true, self) { (response) in
            if let dictData = response.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                        self.btnRemoveSubscription.isHidden = true
                        self.lblSep.isHidden = true
                        self.heightForRemoveSubscription.constant = 0
                    }else{
                        Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
            
        }
    }
    
    
    func openDeactivateAccountDialog(){
        let dialog = CustomDialogForConfirmation.ShowCustomDialogForConfirmation(title: "", msg: "If you deactivate or remove your account, all your records including payment will be removed and you will be required to sign up again if you like to use the app. Do you want to proceed .?", titleBtnLeft: "No".uppercased(), titleBtnRight: "Yes".uppercased())
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {
            dialog.removeFromSuperview()
            self.CallDeactivateAccountAPI()
        }
    }
    func openRemoveSubscriptionDialog(){
        let dialog = CustomDialogForConfirmation.ShowCustomDialogForConfirmation(title: "", msg: "If you cancel your subscription you will not have full access to the app. Do you want to proceed.?", titleBtnLeft: "No".uppercased(), titleBtnRight: "Yes".uppercased())
        dialog.onClickLeftButton = {
            dialog.removeFromSuperview()
        }
        dialog.onClickRightButton = {
            dialog.removeFromSuperview()
//            guard let url = URL.init(string: "https://apps.apple.com/account/subscriptions") else {return}
//            if  UIApplication.shared.canOpenURL(url) {
//                UIApplication.shared.open(url)
//            }
            self.CallRemoveSubscriptionAPI()
        }
    }
    
}
