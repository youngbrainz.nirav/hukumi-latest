//
//  AppDelegate.swift
//  Hukumi
//
//  Created by hiren  mistry on 01/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import AKSideMenu
import GooglePlaces
import GoogleMaps
import Firebase
import FirebaseMessaging

var DELEGATE : AppDelegate = UIApplication.shared.delegate as! AppDelegate
let googleAPIKey = "AIzaSyD3zlf76Nyjc2Hf_qWstNB_0wR3eAQzRnQ"

var isInChatVc = false
var appDel = UIApplication.shared.delegate as! AppDelegate
var userOnlineStatus = "1"
var timerCallCounterApi = Timer()


@UIApplicationMain



class AppDelegate: UIResponder, UIApplicationDelegate {

    var screenWidth : CGFloat = 0.0
    var screenHeight : CGFloat = 0.0
    var strDeviceToken = getDeviceTokenFromUserDefaults() ?? "1234"
    var window:UIWindow?
    var navController:UINavigationController?


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool
    {
        // Override point for customization after application launch.
        
       // if launchOptions == nil{
            
            UIApplication.shared.applicationIconBadgeNumber = 0

            
        //}
        
        GMSServices.provideAPIKey(googleAPIKey)
        GMSPlacesClient.provideAPIKey(googleAPIKey)
        
        FirebaseApp.configure()
        configureFirebase()
        registerForNotifications()
        
        self.window = UIWindow(frame: UIScreen.main.bounds)

        self.screenWidth = (self.window?.bounds.size.width ?? 375.0)
        self.screenHeight = (self.window?.bounds.size.height ?? 812.0)
       
        IAPManager.shared.retrivePricing()

        
        let aAuthenticationStoryboard = UIStoryboard(name: "Auth", bundle: nil)
        let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(identifier: "WelcomeNavigation")
        window?.rootViewController = aWelcomeNavigation
        navController = aWelcomeNavigation.navigationController
        window?.makeKeyAndVisible()

        if kCurrentUser.user_id == nil ||  kCurrentUser.user_id == ""{
            let aAuthenticationStoryboard = UIStoryboard(name: "Auth", bundle: nil)
            let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(identifier: "WelcomeNavigation")
            window?.rootViewController = aWelcomeNavigation
            navController = aWelcomeNavigation.navigationController
            window?.makeKeyAndVisible()

        }else{
            
            self.CallUpdateUserStatus()
            
            let aAuthenticationStoryboard = UIStoryboard(name: "Home", bundle: nil)
            let aWelcomeNavigation = aAuthenticationStoryboard.instantiateViewController(identifier: "CustomTabbarVC")
            window?.rootViewController = aWelcomeNavigation
            navController = aWelcomeNavigation.navigationController
            window?.makeKeyAndVisible()
        }
        return true
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        
        userOnlineStatus = "0"
        self.CallUpdateUserStatus()
        
    }
    
    
    func applicationWillEnterForeground(_ application: UIApplication) {
        userOnlineStatus = "1"
        self.CallUpdateUserStatus()
        
    }
    
    func applicationWillTerminate(_ application: UIApplication) {


        //call API Here

        userOnlineStatus = "0"
        self.CallUpdateUserStatus()
        sleep(5)
        print("applicationWillTerminate")
    }
    func CallUpdateUserStatus(){
        var param:[String:Any] = [String:Any]()
        param["user_id"] = kCurrentUser.user_id
        param["status"] = userOnlineStatus

        Networking.performApiCall(.updateOnlineStatus(param), false, (UIApplication.shared.keyWindow?.rootViewController)!) { (res) in
            print(res)
            if let dictData = res.result.value as? [String:Any]{
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        
                    }else{
                       // Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
   
    
    func startApiCounter()
    {
//        timerCallCounterApi.invalidate() // just in case this button is tapped multiple times
//
//               // start the timer
//        timerCallCounterApi = Timer.scheduledTimer(timeInterval: 10, target: self, selector: #selector(callgetCounterapi), userInfo: nil, repeats: true)
//        timerCallCounterApi.fire()
    }
    
    func stopApiCounter()
    {
        timerCallCounterApi.invalidate() // just in case this button is tapped multiple times

    }
    
    @objc func callgetCounterapi(){
        var param:[String:Any] = [String:Any]()
        param["loginuser_id"] = kCurrentUser.user_id
        param["session_token"] = kCurrentUser.session_token

        Networking.performApiCall(.homeScreenUserList(param), false, (UIApplication.shared.keyWindow?.rootViewController)!) { (res) in
            print(res)
            if let dictData = res.result.value as? [String:Any]{
                
                if isFromSignUp == true
                {
                    isFromSignUp = false
                }
                
                
                if let status = dictData["status"] as? Int {
                    if status == 1{
                        if let data = dictData["data"] as? [[String:Any]]{
                            
                            
                            
                            if let matchCOunt = dictData["matchCount"]
                            {
                                strMatchCount = "\(matchCOunt)"
                            }
                            
                            if let matchCOunt = dictData["unReadCount"]
                            {
                                strChatCount = "\(matchCOunt)"
                            }
                            
                            
                           
                           

                                NotificationCenter.default.post(name: Notification.Name("updateTabCounterinExplore"), object: nil)

                                
                          
                            
                            
                        }
                    }else{
                      //  Common().showAlert(strMsg: dictData["msg"] as? String ?? "something went wrong!", view: self)
                    }
                }
            }
        }
    }
    
}


extension UIApplication {
    
    class func topViewController(_ viewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = viewController as? UINavigationController {
            return topViewController(nav.visibleViewController)
        }
        if let tab = viewController as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(selected)
            }
        }
        if let presented = viewController?.presentedViewController {
            return topViewController(presented)
        }
        return viewController
    }
    
    
    
    
}
