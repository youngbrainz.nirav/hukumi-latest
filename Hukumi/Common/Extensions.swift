//
//  Extensions.swift
//  Hukumi
//
//  Created by Govind Prajapati on 07/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit

extension UIColor {
    convenience init(withRGB R: CGFloat, G: CGFloat, B: CGFloat, opacity: CGFloat = 1) {
        self.init(red: R/255.0, green: G/255.0, blue: B/255.0, alpha: opacity)
    }
    
    convenience init(hexString:String, alpha:CGFloat = 1.0) {
        var cString:String = hexString.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        var rgbValue:UInt32 = 10066329 //color #999999 if string has wrong format
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.count) == 6) {
            Scanner(string: cString).scanHexInt32(&rgbValue)
        }
        
        self.init(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: alpha
        )
    }
    
    func toHexString() -> String {
        var r:CGFloat = 0
        var g:CGFloat = 0
        var b:CGFloat = 0
        var a:CGFloat = 0
        
        getRed(&r, green: &g, blue: &b, alpha: &a)
        
        let rgb:Int = (Int)(r*255)<<16 | (Int)(g*255)<<8 | (Int)(b*255)<<0
        
        return String(format:"#%06x", rgb)
    }
    static let themeColor = UIColor.init(red: 233/255, green: 49/255, blue: 0/255, alpha: 1)
}
extension UIView{
    func setRound(withBorderColor:UIColor=UIColor.clear, andCornerRadious:CGFloat = 0.0, borderWidth:CGFloat = 1.0){
        if andCornerRadious==0.0 {
            var frame:CGRect = self.frame
            frame.size.height=min(self.frame.size.width, self.frame.size.height)
            frame.size.width=frame.size.height
            self.frame=frame
            self.layer.cornerRadius=self.layer.frame.size.width/2
        }else {
            self.layer.cornerRadius=andCornerRadious
        }
        self.layer.borderWidth = borderWidth
        self.clipsToBounds = true
        self.layer.borderColor = withBorderColor.cgColor
    }
}
