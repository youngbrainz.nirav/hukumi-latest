//
//  Constant.swift
//  Hukumi
//
//  Created by Govind Prajapati on 06/12/20.
//  Copyright © 2020 Hiren Mistry. All rights reserved.
//

import UIKit
import AKSideMenu

struct Constant {
    
    static let appDel = UIApplication.shared.delegate as? AppDelegate
    
    struct Font {
        
        static func gilroy(size: Float, type: type) -> UIFont? {
        
            UIFont(name: "Gilroy-\(type.rawValue)", size: CGFloat(size))
        }
        
        enum type: String {
            case black = "Black"
            case bold = "Bold"
            case thin = "Thin"
            case light = "Light"
            case regular = "Regular"
            case semiBold = "SemiBold"
        }
    }
    
    struct Color {
        static let themeRed = UIColor(hexString: "FE0000")
    }
}
