
//
//  Common.swift
//  EDR
//
//  Created by hiren  mistry on 08/10/20.
//  Copyright © 2020 hiren  mistry. All rights reserved.
//

extension String {

    func nsRange(from range: Range<String.Index>) -> NSRange {
        let from = range.lowerBound.samePosition(in: utf16)
        let to = range.upperBound.samePosition(in: utf16)
        return NSRange(location: utf16.distance(from: utf16.startIndex, to: from!),
                       length: utf16.distance(from: from!, to: to!))
    }
}

extension String {

    func toDate(withFormat format: String = "yyyy-MM-dd HH:mm:ss")-> Date?{

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let date = dateFormatter.date(from: self)

        return date

    }
}

extension Date {

    func toString(withFormat format: String = "EEEE ، d MMMM yyyy") -> String {

        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        let str = dateFormatter.string(from: self)

        return str
    }
}

extension UITextField {
    func setLeftPaddingPoints(_ amount:CGFloat){
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    func setRightPaddingPoints(_ amount:CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: amount, height: self.frame.size.height))
        self.rightView = paddingView
        self.rightViewMode = .always
    }
}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}

import UIKit

import NVActivityIndicatorView

var vSpinner : NVActivityIndicatorView?
var vwBG : UIView?
var vwBGMain : UIView?
extension UIViewController {
    func showSpinner(onView : UIView) {
        
        vwBGMain = UIView(frame: CGRect(x: (onView.frame.size.width/2) - 35, y: (onView.frame.size.height/2) - 35, width: 70, height: 70))
        vwBGMain?.backgroundColor = .white
        vwBGMain?.layer.cornerRadius = 5
        vwBGMain?.clipsToBounds = true
        let activityIndicatorView = NVActivityIndicatorView(frame: CGRect(x: 10, y: 10, width: 50, height: 50),
                                                            type: .circleStrokeSpin)
        activityIndicatorView.color = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        vwBGMain?.addSubview(activityIndicatorView)
        activityIndicatorView.layer.cornerRadius = 5
        activityIndicatorView.backgroundColor = .white
        vwBG = UIView(frame: onView.bounds)
        vwBG?.backgroundColor = #colorLiteral(red: 0.01568627451, green: 0, blue: 0, alpha: 0.4)
        onView.addSubview(vwBG!)
        onView.addSubview(vwBGMain!)
        activityIndicatorView.startAnimating()
        vSpinner = activityIndicatorView
    }
    
    func removeSpinner() {
        DispatchQueue.main.async {
            vSpinner?.removeFromSuperview()
            vSpinner = nil
            vwBG?.removeFromSuperview()
            vwBGMain?.removeFromSuperview()
            vwBG = nil
            vwBGMain = nil
        }
    }
}
class Common {
    
    
    static let AuthStoryBoard = UIStoryboard(name: "Auth", bundle: nil)
    static let HomeStoryBoard = UIStoryboard(name: "Home", bundle: nil)
    static var keyWindow:UIWindow? {
        var window : UIWindow?
        if #available(iOS 13.0, *) {
            window =  UIApplication
                .shared
                .connectedScenes
                .flatMap { ($0 as? UIWindowScene)?.windows ?? [] }
                .first { $0.isKeyWindow }
        } else {
            // Fallback on earlier versions
            window = UIApplication.shared.keyWindow
        }
        return window
    }
    static func getDeviceSpecificFontSize_2(_ fontsize: CGFloat) -> CGFloat {
        return ((Common.appDelegate.screenWidth) * fontsize) / 375.0
    }
    
    
    class func viewController(_ name: String, onStoryboard storyboardName: String) -> UIViewController
    {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: name)
    }
    
    class func showAlertToVc(vc:UIViewController,strMessage:String){
        let alert = UIAlertController(title: "EDR", message: strMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
              switch action.style{
              case .default:
                    print("default")

              case .cancel:
                    print("cancel")

              case .destructive:
                    print("destructive")

        }}))
        vc.present(alert, animated: true, completion: nil)

    }
    class func showAlertToVc_with_handler(vc:UIViewController,strMessage:String, handler : @escaping () -> Void){
        let alert = UIAlertController(title: "EDR", message: strMessage, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
            handler()
        }))
        vc.present(alert, animated: true, completion: nil)
    }
    

    struct is_Device {
        static let _iPhone = (UIDevice.current.model as String).isEqual("iPhone") ? true : false
        static let _iPad = (UIDevice.current.model as String).isEqual("iPad") ? true : false
        static let _iPod = (UIDevice.current.model as String).isEqual("iPod touch") ? true : false
    }
    struct kAppColor {
        
        static let orange = Common().RGB(r: 255.0, g: 40, b: 40, a: 1.0).cgColor
        
    }
    //Display Size Compatibility
    struct is_iPhone {
        
        static let _x = (UIScreen.main.bounds.size.height >= 812.0 ) ? true : false
        static let _xs_max = (UIScreen.main.bounds.size.height >= 896.0 ) ? true : false
    
        static let _6p = (UIScreen.main.bounds.size.height >= 736.0 ) ? true : false
        static let _6 = (UIScreen.main.bounds.size.height <= 667.0 && UIScreen.main.bounds.size.height > 568.0) ? true : false
        static let _5 = (UIScreen.main.bounds.size.height <= 568.0 && UIScreen.main.bounds.size.height > 480.0) ? true : false
        static let _4 = (UIScreen.main.bounds.size.height <= 480.0) ? true : false
    }
    
    //IOS Version Compatibility
    struct is_iOS {
        static let _10 = ((Float(UIDevice.current.systemVersion as String))! >= Float(10.0)) ? true : false
        static let _9 = ((Float(UIDevice.current.systemVersion as String))! >= Float(9.0) && (Float(UIDevice.current.systemVersion as String))! < Float(10.0)) ? true : false
        static let _8 = ((Float(UIDevice.current.systemVersion as String))! >= Float(8.0) && (Float(UIDevice.current.systemVersion as String))! < Float(9.0)) ? true : false
    }
    
    // MARK: -  Shared classes
    static let appDelegate: AppDelegate = UIApplication.shared.delegate as! AppDelegate
    
    func RGB(r: Float, g: Float, b: Float, a: Float) -> UIColor {
        return UIColor(red: CGFloat(r / 255.0), green: CGFloat(g / 255.0), blue: CGFloat(b / 255.0), alpha: CGFloat(a))
    }
    func delay(delay: Double, closure: @escaping ()->()) {
        let when = DispatchTime.now() + delay
        DispatchQueue.main.asyncAfter(deadline: when, execute: closure)
    }

    func showAlert(strTitle:String = "",strMsg:String,view:UIViewController){
        let alert = UIAlertController(title: strTitle, message: strMsg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
        DispatchQueue.main.async{
            view.present(alert, animated: true, completion: nil)
        }
    }
    func isValidEmail(email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }

}
